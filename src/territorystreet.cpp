/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "territorystreet.h"

TerritoryStreet::TerritoryStreet(QObject *parent)
    : QObject(parent), m_id(0), m_territoryId(-1), m_streetName(""), m_wktGeometry(""), m_isDirty(false)
{
}

TerritoryStreet::TerritoryStreet(const int territoryId, const QString streetName,
                                 const QString fromNumber, const QString toNumber, const int quantity, const int streetTypeId,
                                 const QString wktGeometry, QObject *parent)
    : QObject(parent), m_id(0), m_territoryId(territoryId), m_streetName(streetName), m_fromNumber(fromNumber), m_toNumber(toNumber), m_quantity(quantity), m_streetTypeId(streetTypeId), m_wktGeometry(wktGeometry), m_isDirty(true)
{
}

TerritoryStreet::TerritoryStreet(const int id, const int territoryId, const QString streetName, const QString fromNumber, const QString toNumber, const int quantity, const int streetTypeId, const QString wktGeometry, QObject *parent)
    : QObject(parent), m_id(id), m_territoryId(territoryId), m_streetName(streetName), m_fromNumber(fromNumber), m_toNumber(toNumber), m_quantity(quantity), m_streetTypeId(streetTypeId), m_wktGeometry(wktGeometry), m_isDirty(false)
{
}

TerritoryStreet::~TerritoryStreet()
{
}

int TerritoryStreet::id() const
{
    return m_id;
}

int TerritoryStreet::territoryId() const
{
    return m_territoryId;
}

void TerritoryStreet::setTerritoryId(const int value)
{
    if (m_territoryId != value) {
        m_territoryId = value;
        setIsDirty(true);
    }
}

QString TerritoryStreet::streetName() const
{
    return m_streetName;
}

void TerritoryStreet::setStreetName(const QString value)
{
    if (m_streetName != value) {
        m_streetName = value;
        setIsDirty(true);
    }
}

QString TerritoryStreet::fromNumber() const
{
    return m_fromNumber;
}

void TerritoryStreet::setFromNumber(QString value)
{
    if (m_fromNumber != value) {
        m_fromNumber = value;
        setIsDirty(true);
    }
}

QString TerritoryStreet::toNumber() const
{
    return m_toNumber;
}

void TerritoryStreet::setToNumber(QString value)
{
    if (m_toNumber != value) {
        m_toNumber = value;
        setIsDirty(true);
    }
}

int TerritoryStreet::quantity() const
{
    return m_quantity;
}

void TerritoryStreet::setQuantity(int value)
{
    if (m_quantity != value) {
        m_quantity = value;
        setIsDirty(true);
    }
}

int TerritoryStreet::streetTypeId() const
{
    return m_streetTypeId;
}

void TerritoryStreet::setStreetTypeId(int value)
{
    if (m_streetTypeId != value) {
        m_streetTypeId = value;
        setIsDirty(true);
    }
}

QString TerritoryStreet::wktGeometry() const
{
    return m_wktGeometry;
}

void TerritoryStreet::setWktGeometry(const QString value)
{
    if (m_wktGeometry != value) {
        m_wktGeometry = value;
        setIsDirty(true);
    }
}

void TerritoryStreet::setIsDirty(const bool value)
{
    m_isDirty = value;
}

bool TerritoryStreet::save()
{
    if (streetName() == "")
        return false;

    // save changes to database
    sql_class *sql = &Singleton<sql_class>::Instance();

    sql_item queryitems;
    queryitems.insert(":id", m_id);
    int streetId = m_id > 0 ? sql->selectScalar("SELECT id FROM territory_street WHERE id = :id", &queryitems, -1).toInt() : -1;

    sql_item insertItems;
    insertItems.insert("territory_id", territoryId());
    insertItems.insert("street_name", streetName());
    insertItems.insert("from_number", fromNumber());
    insertItems.insert("to_number", toNumber());
    insertItems.insert("quantity", quantity());
    insertItems.insert("streettype_id", streetTypeId());
    insertItems.insert("wkt_geometry", wktGeometry());

    bool ret = false;
    if (streetId > 0) {
        // update
        ret = sql->updateSql("territory_street", "id", QString::number(streetId), &insertItems);
    } else {
        // insert new row
        int newId = sql->insertSql("territory_street", &insertItems, "id");
        ret = newId > 0;
        if (newId > 0)
            m_id = newId;
    }
    setIsDirty(!ret);

    return ret;
}

QGeoRectangle TerritoryStreet::boundingGeoRectangle()
{
    QGeoRectangle r;
#if defined(Q_OS_WIN)
    qDebug() << "GDAL missing";
#elif defined(Q_OS_LINUX) || defined(Q_OS_MAC)
    // Parse WKT street geometry
    OGRSpatialReference osr;
    OGRGeometry *streetGeometry = nullptr;
    QByteArray bytes = m_wktGeometry.toUtf8();
    const char *data = bytes.constData();
    OGRErr err = OGRGeometryFactory::createFromWkt(data, &osr, &streetGeometry);
    if (err != OGRERR_NONE) {
        // process error, like emit signal
    } else {
        OGREnvelope streetEnvelope;
        streetGeometry->getEnvelope(&streetEnvelope);
        r.setBottomLeft(QGeoCoordinate(streetEnvelope.MinY, streetEnvelope.MinX));
        r.setTopRight(QGeoCoordinate(streetEnvelope.MaxY, streetEnvelope.MaxX));
        return r;
    }
#endif
    return r;
}

TerritoryStreetModel::TerritoryStreetModel()
    : m_selectedStreet(nullptr)
{
}

TerritoryStreetModel::TerritoryStreetModel(QObject *parent)
    : QAbstractTableModel(parent), m_selectedStreet(nullptr)
{
}

QHash<int, QByteArray> TerritoryStreetModel::roleNames() const
{
    QHash<int, QByteArray> items;
    items[StreetIdRole] = "id";
    items[TerritoryIdRole] = "territoryId";
    items[StreetNameRole] = "streetName";
    items[FromNumberRole] = "fromNumber";
    items[ToNumberRole] = "toNumber";
    items[QuantityRole] = "quantity";
    items[StreetTypeIdRole] = "streetTypeId";
    items[WktGeometryRole] = "wktGeometry";
    return items;
}

int TerritoryStreetModel::rowCount(const QModelIndex & /*parent*/) const
{
    return territoryStreets.count();
}

int TerritoryStreetModel::columnCount(const QModelIndex & /*parent*/) const
{
    return 8;
}

QVariantMap TerritoryStreetModel::get(int row)
{
    QHash<int, QByteArray> names = roleNames();
    QHashIterator<int, QByteArray> i(names);
    QVariantMap res;
    QModelIndex idx = index(row, 0);
    while (i.hasNext()) {
        i.next();
        QVariant data = idx.data(i.key());
        res[i.value()] = data;
    }
    return res;
}

QVariant TerritoryStreetModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    if (index.row() < 0 || index.row() > territoryStreets.count())
        return QVariant();

    if (role == Qt::DisplayRole || role == Qt::EditRole) {
        switch (index.column()) {
        case 0:
            return territoryStreets[index.row()]->id();
        case 1:
            return territoryStreets[index.row()]->territoryId();
        case 2:
            return territoryStreets[index.row()]->streetName();
        case 3:
            return territoryStreets[index.row()]->fromNumber();
        case 4:
            return territoryStreets[index.row()]->toNumber();
        case 5:
            return territoryStreets[index.row()]->quantity();
        case 6:
            return territoryStreets[index.row()]->streetTypeId();
        case 7:
            return territoryStreets[index.row()]->wktGeometry();
        }
    }

    switch (role) {
    case StreetIdRole:
        return territoryStreets[index.row()]->id();
    case TerritoryIdRole:
        return territoryStreets[index.row()]->territoryId();
    case StreetNameRole:
        return territoryStreets[index.row()]->streetName();
    case FromNumberRole:
        return territoryStreets[index.row()]->fromNumber();
    case ToNumberRole:
        return territoryStreets[index.row()]->toNumber();
    case QuantityRole:
        return territoryStreets[index.row()]->quantity();
    case StreetTypeIdRole:
        return territoryStreets[index.row()]->streetTypeId();
    case WktGeometryRole:
        return territoryStreets[index.row()]->wktGeometry();
    }

    return QVariant();
}

bool TerritoryStreetModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    TerritoryStreet *currTerritoryStreet = territoryStreets[index.row()];

    if (role == Qt::EditRole) {
        int column = index.column();

        switch (column) {
        case 0:
            //streetId
            break;
        case 1:
            currTerritoryStreet->setTerritoryId(value.toInt());
            break;
        case 2:
            currTerritoryStreet->setStreetName(value.toString());
            break;
        case 3:
            currTerritoryStreet->setFromNumber(value.toString());
            break;
        case 4:
            currTerritoryStreet->setToNumber(value.toString());
            break;
        case 5:
            currTerritoryStreet->setQuantity(value.toInt());
            break;
        case 6:
            currTerritoryStreet->setStreetTypeId(value.toInt());
            break;
        case 7:
            currTerritoryStreet->setWktGeometry(value.toString());
            break;
        default:
            break;
        }

        emit editCompleted();
    } else {
        switch (role) {
        case Roles::TerritoryIdRole:
            currTerritoryStreet->setTerritoryId(value.toInt());
            emit dataChanged(this->index(index.row(), 1), this->index(index.row(), 1));
            break;
        case Roles::StreetNameRole:
            currTerritoryStreet->setStreetName(value.toString());
            emit dataChanged(this->index(index.row(), 2), this->index(index.row(), 2));
            break;
        case Roles::FromNumberRole:
            currTerritoryStreet->setFromNumber(value.toString());
            emit dataChanged(this->index(index.row(), 3), this->index(index.row(), 3));
            break;
        case Roles::ToNumberRole:
            currTerritoryStreet->setToNumber(value.toString());
            emit dataChanged(this->index(index.row(), 4), this->index(index.row(), 4));
            break;
        case Roles::QuantityRole:
            currTerritoryStreet->setQuantity(value.toInt());
            emit dataChanged(this->index(index.row(), 5), this->index(index.row(), 5));
            break;
        case Roles::StreetTypeIdRole:
            currTerritoryStreet->setStreetTypeId(value.toInt());
            emit dataChanged(this->index(index.row(), 6), this->index(index.row(), 6));
            break;
        case Roles::WktGeometryRole:
            currTerritoryStreet->setWktGeometry(value.toString());
            emit dataChanged(this->index(index.row(), 7), this->index(index.row(), 7));
            break;
        default:
            break;
        }
    }

    return true;
}

Qt::ItemFlags TerritoryStreetModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;
    return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}

QModelIndex TerritoryStreetModel::index(int row, int column, const QModelIndex &parent)
        const
{
    if (hasIndex(row, column, parent)) {
        return createIndex(row, column);
    }
    return QModelIndex();
}

QModelIndex TerritoryStreetModel::getStreetIndex(int streetId) const
{
    for (int row = 0; row < this->rowCount(); ++row) {
        QModelIndex rowIndex = this->index(row, 0);
        if (rowIndex.data(StreetIdRole) == streetId)
            return rowIndex;
    }
    return QModelIndex();
}

int TerritoryStreetModel::addStreet(TerritoryStreet *territoryStreet)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    territoryStreets << territoryStreet;
    endInsertRows();
    return territoryStreet->id();
}

int TerritoryStreetModel::addStreet(int territoryId, const QString streetName, QString wktGeometry, int streetTypeId)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    TerritoryStreet *newTerritoryStreet = new TerritoryStreet(territoryId, streetName, "", "", 1, streetTypeId, wktGeometry);
    newTerritoryStreet->save();
    territoryStreets << newTerritoryStreet;
    endInsertRows();
    return newTerritoryStreet->id();
}

void TerritoryStreetModel::removeStreet(int id)
{
    if (id > 0) {
        sql_class *sql = &Singleton<sql_class>::Instance();

        sql_item s;
        s.insert(":id", id);
        s.insert(":active", 0);
        if (sql->execSql("UPDATE territory_street SET active = :active WHERE id = :id", &s, true)) {
            QModelIndex streetIndex = getStreetIndex(id);
            if (streetIndex.isValid()) {
                int row = streetIndex.row();
                beginRemoveRows(QModelIndex(), row, row);
                territoryStreets.erase(std::next(territoryStreets.begin(), row));
                endRemoveRows();
            }
        }
    }
}

bool TerritoryStreetModel::removeRows(int row, int count, const QModelIndex &parent)
{
    Q_UNUSED(parent);
    if (row < 0 || count < 1 || (row + count) > territoryStreets.size())
        return false;
    beginRemoveRows(QModelIndex(), row, row + count - 1);
    for (int i = 0; i < count; i++) {
        territoryStreets.removeAt(row);
    }
    endRemoveRows();
    return true;
}

void TerritoryStreetModel::loadStreets(int territoryId)
{
    removeRows(0, territoryStreets.count());
    sql_class *sql = &Singleton<sql_class>::Instance();

    QString sqlQuery("SELECT * FROM territory_street WHERE ");
    if (territoryId != 0)
        sqlQuery.append("territory_id = " + QVariant(territoryId).toString() + " AND ");
    sqlQuery.append("active ORDER BY street_name");
    sql_items territoryStreetRows = sql->selectSql(sqlQuery);

    if (!territoryStreetRows.empty()) {
        for (unsigned int i = 0; i < territoryStreetRows.size(); i++) {
            sql_item s = territoryStreetRows[i];

            addStreet(new TerritoryStreet(s.value("id").toInt(),
                                          s.value("territory_id").toInt(),
                                          s.value("street_name").toString(),
                                          s.value("from_number").toString(),
                                          s.value("to_number").toString(),
                                          s.value("quantity").toInt(),
                                          s.value("streettype_id").toInt(),
                                          s.value("wkt_geometry").toString()));
        }
    }
}

void TerritoryStreetModel::saveStreets()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql->startTransaction();
    for (int i = 0; i < territoryStreets.count(); i++) {
        TerritoryStreet *s = territoryStreets[i];
        if (s->isDirty()) {
            s->save();
        }
    }
    sql->commitTransaction();
}

bool TerritoryStreetModel::updateStreet(int streetId, QString wktGeometry)
{
    QModelIndex rowIndex = getStreetIndex(streetId);
    if (rowIndex.isValid()) {
        setData(rowIndex, wktGeometry, WktGeometryRole);
        return true;
    }
    return false;
}

bool TerritoryStreetModel::updateStreet(int streetId, int territoryId)
{
    QModelIndex rowIndex = getStreetIndex(streetId);
    if (rowIndex.isValid()) {
        setData(rowIndex, territoryId, TerritoryIdRole);
        return true;
    }
    return false;
}

bool TerritoryStreetModel::updateStreet(int streetId, int territoryId, const QString streetName, QString wktGeometry)
{
    QModelIndex rowIndex = getStreetIndex(streetId);
    if (rowIndex.isValid()) {
        setData(rowIndex, territoryId, TerritoryIdRole);
        setData(rowIndex, streetName, StreetNameRole);
        setData(rowIndex, wktGeometry, WktGeometryRole);
        return true;
    }
    return false;
}

void TerritoryStreetModel::setSelectedStreet(const QModelIndex &selectedIndex)
{
    if (selectedIndex.isValid()) {
        if (m_selectedStreet != nullptr) {
            if (m_selectedStreet->isDirty()) {
                // save current street
                if (m_selectedStreet->save()) {
                    // street saved
                } else {
                    // street not saved
                }
            }
        }
        m_selectedStreet = getItem(selectedIndex);
    } else {
        m_selectedStreet = nullptr;
    }
    emit selectedChanged(selectedIndex);
}

int TerritoryStreetModel::getDefaultStreetTypeId()
{
    sql_class *sql = &Singleton<sql_class>::Instance();
    return QVariant(sql->getSetting("territory_default_streettype", "1")).toInt();
}

TerritoryStreet *TerritoryStreetModel::getItem(const QModelIndex &index) const
{
    if (index.isValid()) {
        TerritoryStreet *item = territoryStreets[index.row()];
        if (item)
            return item;
    }
    return nullptr;
}

TerritoryStreetSortFilterProxyModel::TerritoryStreetSortFilterProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
}

void TerritoryStreetSortFilterProxyModel::setFilterTerritoryId(int value)
{
    m_territoryId = value;
    invalidateFilter();
}

bool TerritoryStreetSortFilterProxyModel::filterAcceptsRow(int sourceRow,
                                                           const QModelIndex &sourceParent) const
{
    QModelIndex indexTerritoryId = sourceModel()->index(sourceRow, 1, sourceParent);
    int territoryId = sourceModel()->data(indexTerritoryId).toInt();
    return (territoryId == m_territoryId);
}

bool TerritoryStreetSortFilterProxyModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    QVariant leftData = sourceModel()->data(left);
    QVariant rightData = sourceModel()->data(right);

    return leftData.toString() < rightData.toString();
}
