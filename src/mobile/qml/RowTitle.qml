import QtQuick 2.3
import QtQuick.Controls 1.3
import QtQuick.Layouts 1.1

Label {
    id: label
    property int leftMargin: 10*dpi
    property color textColor: "grey"
    property bool uppercase: true

    Layout.fillWidth: true
    Layout.leftMargin: leftMargin
    //anchors.left: parent.left
    //anchors.leftMargin: leftMargin
    //anchors.right: parent.right
    color: textColor
    renderType: Text.QtRendering
    font.pointSize: app_info.fontsize
    font.capitalization: uppercase ? Font.AllUppercase : Font.MixedCase
}
