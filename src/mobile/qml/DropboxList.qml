/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2019, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import net.theocbase.mobile 1.0
import net.theocbase 1.0

Page {
    id: dropboxList

    property DBAccount account: ccloud.authentication.getAccountInfo()

    signal syncFileChanged(string path)

    function searchFiles(filename) {
        busy.running = true
        listView.model = ccloud.authentication.searchFile(filename)
        listView.enabled = true
        listView.filetest = account.syncFile
        busy.running = false
    }

    Connections {
        target: account
        onSyncFileChanged: {
            syncFileChanged(account.syncFile)
        }
    }

    Component.onCompleted: {
        timer.start()
    }
    Timer {
        id: timer
        interval: 500;
        onTriggered: searchFiles("syncfile.json")
    }

    header: BaseToolbar {
        title: "Syncfile"
        componentLeft: ToolButton {
            icon.source: "qrc:///done.svg"
            icon.color: "white"
            onClicked: stackView.pop()
        }
    }

    BusyIndicator {
        id: busy
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        running: false
    }

    MsgBox { id: msg }

    ListView {
        id: listView
        anchors.fill: parent
        enabled: false
        property string filetest
        delegate: RadioDelegate {
            width: parent.width
            height: 40*dpi
            text: model.path + (model.sharedby === "" ? "" : "(" + model.sharedby + ")")
            font.pointSize: app_info.fontsize
            checked: model.path === listView.filetest
            onToggled: {
                if (checked) {
                    var clickedFunc = function(ok) {
                        if (ok) {
                            ccloud.authentication.getAccountInfo().syncFile = model.path
                            listView.filetest = model.path
                        } else {
                            listView.filetest = ""
                            listView.filetest = account.syncFile
                        }
                        msg.onButtonClicked.disconnect(clickedFunc)
                    }
                    msg.onButtonClicked.connect(clickedFunc)
                    msg.showYesNo("Syncfile","Do you want to change syncfile?\n" +
                                  "(If you accept you will lose any changes that have not yet been synchronized)",-1)
                }
            }
        }
    }
}
