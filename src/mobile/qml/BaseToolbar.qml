import QtQuick 2.3
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.3
import QtQuick.Layouts 1.1

ToolBar {
    property alias componentLeft: loaderLeft.sourceComponent
    property alias componentLeftVisible: loaderLeft.visible
    property alias componentLeftMiddle: loaderLeftMiddle.sourceComponent
    property alias componentLeftMiddleVisible: loaderLeftMiddle.visible
    property alias componentMiddle: loaderMiddle.sourceComponent
    property alias componentRightMiddle: loaderRightMiddle.sourceComponent
    property alias componentRightMiddleVisible: loaderRightMiddle.visible
    property alias componentRight: loaderRight.sourceComponent
    property alias componentRightVisible: loaderRight.visible

    property string title    
    property color titleColor: "white"
    property int preferredHeight: 45 * dpi + toolbarTopMargin
    property bool showBorderLine: true

    height: 45 * dpi + toolbarTopMargin

    RowLayout {
        id: row
        anchors.fill: parent
        anchors.topMargin: toolbarTopMargin

        Loader {
            id: loaderLeft
            Layout.fillHeight: true
            //Layout.fillWidth: true
            sourceComponent: defaultComp
            Layout.preferredWidth: parent.height
        }
        Loader {
            id: loaderLeftMiddle
            Layout.fillHeight: true
            Layout.preferredWidth: parent.height            
        }
        Loader {
            id: loaderMiddle
            Layout.fillHeight: true
            Layout.fillWidth: true
            sourceComponent: defaultLabel
        }
        Loader {
            id: loaderRightMiddle
            Layout.fillHeight: true
            Layout.preferredWidth: parent.height
        }
        Loader {
            id: loaderRight
            Layout.fillHeight: true
            //Layout.fillWidth: true
            sourceComponent: defaultComp
            Layout.preferredWidth: parent.height
        }
    }

    Component {
        id: defaultLabel
        Label {
            anchors.fill: parent
            id: labelComp
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: title
            font.bold: true
            font.pointSize: app_info.fontsize
            color: titleColor// "black"
            renderType: Text.QtRendering
            elide: Text.ElideMiddle
        }
    }
    Component {
        id: defaultComp
        Item { implicitWidth: 20 }
    }
}
