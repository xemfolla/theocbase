<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>CBStudySchedule</name>
    <message>
        <source>S</source>
        <comment>abbreviation of the &apos;study&apos; (Congregation Bible Study)</comment>
        <translation>G</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Geen volgende les toewijzen</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Op huidige les laten staan</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Data ongeldig</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Timing toevoegen?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Bron</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Student</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Assistent</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resultaat</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Voltooid</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Vrijwilliger</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Een vrijwilliger selecteren</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Huidige les</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Oefening voltooid</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Volgende les</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Volgende les selecteren</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Timing</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notities</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Informatie school</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Stopwatch starten</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Stopwatch stilzetten</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Setting</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Setting selecteren</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Bron</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Studieleider</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Spreker</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lezer</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notities</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Details</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>Opmerkingen</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>SCHATTEN UIT GODS WOORD</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>LEG JE TOE OP DE VELDDIENST</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>LEVEN ALS CHRISTENEN</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Voorzitter</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Raadgever</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Lied %1 en gebed</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Lied</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Studieleider</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lezer</translation>
    </message>
    <message>
        <source>Main</source>
        <comment>Main class when auxiliary classes</comment>
        <translation>Hoofd</translation>
    </message>
    <message>
        <source>Second</source>
        <comment>Auxiliary Class</comment>
        <translation>Tweede</translation>
    </message>
    <message>
        <source>Third</source>
        <comment>Auxiliary Class</comment>
        <translation>Derde</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Inleiding</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Samenvatting van deze week en vooruitblik op volgende week</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Gebed</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Schema importeren...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>HZ</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>2eZ</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>3eZ</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Op huidige les laten staan</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Stopwatch starten</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Stopwatch stilzetten</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Timing toevoegen?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Bron</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Student</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Setting</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Resultaat</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Voltooid</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Timing</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Huidige les</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Oefening voltooid</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Volgende les</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Volgende les selecteren</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Notities</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Assistent</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Invaller</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Stopwatch</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>De huisbewoner mag niet iemand zijn van het andere geslacht.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Les</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Details</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Slepen om te vernieuwen</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Loslaten om te vernieuwen</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Loading...</source>
        <translation>Laden...</translation>
    </message>
    <message>
        <source>Login Failed</source>
        <translation>Aanmelden mislukt</translation>
    </message>
    <message>
        <source>TheocBase Login</source>
        <translation>TheocBase Login</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Gebruikersnaam</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Paswoord</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Aanmelden</translation>
    </message>
    <message>
        <source>New User / Forgot password</source>
        <translation>Nieuwe gebruiker / Paswoord vergeten</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Gebruikersnaam of e-mail</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Account aanmaken</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Paswoord opnieuw instellen</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>E-mailadres niet gevonden!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Paswoord vergeten</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Aanmeldpagina</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Main</source>
        <translation>Hoofd</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Tweede</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Derde</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Week van %1</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Gemeentebijbelstudie</translation>
    </message>
    <message>
        <source>Theocratic Ministry School</source>
        <translation>Theocratische bedieningsschool</translation>
    </message>
    <message>
        <source>Service Meeting</source>
        <translation>Dienstvergadering</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation>Openbare lezing</translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation>Wachttorenstudie</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Doordeweekse vergadering</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Weekendvergadering</translation>
    </message>
    <message>
        <source>Midweek</source>
        <comment>Midweek Meeting</comment>
        <translation>Doordeweek</translation>
    </message>
    <message>
        <source>Weekend</source>
        <comment>Weekend Meeting</comment>
        <translation>Weekend</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>UITGAANDE SPREKERS</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 spreker weg dit weekend</numerusform>
            <numerusform>%1 sprekers weg dit weekend</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Geen sprekers weg dit weekend</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Lied en gebed</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Lied %1 en gebed</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>OPENBARE LEZING</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>WACHTTORENSTUDIE</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Lied %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Studieleider</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lezer</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>WT importeren...</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Gemeente</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Spreker</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobiel</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefoon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Gastheer</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Openbare Lezing</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Voornaam</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Achternaam</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Broeder</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Zuster</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Dienaar</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Gezin</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Gezinslid van</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Contactgegevens</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefoon</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Use for School</source>
        <translation>Toewijzingen school</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Alle scholen</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Enkel hoofdzaal</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Enkel extra lokalen</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation>Hoofdpunten</translation>
    </message>
    <message>
        <source>No 1</source>
        <translation>Nr 1</translation>
    </message>
    <message>
        <source>No 2</source>
        <translation>Nr 2</translation>
    </message>
    <message>
        <source>No 3</source>
        <translation>Nr 3</translation>
    </message>
    <message>
        <source>Break</source>
        <translation>Pauze</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Gezinshoofd</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Assistent</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Call %1?</source>
        <comment>Call to phone number</comment>
        <translation>Bellen naar %1?</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Actief</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Voorzitter</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Schatten uit Gods Woord</translation>
    </message>
    <message>
        <source>Digging for Spiritual Gems</source>
        <translation>Graven naar geestelijke juweeltjes</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Bijbellezen</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Eerste gesprek</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Nabezoek</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Bijbelstudie</translation>
    </message>
    <message>
        <source>Prepare This Month&#x27;s Presentations</source>
        <translation>Presentaties voor deze maand voorbereiden</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Aandelen bij &quot;Leven als Christenen&quot;</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Gemeentebijbelstudie</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Lezer Gemeentebijbelstudie</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Nieuwe verkondiger</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Gebed</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Gastheer voor openbare sprekers</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobiel</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Doordeweekse vergadering</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Lezing</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Weekendvergadering</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Wachttorenstudieleider</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Wachttorenstudielezer</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Verkondigers</translation>
    </message>
</context>
<context>
    <name>SchoolSchedule</name>
    <message>
        <source>Main</source>
        <translation>Hoofd</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Tweede</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Derde</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Keuzelijst</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Instellingen</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Afmelden</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase Homepagina</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Reactie</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Laatste synchronisatie: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Schema</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Tijdstip tonen</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Tijdsduur tonen</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Gebruikersinterface</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Taal</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Synchroniseren...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Voorzitter</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Lied</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Voorzitter Weekendvergadering</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Lied</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Lied Wachttoren</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Wachttorenuitgave</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Artikel</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Studieleider</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Lezer</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <source>Not set</source>
        <translation>Niet ingesteld</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Tijdlijn</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Aantal weken vóór de geselecteerde datum</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Aantal weken na de geselecteerde datum</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>weken</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Aantal weken om grijs te maken na een toewijzing</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Updates available. Do you want to synchronize?</source>
        <translation>Nieuwe data beschikbaar... Wilt u synchroniseren?</translation>
    </message>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Identieke wijzigingen zijn zowel lokaal als in de cloud teruggevonden (%1 rijen). Wilt u de lokale wijzigingen behouden?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>De gegevens in de cloud zijn gereset. Je lokale data zal worden vervangen. Verder gaan?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Niet de volgende les toewijzen</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>niet ingesteld</translation>
    </message>
</context>
<context>
    <name>schoolview</name>
    <message>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>H</translation>
    </message>
    <message>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>L</translation>
    </message>
</context></TS>