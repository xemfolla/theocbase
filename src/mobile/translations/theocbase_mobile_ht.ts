<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ht">
<context>
    <name>CBStudySchedule</name>
    <message>
        <source>S</source>
        <comment>abbreviation of the &apos;study&apos; (Congregation Bible Study)</comment>
        <translation>EBK</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Pa asiyen pwochen leson an pou travay</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Travay sou menm leson an</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Done sa a pa valab</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Ajoute kantite tan an?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Referans</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Elèv</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Moun k ap fè l avè w</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rezilta</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Leson an fin travay</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Volontè</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Chwazi yon volontè</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Leson w ap travay</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Egzèsis yo fèt</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Pwochen Leson</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Chwazi pwochen leson an</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Kantite tan</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Nòt</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Enfòmasyon sou lekòl la</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Demare kwonomèt la</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Kanpe kwonomèt la</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Kad</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Chwazi yon kad</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Referans</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lektè</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Nòt</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detay</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>Nòt</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>TREZÒ KI NAN PAWÒL BONDYE A</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>BYEN PREPARE W POU TRAVAY PREDIKASYON AN</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>LAVI NOU ANTANKE KRETYEN</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Prezidan</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Konseye</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Kantik %1 ak Priyè</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lektè</translation>
    </message>
    <message>
        <source>Main</source>
        <comment>Main class when auxiliary classes</comment>
        <translation>Prensipal</translation>
    </message>
    <message>
        <source>Second</source>
        <comment>Auxiliary Class</comment>
        <translation>Dezyèm</translation>
    </message>
    <message>
        <source>Third</source>
        <comment>Auxiliary Class</comment>
        <translation>Twazyèm</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Pawòl Entwodiksyon</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Sa n wè semèn sa a ak sa n ap wè lòt semèn</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Priyè</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Enpòte Pwogram nan...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>SP</translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>SS1</translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>SS2</translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Travay sou menm leson an</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Demare kwonomèt la</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Kanpe kwonomèt la</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Ajoute kantite tan an?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Referans</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Elèv</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Kad</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rezilta</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Fin travay leson an</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Kantite tan</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Leson w ap travay</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Egzèsis yo fèt</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Pwochen Leson</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Chwazi pwochen leson an</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Nòt</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Moun k ap fè l avè w</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Volontè</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Kwonomèt</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Moun k ap fè patisipasyon an ak elèv la pa dwe yon moun ki pa menm sèks avè l</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Pwen leson</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detay</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Rale l desann pou w mete l ajou</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Lage l pou w mete l ajou...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Loading...</source>
        <translation>Chajman...</translation>
    </message>
    <message>
        <source>Login Failed</source>
        <translation>Keneksyon an pa fèt</translation>
    </message>
    <message>
        <source>TheocBase Login</source>
        <translation>Konekte sou pwogram TheocBase la</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Non itilizatè a</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Modpas</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Konekte</translation>
    </message>
    <message>
        <source>New User / Forgot password</source>
        <translation>Nouvo itilizatè / Bliye modpas ou</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Non itlizatè a oswa Imel li</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imel</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Kreye yon Kont</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Reyaktive Modpas ou</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Adrès Imel sa a pa disponib!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Bliye Modpas</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Paj Koneksyon</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Main</source>
        <translation>Prensipal</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Dezyèm</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Twazyèm</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Semèn ki kòmanse %1</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Etid Biblik Kongregasyon an</translation>
    </message>
    <message>
        <source>Theocratic Ministry School</source>
        <translation>Lekòl Ministè Teyokratik</translation>
    </message>
    <message>
        <source>Service Meeting</source>
        <translation>Reyinyon Sèvis</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation>Diskou Piblik</translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation>Etid Toudegad</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reyinyon Lasemèn nan</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Reyinyon Fen Semèn nan</translation>
    </message>
    <message>
        <source>Midweek</source>
        <comment>Midweek Meeting</comment>
        <translation>Lasemèn</translation>
    </message>
    <message>
        <source>Weekend</source>
        <comment>Weekend Meeting</comment>
        <translation>Fen Semèn</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>ORATÈ K AP SÒTI AL BAY DISKOU NAN LÒT KONGREGASYON</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>Gen %1 oratè k ap sòti al bay diskou nan lòt kongregasyon wikenn sa a</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Pa gen oratè k ap sòti al bay diskou nan lòt kongregasyon wikenn sa a</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Kantik ak Priyè</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Kantik %1 ak Priyè</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>DISKOU PIBLIK</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>ETID TOUDEGAD</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Kantik %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Lektè</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Enpòte TG...</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Kongregasyon</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Oratè</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Selilè</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefòn</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imel</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Enfo</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Moun k ap bay ospitalite</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Diskou Piblik</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Non</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Siyati</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Frè</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sè</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Frè nome</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Fanmi</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Fanmi li fè pati</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Enfòmasyon sou moun nan</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefòn</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imel</translation>
    </message>
    <message>
        <source>Use for School</source>
        <translation>Itilize pou Lekòl la</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Tout Sal yo</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Sal Prensipal Sèlman</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Sal Segondè Sèlman</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation>Pwen entèresan</translation>
    </message>
    <message>
        <source>No 1</source>
        <translation>No. 1</translation>
    </message>
    <message>
        <source>No 2</source>
        <translation>No. 2</translation>
    </message>
    <message>
        <source>No 3</source>
        <translation>No. 3</translation>
    </message>
    <message>
        <source>Break</source>
        <translation>Pòz</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Chèf Fanmi</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Moun k ap fè l avè w</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Enfo</translation>
    </message>
    <message>
        <source>Call %1?</source>
        <comment>Call to phone number</comment>
        <translation>Rele %1?</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktif</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Prezidan</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Trezò ki nan pawòl Bondye a</translation>
    </message>
    <message>
        <source>Digging for Spiritual Gems</source>
        <translation>Fouye pou n ka jwenn trezò espirityèl</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Lekti Labib</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Premye Rankont</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Nouvèl Vizit</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Etid Biblik</translation>
    </message>
    <message>
        <source>Prepare This Month&#x27;s Presentations</source>
        <translation>Prepare prezantasyon pou mwa a</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Diskou nan pati Lavi nou antanke kretyen</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Etid Biblik Kongregasyon an</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Lektè Etid Biblik Kongregasyon an</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Nouvo pwoklamatè</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Priyè</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Moun k ap bay oratè vizitè yo ospitalite</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Selilè</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Reyinyon Lasemèn nan</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Diskou</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Reyinyon Fen Semèn nan</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Kondiktè Etid Toudegad la</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Lektè Etid Toudegad la</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Pwoklamatè yo</translation>
    </message>
</context>
<context>
    <name>SchoolSchedule</name>
    <message>
        <source>Main</source>
        <translation>Prensipal</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Dezyèm</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Twazyèm</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Chwazi Lis</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Kad yo</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Dekonekte</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Enfo</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Vèsyon</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>Paj Akèy TheocBase la</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Ide itilizatè yo</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Dènye senkwonizasyon an te fèt nan dat: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Pwogram</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Afiche Lè a</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Afiche kantite tan an</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Anviwònman Itilizatè a</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Lang</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Konekte</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imel</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Non</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Ap senkwonize...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Prezidan</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Prezidan Reyinyon Fen Semèn nan</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Kantik</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Kantik Toudegad</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Nimewo Toudegad la</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Kantite Atik</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tèm</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Kondiktè</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Lektè</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <source>Not set</source>
        <translation>Sa a pa defini</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Kantite tan ki pase</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Kantite semèn anvan dat ki chwazi a</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Kantite semèn ki vini aprè dat ki chwazi a</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>semèn yo</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Kantite semèn pou mete an gri aprè yon asiyasyon</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Updates available. Do you want to synchronize?</source>
        <translation>Gen mizajou ki disponib. Èske w vle senkwonize?</translation>
    </message>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Chanjman ki fèt la yo parèt tou nan &quot;cloud&quot; (%1 ranje). Èske w vle anrejistre chanjman ki fèt la yo?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Enfòmasyon ki nan cloud yo efase. Y ap ranplase enfòmasyon ki nan òdinatè w la. Kontinye?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Pa asiyen pwochen leson an pou travay</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Sa a pa defini</translation>
    </message>
</context>
<context>
    <name>schoolview</name>
    <message>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>PE</translation>
    </message>
    <message>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>L</translation>
    </message>
</context></TS>