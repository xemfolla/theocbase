<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="lt">
<context>
    <name>CBStudySchedule</name>
    <message>
        <source>S</source>
        <comment>abbreviation of the &apos;study&apos; (Congregation Bible Study)</comment>
        <translation>BBS</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Kitos pamokos neskirti</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Pakartoti pamoką</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Negaliojantys duomenys</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Pridėti laiką</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Medžiaga</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Mokinys</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Padėjėjas/a</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rezultatas</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Užbaigtas</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Savanoris</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Pasirinkti savanorį</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Dabartinė pamoka</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Atlikti pratimai</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Kita pamoka</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Pasirinkti kitą pamoką</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Laikas</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Pastabos</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Mokyklos duomenys</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Pradėti laiką</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Sustabdyti laiką</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Situacija</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Pasirinkti situaciją</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Medžiagos šaltinis</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vadovas</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Kalbėtojas</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Skaitovas</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Pastabos</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detalės</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation>Užrašai</translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>IŠ DIEVO ŽODŽIO LOBYNO</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>DIRBKIME EVANGELIZUOTOJO DARBĄ</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>ESAME KRIKŠČIONYS</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Pirmininkas</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Patarėjas</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Giesmė %1 ir malda</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Giesmė</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vadovas</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Skaitovas</translation>
    </message>
    <message>
        <source>Main</source>
        <comment>Main class when auxiliary classes</comment>
        <translation>Pagrindinė</translation>
    </message>
    <message>
        <source>Second</source>
        <comment>Auxiliary Class</comment>
        <translation>Antra</translation>
    </message>
    <message>
        <source>Third</source>
        <comment>Auxiliary Class</comment>
        <translation>Trečia</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Įžanginiai žodžiai</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Pakartojimas ir kitos savaitės medžiagos apžvalga</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Malda</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation>Įkelti tvarkaraštį...</translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Pakartoti pamoką</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Pradėti skaičiuoti laiką</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Sustabdyti laiką</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Pridėti laiką?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Medžiagos šaltinis</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Mokinys</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Situacija</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Rezultatas</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Atlikta</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Laikas</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Dabartinė pamoka</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Pratimas atliktas</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Kita pamoka</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Pasirinkti kitą pamoką</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Pastabos</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Padėjėjas/a</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Savanoris</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Pradėti skaičiuoti laiką</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Padejejas/a neturėtų būti kažkas iš priešingos lyties.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation>Pamoka</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Detalės</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Paslink žemyn, kad atsinaujintų...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Paleisk, kad atsinaujintų...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Loading...</source>
        <translation>Kraunasi...</translation>
    </message>
    <message>
        <source>Login Failed</source>
        <translation>Nepavyko prisijungti</translation>
    </message>
    <message>
        <source>TheocBase Login</source>
        <translation>TheocBase Prisijungti</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Vartotojo vardas</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Slaptažodis</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Prisijungti</translation>
    </message>
    <message>
        <source>New User / Forgot password</source>
        <translation>Naujas vartotojas / Pamiršo slaptažodį </translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Vartotojo vardas arba el. pašto adresas</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>El. paštas</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Sukurti paskyrą</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Atkurti slaptažodį</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>El. pašto adresas nerastas</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Pamiršau slaptažodį</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Prisijungimo puslapis</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Main</source>
        <translation>Pagrindinis</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Antras</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Trečias</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Savaitė nuo %1</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Bendruomenės biblijos studijos</translation>
    </message>
    <message>
        <source>Theocratic Ministry School</source>
        <translation>Teokratinės tarnybos mokykla</translation>
    </message>
    <message>
        <source>Service Meeting</source>
        <translation>Tarnybos sueiga</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation>Viešoji kalba</translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation>Sargybos bokšto studijos</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Šiokiadienio sueiga</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Savaitgalio sueiga</translation>
    </message>
    <message>
        <source>Midweek</source>
        <comment>Midweek Meeting</comment>
        <translation>Šokiadienis</translation>
    </message>
    <message>
        <source>Weekend</source>
        <comment>Weekend Meeting</comment>
        <translation>Savaitgalis</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation>Išvykę kalbėtojai</translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>%1 kalbėtojas išvykęs šį savaitgalį</numerusform>
            <numerusform>%1 kalbėtojai išvykę šį savaitgalį</numerusform>
            <numerusform>%1 kalbėtojų išvykę šį savaitgalį</numerusform>
        </translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation>Šį savaitgalį išvykusių kalbėtojų nėra </translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Giesmė ir malda</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Giesmė %1 ir malda</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>Viešoji kalba</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>SARGYBOS BOKŠTO STUDIJA</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Giesmė %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Vadovas</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Skaitovas</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation>Įkelti SB...</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation>Bendruomenė</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Kalbėtojas</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobilusis telefonas</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefonas</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>El.paštas</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Host</source>
        <translation>Kas kviečia pietų</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation>Viešoji kalba</translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Vardas</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Pavardė</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Brolis</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sesė</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Patarnautojas</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Šeima</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Šeimos narys su</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Kontakto domenys</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefonas</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>El. paštas</translation>
    </message>
    <message>
        <source>Use for School</source>
        <translation>Naudoti mokyklai</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Visose salėse</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Tik pagrindinėje salėje</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Tik antrojoje salėje</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation>Įsidėmėtini punktai</translation>
    </message>
    <message>
        <source>No 1</source>
        <translation>Nr. 1</translation>
    </message>
    <message>
        <source>No 2</source>
        <translation>Nr. 2</translation>
    </message>
    <message>
        <source>No 3</source>
        <translation>Nr. 3</translation>
    </message>
    <message>
        <source>Break</source>
        <translation>Pertrauka</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Šeimos galva</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Padėjėjas/a</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Call %1?</source>
        <comment>Call to phone number</comment>
        <translation>Skambinti %1?</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktyvus</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Pirmininkas</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Iš Dievo žodžio lobyno</translation>
    </message>
    <message>
        <source>Digging for Spiritual Gems</source>
        <translation>Ką vertinga radome</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Biblijos skaitymas</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Pirmas pokalbis</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Pakartotinis aplankymas</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Biblijos studijos</translation>
    </message>
    <message>
        <source>Prepare This Month&#x27;s Presentations</source>
        <translation>Ruoškimės pristatyti šio mėnesio leidinius</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Esame krikščionys. Kalbos</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Bendruomenės Biblijos studijos</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Bendr. Biblijos studijos skaitovas</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Naujas skelbėjas</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Malda</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation>Kas kviečia kalbėtoją pietų</translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation>Mobilusis telefonas</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Šokiadienio sueiga</translation>
    </message>
    <message>
        <source>Talk</source>
        <translation>Kalba</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Savaitgalio sueiga</translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation>Sargybos bokšto vadovas</translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation>Sargybos bokšto skaitovas</translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Skelbėjai</translation>
    </message>
</context>
<context>
    <name>SchoolSchedule</name>
    <message>
        <source>Main</source>
        <translation>Pagrindinis</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Sekantis</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Trečia</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation>Pasirinkimų sąrašas</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Nustatymai</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Atsijungti</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Informacija</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versija</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBasės interneto svetainė</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Atsilėpimai</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Paskutinį kartą sinchronizuota: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Tvarkaraštis</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Rodyti laiką</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Rodyti trukmę</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Vartotojo sąsaja</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Kalba</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Prisijungti</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>El.paštas</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Vardas</translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Sinchronizuoja...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation>Pirmininkas</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Giesmė</translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation>Savaitgalio sueigos pirmininkas</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation>Giesmė</translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation>Sargybos bokšto giesmė</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation>Sargybos bokšto numeris</translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>Straipsnio nr</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>Vadovas</translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>Skaitovas</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <source>Not set</source>
        <translation>Nenustatytas</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation>Laiko tekmė</translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation>Savaičių skaičius prieš pasirinktą datą</translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation>Savaičių skaičius po pasirinktos datos</translation>
    </message>
    <message>
        <source>weeks</source>
        <translation>savaitės</translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation>Savaitės, kurios papilkėja po užduoties</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Updates available. Do you want to synchronize?</source>
        <translation>Yra atnaujinimų. Ar norėtumėte sinchronizuoti?</translation>
    </message>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Pakeitimai rasti programoje ir TheocBase talpykloje (%1rows). Ar norite išsaugoti įrenginyje esančius pakeitimus?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation>Debesijos duomenys ištrinti. Jūsų įrenginyje esantys duomenys bus pakeisti. Tęsti?</translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Nepaskirti kitos pamokos</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Nenustatyta</translation>
    </message>
</context>
<context>
    <name>schoolview</name>
    <message>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>ĮP</translation>
    </message>
    <message>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>SK</translation>
    </message>
</context></TS>