<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sr">
<context>
    <name>CBStudySchedule</name>
    <message>
        <source>S</source>
        <comment>abbreviation of the &apos;study&apos; (Congregation Bible Study)</comment>
        <translation>SRB</translation>
    </message>
</context>
<context>
    <name>Details</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Ne dodeljuj sledeću govorničku veštinu</translation>
    </message>
    <message>
        <source>Leave on current study</source>
        <translation>Ponoviti govorničku veštinu</translation>
    </message>
    <message>
        <source>Invalid data</source>
        <translation>Nevažeći podatak</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Uneti vreme?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Izvor</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Učesnik</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Sagovornik</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Ishod</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Završeno</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Zamena</translation>
    </message>
    <message>
        <source>Select a volunteer</source>
        <translation>Izaberi zamenu</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Dodeljena govornička veština</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Vežba urađena</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Sledeća govornička veština</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Izaberi sledeću govorničku veštinu</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Vreme</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Beleške</translation>
    </message>
    <message>
        <source>School Details</source>
        <translation>Dodatne informacije</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Pokreni vreme</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Zaustavi vreme</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Okvir</translation>
    </message>
    <message>
        <source>Select setting</source>
        <comment>for demonstration</comment>
        <translation>Izaberi okvir</translation>
    </message>
</context>
<context>
    <name>LMMAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Izvor</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Voditelj</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation>Predavač</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Čitač</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Beleške</translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMSchedule_Mobile</name>
    <message>
        <source>TREASURES FROM GOD&#x27;S WORD</source>
        <translation>BLAGO IZ BOŽJE REČI</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>PRIPREMA ZA SLUŽBU PROPOVEDANJA</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation>HRIŠĆANSKI ŽIVOT</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Predsedavajući</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation>Savetnik</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Pesma br. %1 i molitva</translation>
    </message>
    <message>
        <source>Song</source>
        <translation>Pesma</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Voditelj</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Čitač</translation>
    </message>
    <message>
        <source>Main</source>
        <comment>Main class when auxiliary classes</comment>
        <translation>Glavni</translation>
    </message>
    <message>
        <source>Second</source>
        <comment>Auxiliary Class</comment>
        <translation>1. pomoćni</translation>
    </message>
    <message>
        <source>Third</source>
        <comment>Auxiliary Class</comment>
        <translation>2. pomoćni</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation>Uvodne reči</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation>Sažetak programa i najava za sledeću sedmicu</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Molitva</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMMStudentAssignmentDialog</name>
    <message>
        <source>Leave on current study</source>
        <translation>Ponoviti govorničku veštinu</translation>
    </message>
    <message>
        <source>Start stopwatch</source>
        <translation>Pokreni vreme</translation>
    </message>
    <message>
        <source>Stop stopwatch</source>
        <translation>Zaustavi vreme</translation>
    </message>
    <message>
        <source>Add the timing?</source>
        <translation>Uneti vreme?</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <source>Source</source>
        <translation>Izvor</translation>
    </message>
    <message>
        <source>Student</source>
        <translation>Učesnik</translation>
    </message>
    <message>
        <source>Setting</source>
        <comment>for demonstration</comment>
        <translation>Okvir</translation>
    </message>
    <message>
        <source>Result</source>
        <translation>Ishod</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Završeno</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation>Vreme</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation>Dodeljena govornička veština</translation>
    </message>
    <message>
        <source>Exercises Completed</source>
        <translation>Vežba urađena</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation>Sledeća govornička veština</translation>
    </message>
    <message>
        <source>Select next study</source>
        <translation>Izaberi sledeću govorničku veštinu</translation>
    </message>
    <message>
        <source>Notes</source>
        <translation>Beleške</translation>
    </message>
    <message>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>Sagovornik</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation>Zamena</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation>Štoperica</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>Sagovornik ne treba da bude osoba suprotnog pola.</translation>
    </message>
    <message>
        <source>Study point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Details</source>
        <comment>Page title</comment>
        <translation>Dodatne informacije</translation>
    </message>
</context>
<context>
    <name>ListHeader</name>
    <message>
        <source>Pull to refresh...</source>
        <translation>Povuci za osvežavanje programa...</translation>
    </message>
    <message>
        <source>Release to refresh...</source>
        <translation>Pusti za osvežavanje programa...</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Loading...</source>
        <translation>Učitavanje...</translation>
    </message>
    <message>
        <source>Login Failed</source>
        <translation>Prijavljivanje neuspešno</translation>
    </message>
    <message>
        <source>TheocBase Login</source>
        <translation>Prijavljivanje na TheocBase</translation>
    </message>
    <message>
        <source>Username</source>
        <translation>Korisničko ime</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Lozinka</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>Prijava</translation>
    </message>
    <message>
        <source>New User / Forgot password</source>
        <translation>Novi korisnik / Zaboravljena lozinka</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation>Korisničko ime ili imejl</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imejl</translation>
    </message>
    <message>
        <source>Create Account</source>
        <translation>Kreiraj novi nalog</translation>
    </message>
    <message>
        <source>Reset Password</source>
        <translation>Poništi lozinku</translation>
    </message>
    <message>
        <source>Email address not found!</source>
        <translation>Imejl adresa nije pronađena!</translation>
    </message>
    <message>
        <source>Forgot Password</source>
        <translation>Zaboravio sam lozinku</translation>
    </message>
    <message>
        <source>Login Page</source>
        <translation>Stranica za prijavljivanje</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Main</source>
        <translation>Glavni</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>1. pomoćni</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>2. pomoćni</translation>
    </message>
    <message>
        <source>Week starting %1</source>
        <translation>Sedmica od</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Skupštinsko razmatranje Biblije</translation>
    </message>
    <message>
        <source>Theocratic Ministry School</source>
        <translation>Teokratska škola propovedanja</translation>
    </message>
    <message>
        <source>Service Meeting</source>
        <translation>Poučavanje za hrišćansku službu</translation>
    </message>
    <message>
        <source>Public Talk</source>
        <translation>Javno predavanje</translation>
    </message>
    <message>
        <source>Watchtower Study</source>
        <translation>Razmatranje Stražarske kule</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation>Sastanak radnim danom</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation>Sastanak vikendom</translation>
    </message>
    <message>
        <source>Midweek</source>
        <comment>Midweek Meeting</comment>
        <translation>Radnim danom</translation>
    </message>
    <message>
        <source>Weekend</source>
        <comment>Weekend Meeting</comment>
        <translation>Vikendom</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakers</name>
    <message>
        <source>OUTGOING SPEAKERS</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%1 speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No speakers away this weekend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule_Mobile</name>
    <message>
        <source>Song and Prayer</source>
        <translation>Pesma i molitva</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation>Pesma br. %1 i molitva</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation>JAVNO PREDAVANJE</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation>RAZMATRANJE STRAŽARSKE KULE</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation>Pesma br. %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation>Voditelj</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation>Čitač</translation>
    </message>
    <message>
        <source>Import WT...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Congregation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Host</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Public Talk</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublisherDetail</name>
    <message>
        <source>First Name</source>
        <translation>Ime</translation>
    </message>
    <message>
        <source>Last Name</source>
        <translation>Prezime</translation>
    </message>
    <message>
        <source>Brother</source>
        <translation>Brat</translation>
    </message>
    <message>
        <source>Sister</source>
        <translation>Sestra</translation>
    </message>
    <message>
        <source>Servant</source>
        <translation>Sluga</translation>
    </message>
    <message>
        <source>Family</source>
        <translation>Porodica</translation>
    </message>
    <message>
        <source>Family member linked to</source>
        <translation>Član porodice</translation>
    </message>
    <message>
        <source>Contact Information</source>
        <translation>Kontakt</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Broj telefona</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Imejl</translation>
    </message>
    <message>
        <source>Use for School</source>
        <translation>Odobreno učešće</translation>
    </message>
    <message>
        <source>All Classes</source>
        <translation>Svi razredi</translation>
    </message>
    <message>
        <source>Only Main Class</source>
        <translation>Samo glavni razred</translation>
    </message>
    <message>
        <source>Only Auxiliary Classes</source>
        <translation>Samo pomoćni razred</translation>
    </message>
    <message>
        <source>Highlights</source>
        <translation>Izabrane misli</translation>
    </message>
    <message>
        <source>No 1</source>
        <translation>Zadatak br. 1</translation>
    </message>
    <message>
        <source>No 2</source>
        <translation>Zadatak br. 2</translation>
    </message>
    <message>
        <source>No 3</source>
        <translation>Zadatak br. 3</translation>
    </message>
    <message>
        <source>Break</source>
        <translation>Pauza</translation>
    </message>
    <message>
        <source>Family Head</source>
        <translation>Poglavar porodice</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation>Sagovornik</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Informacije</translation>
    </message>
    <message>
        <source>Call %1?</source>
        <comment>Call to phone number</comment>
        <translation>Pozovi %1</translation>
    </message>
    <message>
        <source>Active</source>
        <translation>Aktivan</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation>Predsedavajući</translation>
    </message>
    <message>
        <source>Treasures from God&#x27;s Word</source>
        <translation>Blago iz Božje Reči</translation>
    </message>
    <message>
        <source>Digging for Spiritual Gems</source>
        <translation>Pronađimo dragulje u Božjoj Reči</translation>
    </message>
    <message>
        <source>Bible Reading</source>
        <translation>Čitanje Biblije</translation>
    </message>
    <message>
        <source>Initial Call</source>
        <translation>Prvi razgovor</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <translation>Naknadna poseta</translation>
    </message>
    <message>
        <source>Bible Study</source>
        <translation>Biblijski kurs</translation>
    </message>
    <message>
        <source>Prepare This Month&#x27;s Presentations</source>
        <translation>Predlozi za ponudu literature</translation>
    </message>
    <message>
        <source>Living as Christians Talks</source>
        <translation>Hrišćanski život – Govori</translation>
    </message>
    <message>
        <source>Congregation Bible Study</source>
        <translation>Skupštinsko razmatranje Biblije</translation>
    </message>
    <message>
        <source>Cong. Bible Study Reader</source>
        <translation>Čitač na Skup. razmatranju Biblije</translation>
    </message>
    <message>
        <source>New publisher</source>
        <translation>Novi objavitelj</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation>Molitva</translation>
    </message>
    <message>
        <source>Host for Public Speakers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mobile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Talk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Conductor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Study Reader</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PublishersPage</name>
    <message>
        <source>Publishers</source>
        <translation>Objavitelji</translation>
    </message>
</context>
<context>
    <name>SchoolSchedule</name>
    <message>
        <source>Main</source>
        <translation>Glavni</translation>
    </message>
    <message>
        <source>Second</source>
        <translation>Pomoćni 1.</translation>
    </message>
    <message>
        <source>Third</source>
        <translation>Pomoćni 2.</translation>
    </message>
</context>
<context>
    <name>SelectionListPage</name>
    <message>
        <source>Selection List</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <comment>application settings</comment>
        <translation>Postavke</translation>
    </message>
    <message>
        <source>Logout</source>
        <translation>Odjava</translation>
    </message>
    <message>
        <source>Info</source>
        <translation>Informacije</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Verzija</translation>
    </message>
    <message>
        <source>TheocBase Homepage</source>
        <translation>TheocBase Početna stranica</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation>Povratne informacije</translation>
    </message>
    <message>
        <source>Last synchronized: %1</source>
        <translation>Poslednja sinhronizacija: %1</translation>
    </message>
    <message>
        <source>Schedule</source>
        <translation>Raspored</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation>Prikaži vreme</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation>Prikaži trajanje</translation>
    </message>
    <message>
        <source>User Interface</source>
        <translation>Korisničko radno okruženje</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Jezik</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SynchronizePage</name>
    <message>
        <source>Synchronizing...</source>
        <translation>Sinhronizacija u toku...</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <source>Chairman</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Weekend Meeting Chairman</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <source>Song</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Watchtower Song</source>
        <comment>Page title</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <source>Watchtower Issue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <source>Not set</source>
        <translation>Nije podešeno</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <source>Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks before selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks after selected date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>weeks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of weeks to gray after an assignment</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>Updates available. Do you want to synchronize?</source>
        <translation>Dostupna je nova verzija softvera. Da li želite da pokrenete sinhronizaciju?</translation>
    </message>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation>Promene su vidljive na uređaju koji koristite i na serveru (%1 redovi). Da li želite da sačuvate promene napravljene na uređaju koji koristite?</translation>
    </message>
    <message>
        <source>The cloud data has been reset. Your local data will be replaced. Continue?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>school_detail</name>
    <message>
        <source>Do not assign the next study</source>
        <translation>Ne dodeljuj sledeću govorničku veštinu</translation>
    </message>
    <message>
        <source>Not set</source>
        <translation>Nije podešeno</translation>
    </message>
</context>
<context>
    <name>schoolview</name>
    <message>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>IM</translation>
    </message>
    <message>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>Č</translation>
    </message>
</context></TS>