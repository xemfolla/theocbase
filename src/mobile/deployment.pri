android-no-sdk {
    target.path = /data/user/qt
    export(target.path)
    INSTALLS += target
} else:android {
    x86 {
        target.path = /libs/x86
    } else: armeabi-v7a {
        target.path = /libs/armeabi-v7a
    } else {
        target.path = /libs/armeabi
    }
    export(target.path)
    INSTALLS += target
    QT += androidextras
} else:unix {
    isEmpty(target.path) {
        qnx {
            target.path = /tmp/$${TARGET}/bin
        } else {
            target.path = /opt/$${TARGET}/bin
        }
        export(target.path)
    }
    INSTALLS += target
}

ios {
    # Copy all png-files into bundle
    BUNDLE_DATA.files = $$system("find $$PWD/iOS/ -name '*.png'")
    BUNDLE_DATA.files += $$files($$PWD/iOS/LaunchScreen_ios.xib)

    #BUNDLE_DATA.files += $$PWD/iOS/Icon.png
    #BUNDLE_DATA.files += $$PWD/iOS/Default.png
    #BUNDLE_DATA.files += $$PWD/iOS/Default-568h@2x.png

    #BUNDLE_DATA.files += $$PWD/iOS/Icon~ipad.png
    #BUNDLE_DATA.files += $$PWD/iOS/Icon-76.png
    #BUNDLE_DATA.files += $$PWD/iOS/Icon-76@2x.png

    QMAKE_BUNDLE_DATA += BUNDLE_DATA
    QMAKE_ASSET_CATALOGS = $$PWD/iOS/Images.xcassets
    QMAKE_ASSET_CATALOGS_APP_ICON = "AppIcon"

    # Write version number into iosInfo.plist file
    BUILD_TIME = $$system("date +%s")
    QMAKE_POST_LINK += /usr/libexec/PlistBuddy -c \"Set :CFBundleShortVersionString $${VERSION}\" $${PWD}/iOS/iosInfo.plist
    QMAKE_POST_LINK += ;/usr/libexec/PlistBuddy -c \"Set :CFBundleVersion $${BUILD_TIME}\" $${PWD}/iOS/iosInfo.plist

    # Use iosInfo.plist file
    QMAKE_INFO_PLIST = iOS/iosInfo.plist    

    xcode_product_bundle_identifier_setting.value = "net.theocbase.mobile"
    QMAKE_TARGET_BUNDLE_PREFIX = "net.theocbase"
    QMAKE_BUNDLE = "mobile"

    OTHER_FILES += iOS/Images.xcassets/AppIcon.appiconset/*.png \
        iOS/Images.xcassets/AppIcon.appiconset/Contents.json \
        iOS/LaunchScreen_ios.xib \
        iOS/LaunchBackground.png
}

android {
    defineReplace(droidVersionCode) {
            segments = $$split(1, ".")
            # for (segment, segments): vCode = "$$first(vCode)$$format_number($$segment, width=1 zeropad)"
            for (segment, segments): vCode = "$$first(vCode)$$segment"

            contains(ANDROID_TARGET_ARCH, arm64-v8a): \
                suffix = 1
            else:contains(ANDROID_TARGET_ARCH, armeabi-v7a): \
                suffix = 0            
            return($$first(vCode)$$first(suffix))
    }    
    ANDROID_VERSION_NAME = $$VERSION
    ANDROID_VERSION_CODE = $$droidVersionCode($$ANDROID_VERSION_NAME)
    message($$ANDROID_VERSION_CODE)

    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
    OTHER_FILES += $$PWD/android/src/net/theocbase/mobile/TBActivity.java \
    android/res/layout/tb_splash.xml \
    android/res/drawable-hdpi/icon.png  \
    android/res/drawable-hdpi/splash.9.png \
    android/res/drawable-mdpi/icon.png  \
    android/res/drawable-mdpi/splash.9.png \
    android/res/drawable-xhdpi/icon.png  \
    android/res/drawable-xhdpi/splash.9.png \
    android/res/drawable-xxhdpi/icon.png  \
    android/res/drawable-xxhdpi/splash.9.png \
    android/res/drawable-xxxhdpi/icon.png  \
    android/res/drawable-xxxhdpi/splash.9.png
}
winrt {
#CONFIG(debug, debug|release) {
#    DESTDIR = build_winrt/debug
#} else {
#    DESTDIR = build_winrt/release
#}
    WINRT_MANIFEST.publisher = TheocBase
    #
    WINRT_MANIFEST.description = Mobile version of TheocBase to view and edit TMS schedule and publisher details.
    WINRT_MANIFEST.logo_large = winrt/assets/logo_150x150.png
    WINRT_MANIFEST.logo_medium = winrt/assets/logo_71x71.png
    WINRT_MANIFEST.logo_small = winrt/assets/logo_44x44.png
    WINRT_MANIFEST.logo_store = winrt/assets/logo_120x120.png
    WINRT_MANIFEST.splash_screen = winrt/assets/logo_480x800.png
    WINRT_MANIFEST.wide = winrt/assets/logo_310x150.png
    WINRT_MANIFEST.capabilities += internetClientServer
    WINRT_MANIFEST.background = $${LITERAL_HASH}00a2ff
    WINRT_MANIFEST.version = 2015.7.2.2
    DLLDESTDIR = $$OUT_PWD/build_winrt
    #QMAKE_POST_LINK = cp $$PWD/winrt/assets/*.png
}

export(INSTALLS)
