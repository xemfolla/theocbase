#ifndef MESSAGEBOX_H
#define MESSAGEBOX_H

#include <QObject>

class MessageBox : public QObject
{
    Q_OBJECT
public:
    MessageBox();
    Q_INVOKABLE void show(QString title, QString message);
    Q_INVOKABLE void showYesNo(QString title, QString message, int id = 0);
signals:
    void buttonClicked(bool ok, int id);
public slots:

private:
};

#endif // MESSAGEBOX_H
