﻿#okNextError Any
INSERT INTO languages (id,language,desc,code, uuid) SELECT (SELECT MAX(id)+1 FROM languages),'Guadeloupean Creole','Kréyòl Gwadloup (Guadeloupean Creole)','gcf','08af6161-9ffa-75bc-5308-1f8c2b4ddf34' WHERE NOT EXISTS (SELECT 1 FROM languages WHERE code = 'gcf')
