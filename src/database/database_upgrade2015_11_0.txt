﻿delete from lmm_workbookregex where lang = 'en'
INSERT INTO lmm_workbookregex VALUES ("en","song","Song\s*(\d+)(.*)",null);
INSERT INTO lmm_workbookregex VALUES ("en","date","^(\D+)\s(\d+)\D*\s*(\d+)$","month;fromday;thruday");
INSERT INTO lmm_workbookregex VALUES ("en","assignment1","([^:]+):([^()]*)[(](\d+)\smin.([^[)]*)[)](.*)","theme;source1;timing;timingextra;source2");
