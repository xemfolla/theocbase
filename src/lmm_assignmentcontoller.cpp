#include "lmm_assignmentcontoller.h"

LMM_AssignmentContoller::LMM_AssignmentContoller(QObject *parent) : QObject(parent)
{
    m_assignment = nullptr;
    m_currentstudy = nullptr;
    m_nextstudy = nullptr;
}

LMM_Assignment_ex *LMM_AssignmentContoller::assignment() const
{
    return m_assignment;
}

void LMM_AssignmentContoller::setAssignment(LMM_Assignment_ex *a)
{
    m_assignment = a;
}

QPoint LMM_AssignmentContoller::getMousePosition() const
{
    qDebug() << "get mouse pos";
    return QCursor::pos();
}

QRect LMM_AssignmentContoller::getScreenAvailableGeometry() const
{
    QScreen *currentScreen = QGuiApplication::screenAt(QCursor::pos());
    return currentScreen->availableGeometry();
}

QPoint LMM_AssignmentContoller::getDialogPosition(int height, int width, int controlposx, int controlposy) const
{
    QPoint mousePos = QCursor::pos();
    QRect screenRect = getScreenAvailableGeometry();
    int x, y;
    x = mousePos.x() - controlposx;
    y = mousePos.y() - controlposy;
    if (y+height > screenRect.y() + screenRect.height() - 10)
        y = screenRect.y() + screenRect.height() - height - 10;
    if (x+width > screenRect.x() + screenRect.width() - 10)
        x = screenRect.x() + screenRect.width() - width - 10;
    return QPoint(x,y);
}

person *LMM_AssignmentContoller::getPublisherById(int id) const
{
    return cpersons::getPerson(id);
}

QVariant LMM_AssignmentContoller::getStudies()
{
    QStringList retList;
    retList.append(tr("Do not assign the next study"));
    school s;
    QList<schoolStudy*> slist = s.getStudies();
    for(schoolStudy *study : slist){
        retList.append(QVariant(study->getNumber()).toString() + " " + study->getName());
    }
    qDeleteAll(slist);
    slist.clear();
    return QVariant::fromValue(retList);
}

schoolStudentStudy *LMM_AssignmentContoller::getActiveStudy()
{    
    if (!m_assignment || !m_assignment->speaker()) return nullptr;

    school s;
    if (m_assignment->completed()){
        m_currentstudy = s.getStudyUsed(m_assignment->speaker()->id(),m_assignment->date(),
                                        m_assignment->talkId() == LMM_Schedule::TalkType_BibleReading ?
                                            school_item::Reading :
                                            (m_assignment->canHaveAssistant() ? school_item::Demonstration : school_item::Discource)
                                            );
    }else{
        m_currentstudy = s.getActiveStudyPoint(m_assignment->speaker()->id(),
                                               m_assignment->talkId() == LMM_Schedule::TalkType_BibleReading ?
                                                   school_item::Reading :
                                                   (m_assignment->canHaveAssistant() ? school_item::Demonstration : school_item::Discource),
                                               false, m_assignment->date());
    }

    if(m_currentstudy)
        m_currentstudy->setParent(this);

    if (m_currentstudy && !m_currentstudy->getEndDate().isNull()){
        QList<schoolStudentStudy *>studies = s.getStudentStudies(m_assignment->speaker()->id());
        foreach (schoolStudentStudy *study, studies){
            if (study->getStartdate() == m_currentstudy->getEndDate().addDays(1)){
                m_nextstudy = new schoolStudentStudy(this);
                m_nextstudy->setName(study->getName());
                m_nextstudy->setReading(study->getReading());
                m_nextstudy->setDemonstration(study->getDemonstration());
                m_nextstudy->setDiscourse(study->getDiscourse());
                m_nextstudy->setStartdate(study->getStartdate());
                m_nextstudy->setEndDate(study->getEndDate());
                m_nextstudy->setExercise(study->getExercise());
                m_nextstudy->setId(study->getId());
                m_nextstudy->setNumber(study->getNumber());
                m_nextstudy->setStudentId(study->getStudentId());
                break;
            }
        }
        qDeleteAll(studies);
        studies.clear();
    }
    return m_currentstudy;
}

int LMM_AssignmentContoller::nextStudy() const
{
    if(m_nextstudy){
        return m_nextstudy->getNumber();
    }else{
        if  (m_currentstudy && !m_currentstudy->getEndDate().isValid()){
            return m_currentstudy->getNumber();
        }else{
            return 0;
        }
    }
}

void LMM_AssignmentContoller::saveStudy(bool exerciseCompleted, bool assignmentCompleted, int next_study_no)
{
    if (!m_currentstudy){
        qDebug() << "no active study";
        return;
    }

    if (m_currentstudy->getId() < 1 && !m_assignment->volunteer()){
        // create a new study
        m_currentstudy->update();
    }

    qDebug() << "exercise" << m_currentstudy->getExercise() << exerciseCompleted;
    qDebug() << "completed" << m_assignment->completed() << assignmentCompleted;
    qDebug() << "nextstudy" << nextStudy() << next_study_no;
    if (m_currentstudy->getExercise() == exerciseCompleted &&
            (m_assignment->completed() == assignmentCompleted) &&
            (nextStudy() == next_study_no)){
        qDebug() << "saveStudy: no changes";
        return;
    }

    if (m_assignment->completed() && !m_assignment->volunteer()){
        qDebug() << "studySave: assignemnt completed";
        if (m_currentstudy->getExercise() != exerciseCompleted)
            m_currentstudy->setExercise(exerciseCompleted);
        ccongregation c;
        QDate fDate = m_assignment->date();
        QDate nDate = fDate.addDays(c.getMeetingDay(fDate,ccongregation::tms)-1);
        m_currentstudy->setEndDate(next_study_no != m_currentstudy->getNumber() ? nDate : QDate());
        m_currentstudy->update();

        // next study
        if ((m_nextstudy) && (m_nextstudy->getNumber() != next_study_no)){
            // next study changed or not assigned -> remove previous next study
            m_nextstudy->remove();
            m_nextstudy = nullptr;
            qDebug() << "next study removed";
        }
        if (!m_nextstudy && next_study_no > 0 && next_study_no != m_currentstudy->getNumber()){
            // add a new next study
            schoolStudentStudy *newNextStudy = new schoolStudentStudy(this);
            newNextStudy->setStudentId(m_assignment->speaker()->id());
            newNextStudy->setExercise(false);
            newNextStudy->setNumber(next_study_no);
            newNextStudy->setStartdate(nDate.addDays(1));
            newNextStudy->update();
            qDebug() << "new next study";
        }
    }else{
        qDebug() << "studySave: assignment is not completed";
        // remove end date from current study
        if (m_currentstudy->getEndDate().isValid()){
            m_currentstudy->setEndDate(QDate());
            m_currentstudy->update();
            qDebug() << "end date removed from current study";
        }
        // remove next study if exits
        if (m_nextstudy){
            m_nextstudy->remove();
            m_nextstudy = nullptr;
            qDebug() << "next study removed";
        }
    }
}

QString LMM_AssignmentContoller::getHistoryTooltip(int userid)
{
    auto tableWidgetSchoolTask = getSchoolHistory(userid, Qt::black, Qt::white);
    int rows = std::min(10, tableWidgetSchoolTask.rowCount()+1);
    QTextDocument doc;
    QTextCursor cursor(&doc);
    cursor.movePosition(QTextCursor::Start);

    QTextTableFormat format;
    format.setBorder(1);
    format.setBorderStyle(QTextFrameFormat::BorderStyle_Solid);
    format.setCellSpacing(0);
    format.setCellPadding(2);
    format.setMargin(0);
    QTextTable *table = cursor.insertTable(rows, 7,format);    

    table->cellAt(0, 0).firstCursorPosition().insertText(QObject::tr("Date"));
    table->cellAt(0, 1).firstCursorPosition().insertText("#");
    table->cellAt(0, 2).firstCursorPosition().insertText(QObject::tr("Assignment"));
    table->cellAt(0, 3).firstCursorPosition().insertText(QObject::tr("Note"));
    table->cellAt(0, 4).firstCursorPosition().insertText(QObject::tr("Time"));    
    table->cellAt(0, 5).firstCursorPosition().insertText(" ");
    table->cellAt(0, 6).firstCursorPosition().insertText(QObject::tr("Together", "The column header text to show partner in student assignment"));

    if (tableWidgetSchoolTask.rowCount())
    {
        for (int r = 1; r < rows; ++r)
        {
            for (int c = 0; c < 6; ++c)
            {
                auto item = tableWidgetSchoolTask.item(r-1,c);
                QTextCharFormat format;
                QFont font;
                font.setItalic(item.italic());
                font.setBold(item.bold());
                font.setStrikeOut(item.strikeOut());
                format.setFont(font);

                if (item.customBackground())
                    format.setBackground(item.background());
                if (item.customForeground())
                    format.setForeground(item.foreground());

                if (!item.iconName().isEmpty()) {
                    table->cellAt(r,c).firstCursorPosition().insertImage(item.iconName());
                    c++;
                }

                table->cellAt(r,c).firstCursorPosition().insertText(item.text(), format);
                table->cellAt(r,c).setFormat(format);
            }
        }
    }    
    return doc.toHtml();
}
