/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef OUTGOINGSPEAKERSMODEL_H
#define OUTGOINGSPEAKERSMODEL_H

#include <QAbstractTableModel>
#include "cpublictalks.h"
#include "todo.h"

class OutgoingSpeakersModel : public QAbstractTableModel
{
    Q_OBJECT

    Q_PROPERTY(int length READ rowCount NOTIFY modelChanged)
public:
    explicit OutgoingSpeakersModel(QObject *parent = nullptr);
    ~OutgoingSpeakersModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    QHash<int, QByteArray> roleNames() const;

    QVariant data(const QModelIndex &index, int role) const;

    Q_INVOKABLE QVariantMap get(int row) const;

    Q_INVOKABLE void loadList(const QDate d);

    Q_INVOKABLE void addRow(const int speakerId, const int congId, const int themeId);

    Q_INVOKABLE void removeRow(const int rowIndex);

    Q_INVOKABLE void editRow(const int row, const int speakerId, const int themeId, const int congId);

    Q_INVOKABLE bool moveToTodo(const int row);
signals:
    void modelChanged();

private:
    enum Roles {
        IdRole = Qt::UserRole,
        CongregationRole,
        CongregationIdRole,
        CongregationAddress,
        CongregationInfo,
        CongregationCircuit,
        SpeakerRole,
        SpeakerIdRole,
        TimeRole,
        ThemeRole,        
        ThemeNoRole,
        ThemeIdRole,
        DateRole,
    };
    QDate _date;
    QList<cpoutgoing*> _list;
};

#endif // OUTGOINGSPEAKERSMODEL_H
