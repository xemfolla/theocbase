import QtQuick 2.11
import QtQuick.Controls 2.4

Page {
    width: 480
    height: 640

    header: Label {
        rightPadding: 56
        leftPadding: 56
        text: "Page 3"
        topPadding: 66
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Label {
        text: "this is new"
        rightPadding: 56
        leftPadding: 56        
    }
}
