<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="my-MM">
<context>
    <name>AssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation type="vanished">အဓိကအကြောင်းအရာ</translation>
    </message>
    <message>
        <source>Source</source>
        <translation type="vanished">ရင်းမြစ်</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation type="vanished">သဘာပတိ</translation>
    </message>
    <message>
        <source>Speaker</source>
        <translation type="vanished">ဟောပြောသူ</translation>
    </message>
    <message>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation type="vanished">ရွေးထားတဲ့</translation>
    </message>
    <message>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation type="vanished">အားလုံး</translation>
    </message>
    <message>
        <source>Note</source>
        <translation type="vanished">မှတ်စုတို</translation>
    </message>
    <message>
        <source>Cancel</source>
        <comment>Cancel button</comment>
        <translation type="vanished">ဖျက်သိမ်းပါ</translation>
    </message>
</context>
<context>
    <name>AssignmentPanel</name>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="40"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="60"/>
        <source>Source</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="75"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="75"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="86"/>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation>ရွေးထားတဲ့</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="87"/>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>အားလုံး</translation>
    </message>
    <message>
        <location filename="../qml/AssignmentPanel.qml" line="106"/>
        <source>Note</source>
        <translation>မှတ်ချက်</translation>
    </message>
</context>
<context>
    <name>CBSDialog</name>
    <message>
        <source>Theme</source>
        <translation type="vanished">အဓိကအကြောင်းအရာ</translation>
    </message>
    <message>
        <source>Source</source>
        <translation type="vanished">ရင်းမြစ်</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation type="vanished">သင်တန်းကိုင်</translation>
    </message>
    <message>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation type="vanished">ရွေးထားတဲ့</translation>
    </message>
    <message>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation type="vanished">အားလုံး</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation type="vanished">စာဖတ်သူ</translation>
    </message>
    <message>
        <source>Note</source>
        <translation type="vanished">မှတ်စုတို</translation>
    </message>
    <message>
        <source>Cancel</source>
        <comment>Cancel button</comment>
        <translation type="vanished">ဖျက်သိမ်းပါ</translation>
    </message>
</context>
<context>
    <name>CBSPanel</name>
    <message>
        <location filename="../qml/CBSPanel.qml" line="39"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="51"/>
        <source>Source</source>
        <translation>_ူဒဖ</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="66"/>
        <source>Conductor</source>
        <translation>သင်တန်းကိုင်</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="72"/>
        <source>CBS conductor</source>
        <comment>Dropdown column title</comment>
        <translation>CBS သင်တန်းမှူး</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="73"/>
        <source>Any CL assignment</source>
        <comment>Dropdown column title</comment>
        <translation>ခရစ်ယာန်အသက်တာ တာဝန်</translation>
    </message>
    <message>
        <source>Selected</source>
        <comment>Dropdown column title</comment>
        <translation type="vanished">ရွေးထားတဲ့</translation>
    </message>
    <message>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation type="vanished">အားလုံး</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="86"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../qml/CBSPanel.qml" line="105"/>
        <source>Note</source>
        <translation>မှတ်ချက်</translation>
    </message>
</context>
<context>
    <name>ComboBoxTable</name>
    <message>
        <location filename="../qml/ComboBoxTable.qml" line="189"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../qml/ComboBoxTable.qml" line="195"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
</context>
<context>
    <name>CongregationMap</name>
    <message>
        <location filename="../qml/CongregationMap.qml" line="68"/>
        <source>Display congregation address</source>
        <comment>Display marker at the location of the congregation on the map</comment>
        <translation>အသင်းတော်လိပ်စာ ပြပါ</translation>
    </message>
</context>
<context>
    <name>DropboxSettings</name>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="76"/>
        <source>Are you sure you want to permanently delete your cloud data?</source>
        <translation>အွန်လိုင်း ဒေတာ အတည်ဖျက်ချင်တာ သေချာလား?</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="214"/>
        <source>Last synchronized: %1</source>
        <translation>နောက်ဆုံး တပြေးညီ ချိန်ညှိခဲ့ %1</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="218"/>
        <source>Synchronize</source>
        <translation>တပြေးညီ ချိန်ညှိမယ်</translation>
    </message>
    <message>
        <location filename="../qml/DropboxSettings.qml" line="230"/>
        <source>Delete Cloud Data</source>
        <translation>အွန်လိုင်းဒေတာ ဖျက်မယ်</translation>
    </message>
</context>
<context>
    <name>HelpViewer</name>
    <message>
        <location filename="../helpviewer.cpp" line="81"/>
        <source>TheocBase Help</source>
        <translation>TheocBase အကူအညီ</translation>
    </message>
    <message>
        <location filename="../helpviewer.cpp" line="82"/>
        <source>Unable to launch the help viewer (%1)</source>
        <translation>အကူအညီစာမျက်နှာ (%1) ကို ဖွင့်လို့မရ</translation>
    </message>
</context>
<context>
    <name>LMMNotesDialog</name>
    <message>
        <source>Notes</source>
        <translation type="vanished">မှတ်ချက်</translation>
    </message>
    <message>
        <source>Cancel</source>
        <comment>Cancel button</comment>
        <translation type="vanished">မလုပ်တော့ဘူး</translation>
    </message>
</context>
<context>
    <name>LMMNotesPanel</name>
    <message>
        <location filename="../qml/LMMNotesPanel.qml" line="42"/>
        <source>Notes</source>
        <translation>မှတ်စု</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment</name>
    <message>
        <location filename="../lmm_assignment.cpp" line="180"/>
        <source>Please find below details of your assignment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="181"/>
        <source>Date</source>
        <translation>ရက်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="182"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="183"/>
        <source>Assignment</source>
        <translation>တာဝန်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="184"/>
        <source>Theme</source>
        <translation>အဓိကအချက်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment.cpp" line="185"/>
        <source>Source material</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LMM_AssignmentContoller</name>
    <message>
        <location filename="../lmm_assignmentcontoller.cpp" line="54"/>
        <source>Do not assign the next study</source>
        <translation>နောက်သွန်သင်ချက်အတွက် တာဝန် မပေးပါနဲ့</translation>
    </message>
</context>
<context>
    <name>LMM_Assignment_ex</name>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="179"/>
        <source>Please find below details of your assignment:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="180"/>
        <source>Date</source>
        <translation>ရက်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="181"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="184"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="186"/>
        <source>Assistant</source>
        <translation>အကူ</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="188"/>
        <source>Assignment</source>
        <translation>တာဝန်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="191"/>
        <source>Study</source>
        <translation>သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="193"/>
        <source>Theme</source>
        <translation>အဓိကအချက်</translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="194"/>
        <source>Source material</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../lmm_assignment_ex.cpp" line="238"/>
        <source>See your be book</source>
        <comment>Counsel point is not known yet. See your &apos;Ministry School&apos; book.</comment>
        <translation>ကျောင်းစာအုပ်ကို ကြည့်ပါ</translation>
    </message>
</context>
<context>
    <name>LMM_Meeting</name>
    <message>
        <location filename="../lmm_meeting.cpp" line="607"/>
        <source>Enter source material here</source>
        <translation>ရင်းမြစ် အချက်အလက် ဒီနေရာမှာ ရိုက်ထည့်ပါ</translation>
    </message>
</context>
<context>
    <name>LMM_Schedule</name>
    <message>
        <location filename="../lmm_schedule.cpp" line="97"/>
        <location filename="../lmm_schedule.cpp" line="122"/>
        <source>Chairman</source>
        <comment>talk title</comment>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="98"/>
        <location filename="../lmm_schedule.cpp" line="123"/>
        <source>Treasures From God’s Word</source>
        <comment>talk title</comment>
        <translation>ကျမ်းစာထဲက အဖိုးတန်ဘဏ္ဍာများ</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="99"/>
        <location filename="../lmm_schedule.cpp" line="124"/>
        <source>Digging for Spiritual Gems</source>
        <comment>talk title</comment>
        <translation>ရှာဖွေလေ့လာ ဘုရားရေးရာရတနာ</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="100"/>
        <location filename="../lmm_schedule.cpp" line="125"/>
        <source>Bible Reading</source>
        <comment>talk title</comment>
        <translation>ကျမ်းစာဖတ်ရှုခြင်း</translation>
    </message>
    <message>
        <source>Prepare This Month’s Presentations</source>
        <comment>talk title</comment>
        <translation type="vanished">ဒီလအတွက် တင်ဆက်နည်းတွေကို ပြင်ဆင်ပါ</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="103"/>
        <location filename="../lmm_schedule.cpp" line="128"/>
        <source>Initial Call</source>
        <comment>talk title</comment>
        <translation>ဦးဆုံးတွေ့ဆုံချိန်</translation>
    </message>
    <message>
        <source>Return Visit</source>
        <comment>talk title</comment>
        <translation type="vanished">ပြန်လည်ပတ်မှု</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="101"/>
        <location filename="../lmm_schedule.cpp" line="126"/>
        <source>Sample Conversation Video</source>
        <comment>talk title</comment>
        <translation>နမူနာ စကားပြော ဗီဒီယို</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="102"/>
        <source>Apply Yourself to Reading and Teaching</source>
        <translation type="unfinished">ဖတ်ရှုခြင်းနဲ့ သွန်သင်ခြင်းမှာ ကြိုးစားပါ</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="104"/>
        <location filename="../lmm_schedule.cpp" line="129"/>
        <source>First Return Visit</source>
        <comment>talk title</comment>
        <translation>ဦးဆုံး ပြန်လည်ပတ်မှု</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="105"/>
        <location filename="../lmm_schedule.cpp" line="130"/>
        <source>Second Return Visit</source>
        <comment>talk title</comment>
        <translation>ဒုတိယအကြိမ် ပြန်လည်ပတ်မှု</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="106"/>
        <location filename="../lmm_schedule.cpp" line="131"/>
        <source>Third Return Visit</source>
        <comment>talk title</comment>
        <translation>တတိယအကြိမ်  ပြန်လည်ပတ်မှု</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="107"/>
        <location filename="../lmm_schedule.cpp" line="132"/>
        <source>Bible Study</source>
        <comment>talk title</comment>
        <translation>ကျမ်းစာသင်အံမှု</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="108"/>
        <location filename="../lmm_schedule.cpp" line="133"/>
        <source>Talk</source>
        <comment>talk title</comment>
        <translation>ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="109"/>
        <location filename="../lmm_schedule.cpp" line="134"/>
        <source>Living as Christians Talk 1</source>
        <comment>talk title</comment>
        <translation>ခရစ်ယာန်အသက်တာလမ်းစဉ် ဟောပြောချက် ၁</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="110"/>
        <location filename="../lmm_schedule.cpp" line="135"/>
        <source>Living as Christians Talk 2</source>
        <comment>talk title</comment>
        <translation>ခရစ်ယာန်အသက်တာလမ်းစဉ် ဟောပြောချက် ၂</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="111"/>
        <location filename="../lmm_schedule.cpp" line="136"/>
        <source>Living as Christians Talk 3</source>
        <comment>talk title</comment>
        <translation>ခရစ်ယာန်အသက်တာလမ်းစဉ် ဟောပြောချက် ၃</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="112"/>
        <location filename="../lmm_schedule.cpp" line="137"/>
        <source>Congregation Bible Study</source>
        <comment>talk title</comment>
        <translation>အသင်းတော်ကျမ်းစာလေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="113"/>
        <location filename="../lmm_schedule.cpp" line="138"/>
        <source>Circuit Overseer&apos;s Talk</source>
        <comment>talk title</comment>
        <translation>တိုက်နယ် ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../lmm_schedule.cpp" line="127"/>
        <source>Apply Yourself to Reading and Teaching</source>
        <comment>talk title</comment>
        <translation>ဖတ်ရှုခြင်းနဲ့ သွန်သင်ခြင်းမှာ ကြိုးစားပါ</translation>
    </message>
</context>
<context>
    <name>LifeMinistryMeetingSchedule</name>
    <message>
        <source>TREASURES FROM GOD&apos;S WORD</source>
        <translation type="vanished">ကျမ်းစာထဲက အဖိုးတန်ဘဏ္ဍာများ</translation>
    </message>
    <message>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation type="vanished">အမှုဆောင်လုပ်ငန်းမှာ ကျင့်သုံးပါ</translation>
    </message>
    <message>
        <source>LIVING AS CHRISTIANS</source>
        <translation type="vanished">ခရစ်ယာန်အသက်တာလမ်းစဥ်</translation>
    </message>
    <message>
        <source>Import Schedule...</source>
        <translation type="vanished">အစီအစဥ်ဇယား တင်သွင်းမယ်...</translation>
    </message>
    <message>
        <source>Chairman</source>
        <translation type="vanished">သဘာပတိ</translation>
    </message>
    <message>
        <source>Counselor</source>
        <translation type="vanished">အကြံပေးသူ</translation>
    </message>
    <message>
        <source>Song %1 and Prayer</source>
        <translation type="vanished">သီချင်း %1 နဲ့ ဆုတောင်းချက်</translation>
    </message>
    <message>
        <source>Opening Comments</source>
        <translation type="vanished">နိဒါန်း</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation type="vanished">ပြန်လည်သုံးသပ်ပါ။ နောက်အပတ်အတွက် ကြိုသုံးသပ်ပါ</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation type="vanished">သီချင်း %1</translation>
    </message>
    <message>
        <source>Main hall</source>
        <translation type="vanished">ပင်မ ခန်းမ</translation>
    </message>
    <message>
        <source>Auxiliary classroom 1</source>
        <translation type="vanished">အရန်စာသင်ခန်း ၁</translation>
    </message>
    <message>
        <source>Auxiliary classroom 2</source>
        <translation type="vanished">အရန်စာသင်ခန်း ၂</translation>
    </message>
    <message>
        <source>Main Class</source>
        <translation type="vanished">ပင်မသင်တန်း</translation>
    </message>
    <message>
        <source>Second Class</source>
        <translation type="vanished">ဒုတိယသင်တန်း</translation>
    </message>
    <message>
        <source>Third class</source>
        <translation type="vanished">တတိယသင်တန်း</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation type="vanished">သင်တန်းကိုင်</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation type="vanished">စာဖတ်သူ</translation>
    </message>
</context>
<context>
    <name>Login</name>
    <message>
        <location filename="../qml/Login.qml" line="61"/>
        <source>Username or Email</source>
        <translation>အသုံးပြုသူနာမည်/အီးမေးလ်</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="69"/>
        <source>Password</source>
        <translation>လျှို့ဝှက်စာသား</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="78"/>
        <source>Login</source>
        <translation>အကောင့်ဝင်</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="97"/>
        <source>Forgot Password</source>
        <translation>လျှို့ဝှက်စာသား မေ့သွား</translation>
    </message>
    <message>
        <location filename="../qml/Login.qml" line="111"/>
        <source>Create Account</source>
        <translation>အကောင့်အသစ်ဖွင့်မယ်</translation>
    </message>
</context>
<context>
    <name>LoginForm.ui</name>
    <message>
        <source>TheocBase</source>
        <translation type="vanished">TheocBase</translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="vanished">အသုံးပြုသူနာမည်</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="vanished">အဝင်</translation>
    </message>
</context>
<context>
    <name>MAC_APPLICATION_MENU</name>
    <message>
        <location filename="../main.cpp" line="62"/>
        <source>Services</source>
        <translation>၀န်ဆောင်မူများ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>Hide %1</source>
        <translation>%1 ကွယ်ထားမယ်</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="64"/>
        <source>Hide Others</source>
        <translation>တခြားဟာတွေ ကွယ်ထားမယ်</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="65"/>
        <source>Show All</source>
        <translation>အားလုံး ပြပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="66"/>
        <source>Preferences...</source>
        <translation>နှစ်သက်မှု...</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="67"/>
        <source>Quit %1</source>
        <translation>%1 ထွက်မယ်</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="68"/>
        <source>About %1</source>
        <translation>%1 အကြောင်း</translation>
    </message>
</context>
<context>
    <name>MWMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="76"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="95"/>
        <source>Auxiliary Classroom Counselor II</source>
        <translation>အရန်သွန်သင်သူ ၂</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingChairmanPanel.qml" line="115"/>
        <source>Auxiliary Classroom Counselor III</source>
        <translation>အရန်သွန်သင်သူ ၃</translation>
    </message>
</context>
<context>
    <name>MWMeetingModule</name>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="217"/>
        <source>TREASURES FROM GOD&apos;S WORD</source>
        <translation>ကျမ်းစာထဲက အဖိုးတန်ဘဏ္ဍာများ</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="225"/>
        <source>APPLY YOURSELF TO THE FIELD MINISTRY</source>
        <translation>အမှုဆောင်လုပ်ငန်းမှာ ကျင့်သုံးပါ</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="230"/>
        <source>LIVING AS CHRISTIANS</source>
        <translation>ခရစ်ယာန်အသက်တာလမ်းစဥ်</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="243"/>
        <source>Midweek Meeting</source>
        <translation>ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="255"/>
        <source>Import Schedule...</source>
        <translation>အစီအစဥ်ဇယား တင်သွင်းမယ်...</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="303"/>
        <location filename="../qml/MWMeetingModule.qml" line="504"/>
        <source>MH</source>
        <comment>abbreviation for main hall</comment>
        <translation>ပင်မ</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="304"/>
        <location filename="../qml/MWMeetingModule.qml" line="505"/>
        <source>A1</source>
        <comment>abbreviation for auxiliary classroom 1</comment>
        <translation>အရန်၁</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="305"/>
        <location filename="../qml/MWMeetingModule.qml" line="506"/>
        <source>A2</source>
        <comment>abbreviation for auxiliary classroom 2</comment>
        <translation>အရန်၂</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="314"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="314"/>
        <source>Counselor</source>
        <translation>အကြံပေးသူ</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="340"/>
        <location filename="../qml/MWMeetingModule.qml" line="392"/>
        <source>Song %1 and Prayer</source>
        <translation>သီချင်း %1 နဲ့ ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="355"/>
        <source>Opening Comments</source>
        <translation>နိဒါန်း</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="374"/>
        <source>Review Followed by Preview of Next Week</source>
        <translation>ပြန်လည်သုံးသပ်ခြင်းနဲ့ နောက်တစ်ပတ်အတွက် အကြိုသုံးသပ်ခြင်း</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="469"/>
        <source>Song %1</source>
        <translation>သီချင်း %1</translation>
    </message>
    <message>
        <source>Main hall</source>
        <translation type="vanished">ပင်မ ခန်းမ</translation>
    </message>
    <message>
        <source>Auxiliary classroom 1</source>
        <translation type="vanished">အရန်စာသင်ခန်း ၁</translation>
    </message>
    <message>
        <source>Auxiliary classroom 2</source>
        <translation type="vanished">အရန်စာသင်ခန်း ၂</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="490"/>
        <source>Conductor</source>
        <translation>သင်တန်းကိုင်</translation>
    </message>
    <message>
        <location filename="../qml/MWMeetingModule.qml" line="492"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
</context>
<context>
    <name>MWMeetingPrayerPanel</name>
    <message>
        <location filename="../qml/MWMeetingPrayerPanel.qml" line="44"/>
        <source>Prayer</source>
        <translation>ဆုတောင်းချက်</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>(No meeting)</source>
        <translation type="vanished">(အစည်းအဝေး မရှိပါ)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="418"/>
        <source>No meeting</source>
        <translation>အစည်းအဝေး မရှိပါ</translation>
    </message>
    <message>
        <source>Nothing to display</source>
        <translation type="vanished">ဘာမှ ပြစရာ မရှိပါ</translation>
    </message>
    <message>
        <source>Prayer:</source>
        <translation type="vanished">ဆုတောင်းချက်:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="617"/>
        <source>Conductor:</source>
        <translation>သင်တန်းကိုင်သူ:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="622"/>
        <source>Reader:</source>
        <translation>စာဖတ်သူ:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="467"/>
        <source>Copyright</source>
        <translation>မူပိုင်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="475"/>
        <source>Qt libraries licensed under the GPL.</source>
        <translation>Qt libraries licensed under the GPL.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="467"/>
        <source>TheocBase Team</source>
        <translation>TheocBase အဖွဲ့</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="278"/>
        <source>Last synchronized</source>
        <translation>နောက်ဆုံး တပြေးညီ ချိန်ညှိခဲ့</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="470"/>
        <source>Licensed under GPLv3.</source>
        <translation>Licensed under GPLv3.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="476"/>
        <source>Versions of Qt libraries </source>
        <translation>Versions of Qt libraries </translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="520"/>
        <location filename="../mainwindow.cpp" line="1345"/>
        <source>TheocBase data exchange</source>
        <translation>TheocBase အချက်အလက် ဖလှယ်ခြင်း</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="522"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="522"/>
        <source>Hello</source>
        <translation>မင်္ဂလာပါ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="522"/>
        <source>Best Regards</source>
        <translation>လေးစားစွာြဖင့်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="556"/>
        <source>New update available. Do you want to install?</source>
        <translation>အသစ် ထပ်ဆင့်ပြင်ဆင်မှု ရှိတယ်၊ သွင်းချင်လား?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="560"/>
        <source>No new update available</source>
        <translation>အသစ် ထပ်ဆင့်မြင့်မှု မရှိပါ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="616"/>
        <source>Enter e-mail address.</source>
        <translation>အီးမေးလ်လိပ်စာ ရိုက်ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="595"/>
        <source>Save file</source>
        <translation>ဖိုင် သိမ်းမယ်</translation>
    </message>
    <message>
        <source>TheocBase cloud synchronizing...</source>
        <translation type="vanished">TheocBase cloud တပြေးညီ ချိန်ညှိနေ...</translation>
    </message>
    <message>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want keep the local changes?</source>
        <translation type="vanished">တူတဲ့ အပြောင်းအလဲတွေကို ဒီစက်ထဲမှာရော ကလောက်စတိုးရေ့ခ်ျ ထဲမှာပါ တွေ့ရတယ် (%1 rows). ဒီစက်ထဲက အပြောင်းအလဲတွေကို ဆက်ထားမလား?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1717"/>
        <source>Select ePub file</source>
        <translation>ePub ဖိုင် ရွေးပါ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1630"/>
        <source>Send e-mail reminders?</source>
        <translation>အီးမေးလ် သတိပေးချက် ပို့မလား?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1664"/>
        <source>Updates available...</source>
        <translation>အသစ် ထပ်ဆင့်မြင့်မှုတွေ ရှိတယ်...</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="644"/>
        <location filename="../mainwindow.cpp" line="678"/>
        <location filename="../mainwindow.cpp" line="1248"/>
        <source>Error sending e-mail</source>
        <translation>အီးမေးလ် ပို့လို့မရဘူး ဖြစ်နေတယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="392"/>
        <source>WEEK STARTING %1</source>
        <translation>%1 နဲ့ စတဲ့ အပတ်</translation>
    </message>
    <message>
        <source>min</source>
        <comment>Abbreviation of minutes</comment>
        <translation type="vanished">မိ.</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="576"/>
        <source>Exporting outgoing speakers not ready yet, sorry.</source>
        <translation>သွားလည် ဟောပြောသူတွေ တင်ပို့ဖို့ အဆင်သင့် မဖြစ်သေးပါ၊ တောင်းပန်ပါတယ်။</translation>
    </message>
    <message>
        <source>Exporting speakers to iCal not ready yet, sorry.</source>
        <translation type="vanished">သွားလည် ဟောပြောသူတွေ iCal ကို တင်ပို့ဖို့ အဆင်သင့် မဖြစ်သေးပါ၊ တောင်းပန်ပါတယ်။</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="580"/>
        <source>Emailing iCal is not ready yet, sorry.</source>
        <translation>iCal ကို အီးမေးလ် ပို့လို့ အဆင်သင့် မဖြစ်သေးပါ၊ တောင်းပန်ပါတယ်။</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="584"/>
        <source>Exporting study history to iCal is not supported</source>
        <translation>သင်ခန်းစာမှတ်တမ်းကို iCal ဆီ ပို့လို့မရပါ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="599"/>
        <source>Save folder</source>
        <translation>ဖိုင်တွဲ သိမ်းမယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="680"/>
        <location filename="../mainwindow.cpp" line="1250"/>
        <source>E-mail sent successfully</source>
        <translation>အီးမေးလ် ရောက်သွားပြီ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="683"/>
        <source>Saved successfully</source>
        <translation>သိမ်းတာ အောင်မြင်တယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="743"/>
        <location filename="../mainwindow.cpp" line="748"/>
        <source>Counselor-Class II</source>
        <translation>သွန်သင်သူ အတန်း II</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="744"/>
        <location filename="../mainwindow.cpp" line="749"/>
        <source>Counselor-Class III</source>
        <translation>သွန်သင်သူ အတန်း III</translation>
    </message>
    <message>
        <source>Opening Prayer</source>
        <translation type="vanished">အဖွင့်ဆုတောင်းချက်</translation>
    </message>
    <message>
        <source>Concluding Prayer</source>
        <translation type="vanished">နိဂုံးချုပ်ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="764"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>%1 အတွက် အကူ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="831"/>
        <location filename="../mainwindow.cpp" line="1046"/>
        <location filename="../mainwindow.cpp" line="1071"/>
        <source>Kingdom Hall</source>
        <translation>နိုင်ငံတော်ခန်းမ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="742"/>
        <location filename="../mainwindow.cpp" line="747"/>
        <location filename="../mainwindow.cpp" line="837"/>
        <location filename="../mainwindow.cpp" line="848"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="771"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>အသင်းတော် ကျမ်းစာလေ့လာမှု အတွက် စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="756"/>
        <location filename="../mainwindow.cpp" line="772"/>
        <source>Source</source>
        <comment>short for Source material</comment>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="745"/>
        <location filename="../mainwindow.cpp" line="750"/>
        <location filename="../mainwindow.cpp" line="795"/>
        <location filename="../mainwindow.cpp" line="797"/>
        <source>Prayer</source>
        <translation>ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="756"/>
        <source>Timing</source>
        <translation>အချိန်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="837"/>
        <location filename="../mainwindow.cpp" line="1002"/>
        <source>Public Talk</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="837"/>
        <source>Watchtower Study</source>
        <translation>ကင်းမျှော်စင် လေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="854"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="859"/>
        <source>Watchtower Study Conductor</source>
        <translation>ကင်းမျှော်စင်လေ့လာမှု သင်တန်းကိုင်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="861"/>
        <source>Watchtower reader</source>
        <translation>ကင်းမျှော်စင် ဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1107"/>
        <source>The same changes can be found both locally and in the cloud (%1 rows). Do you want to keep the local changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1148"/>
        <source>The cloud data has now been deleted.</source>
        <translation>အွန်လိုင်း ဒေတာ ဖျက်လိုက်ပြီ။</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1152"/>
        <source>Synchronize</source>
        <translation>တပြေးညီ ဆောင်ရွက်မယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1154"/>
        <source>Sign Out</source>
        <translation>ထွက်မယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1164"/>
        <source>The cloud data has been deleted. Your local data will be replaced. Continue?</source>
        <translation>အွန်လိုင်းဒေတာ ဖျက်လိုက်ပြီ။ သင့်ကွန်ပျူတာမှာရှိတဲ့ ဒေတာကို အစားထိုးတော့မယ်။ ဆက်လုပ်မလား?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1259"/>
        <source>Open file</source>
        <translation>ဖိုင် ဖွင့်မယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1286"/>
        <source>Open directory</source>
        <translation>ဖိုင်တွဲ ဖွင့်မယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1317"/>
        <source>Import Error</source>
        <translation>တင်သွင်းလို့ မရပါ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1317"/>
        <source>Could not import from Ta1ks. Files are missing:</source>
        <translation>Ta1ks ကနေ တင်သွင်းလို့ မရဘူး။ ဖိုင်တွေ ပျောက်နေတယ်၊</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1332"/>
        <source>Save unsaved data?</source>
        <translation>မသိမ်းရသေးတဲ့ အချက်အလက် သိမ်းမလား?</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1341"/>
        <source>Import file?</source>
        <translation>ဖိုင် တင်သွင်းမလား?</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="691"/>
        <source>School assignment</source>
        <translation>ကျောင်းတာဝန်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="293"/>
        <source>Congregation Bible Study:</source>
        <translation>အသင်းတော် ကျမ်းစာလေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="327"/>
        <source>Theocratic Ministry School:</source>
        <translation>သီအိုကရက်တစ် ဓမ္မအမှုကျောင်း</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="358"/>
        <source>Service Meeting:</source>
        <translation>လုပ်ငန်းတော်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="806"/>
        <source>Congregation Bible Study</source>
        <translation>အသင်းတော် ကျမ်းစာလေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="833"/>
        <source>Theocratic Ministry School</source>
        <translation>သီအိုကရက်တစ် ဓမ္မအမှုကျောင်း</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="860"/>
        <source>Service Meeting</source>
        <translation>လုပ်ငန်းတော်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1103"/>
        <location filename="../mainwindow.ui" line="1427"/>
        <source>Export</source>
        <translation>ထုတ်ရန်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1177"/>
        <location filename="../mainwindow.ui" line="1383"/>
        <source>E-mail</source>
        <translation>အီးမေးလ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1406"/>
        <source>Recipient:</source>
        <translation>လက်ခံသူ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1392"/>
        <source>Subject: </source>
        <translation>အကြောင်းအရာ </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1303"/>
        <source>Public talks</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1457"/>
        <source>Import</source>
        <translation>ထည့်သွင်းရန်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1491"/>
        <source>info</source>
        <translation>အချက်အလက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2066"/>
        <source>Data exhange</source>
        <translation>အချက်အလက် ဖလှယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2128"/>
        <source>TheocBase Cloud</source>
        <translation>TheocBase Cloud</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1650"/>
        <location filename="../mainwindow.ui" line="1683"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1882"/>
        <source>Timeline</source>
        <translation>အချိန်ဇယား</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1943"/>
        <source>Print...</source>
        <translation>စာရွက်ပရင့်ထုတ်မယ်...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1946"/>
        <source>Print</source>
        <translation>စာရွက်ပရင့်ထုတ်မယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1955"/>
        <source>Settings...</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>ချိန်ညှိစရာ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1967"/>
        <source>Publishers...</source>
        <translation>ကြေညာသူ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1246"/>
        <location filename="../mainwindow.ui" line="1970"/>
        <source>Publishers</source>
        <translation>ကြေညာသူများ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2036"/>
        <source>Speakers...</source>
        <translation>ဟောပြောသူများ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1283"/>
        <location filename="../mainwindow.ui" line="2039"/>
        <source>Speakers</source>
        <translation>ဟောပြောသူများ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2063"/>
        <source>Data exhange...</source>
        <translation>အချက်အလက် ဖလှယ်...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2075"/>
        <source>TheocBase help...</source>
        <translation>TheocBase အကူအညီ...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2090"/>
        <source>History</source>
        <translation>မှတ်တမ်း</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2102"/>
        <location filename="../mainwindow.ui" line="2105"/>
        <source>Full Screen</source>
        <translation>မျက်နှာပြင်အပြည့် ဖွင့်/ပိတ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2110"/>
        <source>Startup Screen</source>
        <translation>အစစာမျက်နှာ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2119"/>
        <source>Reminders...</source>
        <translation>သတိပေးချက်များ...</translation>
    </message>
    <message>
        <source>&lt;--</source>
        <translation type="vanished">&lt;--</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="568"/>
        <source>Theme:</source>
        <translation>အကြောင်းအရာ:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="573"/>
        <source>Speaker:</source>
        <translation>ဟောပြောသူ:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="578"/>
        <source>Chairman:</source>
        <translation>သဘာပတိ:</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="583"/>
        <source>Watchtower Study:</source>
        <translation>ကင်းမျှော်စင်လေ့လာမှု:</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="529"/>
        <source>Data exchange</source>
        <translation>Data exchange</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1114"/>
        <source>Export Format</source>
        <translation>Export Format</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1120"/>
        <source>For sending data to another user</source>
        <translation>အသုံးပြုသူ နောက်တစ်ယောက်ကို အချက်အလက် ပို့ဖို့အတွက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1139"/>
        <source>For easy import to Calendar programs</source>
        <translation>ပြက္ခဒိန် အစီအစဥ် တင်သွင်းရတာ လွယ်အောင်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1171"/>
        <source>Export Method</source>
        <translation>တင်ပို့မယ့် နည်းလမ်း</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1190"/>
        <source>Save to File</source>
        <translation>ဖိုင်ထဲ သိမ်းမယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1213"/>
        <source>Events grouped by date</source>
        <translation>ရက်စွဲအလိုက် အစီအစဥ်များ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1220"/>
        <source>All day events</source>
        <translation>တစ်နေကုန် အစီအစဥ်များ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1236"/>
        <source>Midweek Meeting</source>
        <translation>ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1259"/>
        <source>Study History</source>
        <translation>သွန်သင်ချက် မှတ်တမ်း</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1310"/>
        <source>Outgoing Talks</source>
        <translation>သွားလည် ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1321"/>
        <source>Date Range</source>
        <translation>ရက်စွဲ အကြာအရှည်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1328"/>
        <source>Previous Weeks</source>
        <translation>အရင် အပတ်တွေ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1342"/>
        <source>From Date</source>
        <translation>ဒီရက်စွဲကစပြီး</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1359"/>
        <source>Thru Date</source>
        <translation>ဒီရက်စွဲအထိ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1836"/>
        <source>File</source>
        <translation>ဖိုင်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1842"/>
        <source>Tools</source>
        <translation>ကိရိယာများ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1855"/>
        <source>Help</source>
        <translation>အကူအညီ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1931"/>
        <source>Today</source>
        <translation>ဒီနေ့</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1979"/>
        <source>Exit</source>
        <translation>ထွက်မယ်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1991"/>
        <source>Report bug...</source>
        <translation>ပြဿနာ သတင်းပို့မယ်...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2003"/>
        <source>Send feedback...</source>
        <translation>အကြံပေးစာ ပို့မယ်...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2021"/>
        <source>About TheocBase...</source>
        <translation>TheocBase အကြောင်း...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2048"/>
        <source>Check updates...</source>
        <translation>အသစ်တင်ထားတာတွေ စစ်မယ်...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2078"/>
        <source>F1</source>
        <translation>F1</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2146"/>
        <source>Territories...</source>
        <translation>ရပ်ကွက်...</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2149"/>
        <source>Territories</source>
        <translation>ရပ်ကွက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1910"/>
        <source>Back</source>
        <translation>အနောက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="1922"/>
        <source>Next</source>
        <translation>အရှေ့</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2137"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="2012"/>
        <source>TheocBase website</source>
        <translation>TheocBase ဝဘ်ဆိုက်</translation>
    </message>
    <message>
        <source>(Circuit overseer visit)</source>
        <translation type="vanished">(တိုက်နယ်ကြီးကြပ်မှုး လည်ပတ်မှု)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="414"/>
        <source>Convention week (no meeting) </source>
        <translation>ခရိုင်စည်းဝေး အပတ် (အသင်းတော်စည်းဝေး မရှိ) </translation>
    </message>
    <message>
        <location filename="../mainwindow.ui" line="534"/>
        <source>Public Talk:</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakerEdit</name>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="70"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="94"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="121"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="147"/>
        <source>Info</source>
        <translation>အခ်က္အလက္</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="159"/>
        <source>Address</source>
        <translation>လိပ်စာ</translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakerEdit.qml" line="172"/>
        <source>Meeting day and time</source>
        <translation>အစည်းအဝေး နေ့နဲ့ အချိန်</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModel</name>
    <message>
        <location filename="../outgoingspeakersmodel.cpp" line="163"/>
        <source>From %1</source>
        <translation>%1 ကနေ</translation>
    </message>
</context>
<context>
    <name>OutgoingSpeakersModule</name>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="109"/>
        <source>Outgoing speakers</source>
        <translation>သွားလည် ဟောပြောသူ</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/OutgoingSpeakersModule.qml" line="193"/>
        <source>%1 speakers away this weekend</source>
        <translation>
            <numerusform>ဒီအပတ်ကုန်မှာ ဟောပြောသူ %1 ယောက် မရှိ</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/OutgoingSpeakersModule.qml" line="194"/>
        <source>No speakers away this weekend</source>
        <translation>ဒီစနေတနင်္ဂနွေမှာ ဟောပြောသူအားလုံး ရှိ</translation>
    </message>
</context>
<context>
    <name>Permission</name>
    <message>
        <source>Edit Settings</source>
        <comment>Access Control</comment>
        <translation type="vanished">_ဒဗိဩဂိ၇ာိ</translation>
    </message>
</context>
<context>
    <name>PublicMeetingSchedule</name>
    <message>
        <source>Song %1 and Prayer</source>
        <translation type="vanished">သီချင်း %1 နဲ့ ဆုတောင်းချက်</translation>
    </message>
    <message>
        <source>Song and Prayer</source>
        <translation type="vanished">သီချင်: နဲ့ ဆုတောင်းချက်</translation>
    </message>
    <message>
        <source>PUBLIC TALK</source>
        <translation type="vanished">လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <source>WATCHTOWER STUDY</source>
        <translation type="vanished">ကင်းမျှော်စင်လေ့လာမှု</translation>
    </message>
    <message>
        <source>Song %1</source>
        <translation type="vanished">သီချင်း %1</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation type="vanished">သင်တန်းကိုင်</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation type="vanished">စာဖတ်သူ</translation>
    </message>
</context>
<context>
    <name>PublicTalkPanel</name>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="48"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="72"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="94"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="128"/>
        <source>Mobile</source>
        <translation>မိုဘိုင်းဖုန်း</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="140"/>
        <source>Phone</source>
        <translation>ဖုန်း</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="152"/>
        <source>Email</source>
        <translation>အီးမေးလ်</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="164"/>
        <source>Info</source>
        <translation>အချက်အလက်</translation>
    </message>
    <message>
        <location filename="../qml/PublicTalkPanel.qml" line="177"/>
        <source>Host</source>
        <translation>အိမ်ရှင်</translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../main.cpp" line="69"/>
        <source>Yes</source>
        <translation>ဟုတ်ကဲ့ပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="70"/>
        <source>&amp;Yes</source>
        <translation>&amp;ဟုတ်ကဲ့ပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="71"/>
        <source>No</source>
        <translation>မဟုတ်ပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="72"/>
        <source>&amp;No</source>
        <translation>&amp;မဟုတ်ပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="73"/>
        <source>Cancel</source>
        <translation>ဖျက်သိမ်းပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="74"/>
        <source>&amp;Cancel</source>
        <translation>&amp;ဖျက်သိမ်းပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="75"/>
        <source>Save</source>
        <translation>သိမ်းမယ်</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="76"/>
        <source>&amp;Save</source>
        <translation>&amp;သိမ်းမယ်</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="77"/>
        <source>Open</source>
        <translation>ဖိုင် ဖွင့်မယ်</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="159"/>
        <source>Wrong username and/or password</source>
        <translation>အသုံးပြုသူနာမည်/လျှို့ဝှက်စာသား မှားနေတယ်</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="267"/>
        <source>Database not found!</source>
        <translation>အချက်အလက်ရင်းမြစ် ရှာမတွေ့!</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="270"/>
        <source>Choose database</source>
        <translation>အချက်အလက်ရင်းမြစ် ရွေးပါ</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="270"/>
        <source>SQLite files (*.sqlite)</source>
        <translation>SQLite files (*.sqlite)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="294"/>
        <source>Database restoring failed</source>
        <translation>Database restoring failed</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="325"/>
        <location filename="../mainwindow.cpp" line="224"/>
        <source>Save changes?</source>
        <translation>ပြောင်းထားတာတွေ သိမ်းမလား?</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="263"/>
        <source>Database copied to </source>
        <translation>အချက်အလက်ရင်းမြစ် ကူးလိုက်ပြီ </translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="37"/>
        <source>Database file not found! Searching path =</source>
        <translation>အချက်အလက်ရင်းမြစ် ရှာမတွေ့! ဖိုင်လမ်းကြောင်း ရှာနေ =</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="56"/>
        <source>Database Error</source>
        <translation>အချက်အလက်ရင်းမြစ် ပြဿနာ</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="706"/>
        <source>This version of the application (%1) is older than the database (%2). There is a strong probability that error messages will popup and changes may not be saved correctly. Please download and install the latest version for best results.</source>
        <translation>ဒီဗားရှင်း (%1) က ရင်းမြစ် (%2) ထက် နောက်ကျနေပြီ။ ချွတ်ယွင်းကြောင်းအသိပေးတဲ့ စာတွေတက်လာနိုင်ပြီး ပြောင်းလိုက်တာတွေကို သိမ်းလို့မရဘဲ ဖြစ်နေနိုင်တယ်။ အကောင်းဆုံးရလဒ် ရဖို့ နောက်ဆုံးထွက် ဗားရှင်းကို ကူးယူပြီး သွင်းလိုက်ပါ။</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="344"/>
        <location filename="../mainwindow.cpp" line="1003"/>
        <location filename="../publictalkedit.cpp" line="70"/>
        <location filename="../sql_class.cpp" line="1225"/>
        <location filename="../sql_class.cpp" line="1226"/>
        <source>Congregation</source>
        <translation type="unfinished">အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="345"/>
        <source>Circuit</source>
        <translation>တိုက်နယ်</translation>
    </message>
    <message>
        <location filename="../sql_class.cpp" line="748"/>
        <source>Database updated</source>
        <translation>အချက်အလက်ရင်းမြစ် အသစ်တင်ပြီးပြီ</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="314"/>
        <location filename="../ccongregation.cpp" line="340"/>
        <source>Circuit Overseer&apos;s visit</source>
        <translation>တိုက်နယ်ကြီးကြပ်မှူးလည်ပတ်မှု</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="316"/>
        <location filename="../ccongregation.cpp" line="321"/>
        <source>%1 (No meeting)</source>
        <comment>no meeting exception type</comment>
        <translation>%1 (အစည်းအဝေး မရှိ)</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="316"/>
        <location filename="../ccongregation.cpp" line="342"/>
        <source>Convention week</source>
        <translation>ခရိုင်စည်းဝေး အပတ်</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="318"/>
        <location filename="../ccongregation.cpp" line="344"/>
        <source>Memorial</source>
        <translation>အောက်မေ့ရာပွဲ</translation>
    </message>
    <message>
        <location filename="../ccongregation.cpp" line="349"/>
        <source>Review</source>
        <comment>Theocratic ministry school review</comment>
        <translation>ပြန်လည်သုံးသပ်ခြင်း</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="56"/>
        <location filename="../cpublictalks.cpp" line="341"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="57"/>
        <location filename="../cpublictalks.cpp" line="260"/>
        <location filename="../cpublictalks.cpp" line="342"/>
        <source>Last</source>
        <translation>နောက်ဆုံး</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="257"/>
        <location filename="../school.cpp" line="424"/>
        <source>All</source>
        <translation>အားလုံး</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="258"/>
        <location filename="../school.cpp" line="425"/>
        <source>Highlights</source>
        <translation>ပေါ်လွင်ချက်</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="259"/>
        <location filename="../school.cpp" line="426"/>
        <source>#1</source>
        <translation type="unfinished">၁</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="260"/>
        <location filename="../school.cpp" line="427"/>
        <source>#2</source>
        <translation type="unfinished">၂</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="261"/>
        <location filename="../school.cpp" line="428"/>
        <source>#3</source>
        <translation type="unfinished">၃</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="262"/>
        <location filename="../school.cpp" line="429"/>
        <source>WT Reader</source>
        <translation>ကင်:မျှော်စင် စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="264"/>
        <location filename="../school.cpp" line="431"/>
        <source>Selected</source>
        <translation>ရွေးထားတဲ့</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="323"/>
        <location filename="../school.cpp" line="486"/>
        <source>Assignment already has been made</source>
        <translation>တာဝန် ပေးပြီးသွားပြီ</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="327"/>
        <location filename="../school.cpp" line="490"/>
        <source>Assigned student has other meeting parts</source>
        <translation>တာဝန်ပေးထားတဲ့ ကျောင်းသားမှာ တခြားတာဝန်ရှိနေတယ်</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="331"/>
        <location filename="../school.cpp" line="494"/>
        <source>Student unavailable</source>
        <translation>ကျောင်းသား မအားပါ</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="335"/>
        <source>Student part of family</source>
        <translation>မိသားစုဝင် ကျောင်းသား</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="434"/>
        <source>Recently together</source>
        <translation>မကြာသေးခင်က အတူတူ</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="498"/>
        <source>Family member used in another TMS assignment</source>
        <translation>မိသားစုဝင်ကို တခြားကျောင်းတာဝန်မှာ သုံးထားတယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="67"/>
        <source>I/O</source>
        <comment>Incoming/Outgoing</comment>
        <translation>I/O</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="69"/>
        <source>Speaker</source>
        <translation type="unfinished">ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="72"/>
        <source>Notes</source>
        <translation>မှတ်စုတိုများ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="564"/>
        <source>Congregation Name</source>
        <translation>အသင်းတော် နာမည်:</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="271"/>
        <location filename="../school.cpp" line="846"/>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation type="unfinished">H</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="273"/>
        <location filename="../historytable.cpp" line="322"/>
        <location filename="../school.cpp" line="854"/>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="322"/>
        <source>CBS</source>
        <translation>အသင်းတော်ကျမ်းစာလေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="322"/>
        <source>C</source>
        <comment>abbreviation of the &apos;conductor&apos;</comment>
        <translation>သင်တန်းမှူး</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="325"/>
        <source>Conductor</source>
        <translation>သင်တန်းမှူး</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="325"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="468"/>
        <source>#</source>
        <translation>#</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="469"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="208"/>
        <source>Assignment</source>
        <translation>တာဝန်</translation>
    </message>
    <message>
        <source>date</source>
        <translation type="vanished">ရက်စွဲ</translation>
    </message>
    <message>
        <source>no</source>
        <translation type="vanished">မဟုတ်</translation>
    </message>
    <message>
        <source>material</source>
        <translation type="vanished">အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="470"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="209"/>
        <source>Note</source>
        <translation>မှတ်ချက်</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="471"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="210"/>
        <source>Time</source>
        <translation>အချိန်</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="256"/>
        <location filename="../school.cpp" line="263"/>
        <location filename="../school.cpp" line="423"/>
        <location filename="../school.cpp" line="430"/>
        <source>Assistant</source>
        <translation>အကူ</translation>
    </message>
    <message>
        <location filename="../school.cpp" line="256"/>
        <source>Student</source>
        <translation>ကျောင်းသား</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="265"/>
        <source>Bible highlights:</source>
        <translation>ကျမ်းစာ ပေါ်လွင်ချက်</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="268"/>
        <source>No. 1:</source>
        <translation>နံပါတ် ၁</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="271"/>
        <source>No. 2:</source>
        <translation>နံပါတ် ၂</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="274"/>
        <source>No. 3:</source>
        <translation>နံပါတ် ၃</translation>
    </message>
    <message>
        <location filename="../school_item.cpp" line="277"/>
        <source>Reader:</source>
        <translation>စာဖတ်သူ:</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="816"/>
        <location filename="../generatexml.cpp" line="107"/>
        <source>Default language not selected!</source>
        <translation>သုံးမယ့် ဘာသာစကား မရွေးရသေး!</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="937"/>
        <source>The header row of CSV file is not valid.</source>
        <translation>CSV ဖိုင်ရဲ့ ခေါင်းစည်းအတန်းက မရဘူး</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="296"/>
        <source>Confirm password!</source>
        <translation>လျှို့ဝှက်စာသား ထပ်ရိုက်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2249"/>
        <source>A user with the same E-mail address has already been added.</source>
        <translation>ဒီအီးမေးလ်လိပ်စာနဲ့ အသုံးပြုသူ ကို ပေါင်းထည့်ပြီးသား</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2455"/>
        <source>All talks have been added to this week</source>
        <translation>ဒီအပတ်မှာ ဟောပြောချက်အားလုံး ထည့်ပြီးပြီ</translation>
    </message>
    <message>
        <location filename="../cpublictalks.cpp" line="259"/>
        <location filename="../mainwindow.cpp" line="1003"/>
        <location filename="../publictalkedit.cpp" line="71"/>
        <source>Theme</source>
        <translation>အဓိကအကြောင်းအရာ</translation>
    </message>
    <message>
        <source>Import Error</source>
        <translation type="vanished">တင်သွင်းလို့ မရပါ</translation>
    </message>
    <message>
        <source>Unable to open database</source>
        <translation type="vanished">အချက်အလက်ရင်းမြစ်ကို ဖွင့်လို့ မရပါ</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="58"/>
        <location filename="../importwintm.cpp" line="42"/>
        <source>Import</source>
        <translation>တင်သွင်း</translation>
    </message>
    <message>
        <location filename="../importTa1ks.cpp" line="58"/>
        <location filename="../importwintm.cpp" line="42"/>
        <source>Import Complete</source>
        <translation>တင်သွင်းပြီးသွားပြီ</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="173"/>
        <source>Date</source>
        <comment>The todo list date cell is in error</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="192"/>
        <source>Speaker</source>
        <comment>The todo list Speaker cell is in error</comment>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="199"/>
        <source>Congregation</source>
        <comment>The todo list Congregation cell is in error</comment>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="209"/>
        <source>Theme (could be a theme this speaker does not give)</source>
        <comment>The todo list Theme cell is in error</comment>
        <translation>အကြောင်းအရာ (ဒီဟောပြောသူ ဟောတာမဟုတ်တဲ့ အကြောင်းအရာ ဖြစ်နိုင်တယ်)</translation>
    </message>
    <message>
        <location filename="../todo.cpp" line="221"/>
        <location filename="../todo.cpp" line="232"/>
        <source>Date Already Scheduled</source>
        <translation>ရက်စွဲ အစီအစဥ် ဆွဲပြီးသား</translation>
    </message>
    <message>
        <source>File reading failed</source>
        <translation type="vanished">ဖိုင် ဖတ်လို့မရပါ</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="85"/>
        <source>Unable to read new Workbook format</source>
        <translation>လေ့ကျင်းခန်းစာစောင် အသစ် ပုံစံကို ဖတ်လို့မရပါ</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="87"/>
        <source>Database not set to handle language &apos;%1&apos;</source>
        <translation>အချက်အလက်ရင်းမြစ်က &apos;%1&apos; ဘာသာစကားကို ကိုင်တွယ်လို့ မရပါ</translation>
    </message>
    <message>
        <source>Nothing imported (no month names recognized)</source>
        <translation type="vanished">ဘာမှ မတင်သွင်းလိုက်ရဘူး (ဘယ်လလည်း မသိရဘူး)</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="89"/>
        <source>Unable to find year/month</source>
        <translation>ခုနှစ်/လ ရှာလို့မတွေ့</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="90"/>
        <source>Nothing imported (no dates recognized)</source>
        <translation>တင်သွင်းထားတာ ဘာမှမရှိပါ (ဘယ်ရက်စွဲမှ မသိပါ)</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="94"/>
        <location filename="../wtimport.cpp" line="31"/>
        <location filename="../wtimport.cpp" line="32"/>
        <source>Imported %1 weeks from %2 thru %3</source>
        <translation>%2 ကနေ %3 အထိ %1 အပတ်တွေ တင်သွင်းပြီးပြီ</translation>
    </message>
    <message>
        <location filename="../importlmmworkbook.cpp" line="397"/>
        <source>Please select the Talk Names to match the names we found in the workbook</source>
        <translation>လေ့ကျင့်ခန်းစာစောင်ထဲက နာမည်တွေနဲ့ ကိုက်အောင် ဟောပြောချက်နာမည်တွေကို ရွေးပေးပါ</translation>
    </message>
    <message>
        <source>Imported weeks from %1 thru %2</source>
        <translation type="vanished">အပတ် %1 ကနေ %2 အထိ တင်သွင်းမယ်</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="7"/>
        <source>Ch</source>
        <comment>history table: abbreviation for the &apos;chairman&apos; of the Christian Life and Ministry Meeting</comment>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="9"/>
        <source>H</source>
        <comment>history table: abbreviation for the &apos;highlights&apos;</comment>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="11"/>
        <source>D</source>
        <comment>history table: abbreviation for &apos;Diging spiritual gem&apos;</comment>
        <translation>D</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="13"/>
        <source>#R</source>
        <comment>history table: abbreviation for the &apos;reader&apos;</comment>
        <translation>#R</translation>
    </message>
    <message>
        <source>#M</source>
        <comment>history table: abbreviation for &apos;Prepare your monthly presentation&apos;</comment>
        <translation type="vanished">#M</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="15"/>
        <source>#V</source>
        <comment>history table: abbreviation for &apos;prepare video presentation&apos;</comment>
        <translation>#V</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="17"/>
        <source>#A</source>
        <comment>history table: abbreviation for &apos;Apply yourself to Reading and Teaching&apos;</comment>
        <translation>#A</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="20"/>
        <source>♢1</source>
        <comment>history table: abbreviation for assignment 1, assistant/householder of &apos;Initial Visit&apos;</comment>
        <translation>♢၁</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="21"/>
        <source>#1</source>
        <comment>history table: abbreviation for assignment 1, &apos;Initial Visit&apos;</comment>
        <translation>၁</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="26"/>
        <source>♢2</source>
        <comment>history table: abbreviation for assignment 2, assistant/householder of &apos;Return Visit&apos;</comment>
        <translation>♢၂</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="27"/>
        <source>#2</source>
        <comment>history table: abbreviation for assignment 2, &apos;Return Visit&apos;</comment>
        <translation>၂</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="31"/>
        <source>♢3</source>
        <comment>history table: abbreviation for assignment 3, assistant/householder of &apos;Bible Study&apos;</comment>
        <translation>♢၃</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="32"/>
        <source>#3</source>
        <comment>history table: abbreviation for assignment 3, &apos;Bible Study&apos;</comment>
        <translation>၃</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="34"/>
        <source>CL1</source>
        <comment>history table: abbreviation for cristian life, talk 1</comment>
        <translation>CL1</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="36"/>
        <source>CL2</source>
        <comment>history table: abbreviation for cristian life, talk 2</comment>
        <translation>CL2</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="38"/>
        <source>CL3</source>
        <comment>history table: abbreviation for cristian life, talk 3</comment>
        <translation>CL3</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="41"/>
        <source>BS-R</source>
        <comment>history table: abbreviation for cristian life, Bible Study Reader</comment>
        <translation>ကျမ်းဖတ်</translation>
    </message>
    <message>
        <location filename="../lmm_meeting.cpp" line="42"/>
        <source>BS</source>
        <comment>history table: abbreviation for cristian life, Bible Study</comment>
        <translation>BS</translation>
    </message>
    <message>
        <location filename="../slipscanner.cpp" line="28"/>
        <source>One-time Scanning of New Slip</source>
        <translation>မှတ်တမ်းအသစ်ကို တစ်ကြိမ်တည်းအပြီး scan ဖတ်ခြင်း</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="820"/>
        <source>All weekend meetings for</source>
        <comment>Filename prefix for weekend meetings iCal export</comment>
        <translation>တနင်္ဂနွေအစည်းအဝေးအားလုံး</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="832"/>
        <source>Weekend Meeting</source>
        <translation>တနင်္ဂနွေအစည်းအဝေး</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="940"/>
        <source>All outgoing talks for</source>
        <comment>File name prefix for outgoing talks iCal export</comment>
        <translation>သွားလည် ဟောပြောချက်အားလုံး</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="956"/>
        <location filename="../mainwindow.cpp" line="967"/>
        <source>Outgoing Talks</source>
        <translation>သွားလည် ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1072"/>
        <source>Midweek Meeting</source>
        <translation>ကြားပတ်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="467"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="206"/>
        <location filename="../publictalkedit.cpp" line="68"/>
        <location filename="../schoolreminder.cpp" line="182"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <source>Material</source>
        <comment>Source material</comment>
        <translation type="vanished">ရင်းမြစ် အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="473"/>
        <location filename="../lmm_assignmentcontoller.cpp" line="212"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>အတူ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="166"/>
        <source>Sister</source>
        <translation>ညီမ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="167"/>
        <source>Brother</source>
        <translation>ညီကို</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="171"/>
        <source>Dear %1 %2</source>
        <translation>ချစ်ခင်ရသော %1 %2</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="172"/>
        <source>Please find below details of your upcoming assignment:</source>
        <translation>လာမယ့် သင့်တာဝန် အသေးစိတ်တွေကို အောက်မှာ ရှာလိုက်ပါ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="231"/>
        <source>Regards</source>
        <translation>လေးစားစွာဖြင့်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="970"/>
        <source>You have a new kind of slip. Please enter a request on the forum so the app can be updated.  Mention code %1</source>
        <translation>တာဝန်စာရွက်အသစ် သင့်ဆီမှာ ရှိတယ်။ app ဗားရှင်းအသစ်ထွက်ဖို့ ဖိုရမ်မှာ တောင်းဆိုမှု လုပ်ပါ။  Mention code %1</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="203"/>
        <source>Publisher</source>
        <comment>Roles and access control</comment>
        <translation>ကြေညာသူ</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="96"/>
        <source>View midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး ဇယားကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="98"/>
        <source>Edit midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး ဇယားပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="100"/>
        <source>View midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး စက်တင်ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="102"/>
        <source>Edit midweek meeting settings</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး စက်တင်ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="104"/>
        <source>Send midweek meeting reminders</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး အသိပေးချက်ပို့</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="106"/>
        <source>Print midweek meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး ဇယား ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="108"/>
        <source>Print midweek meeting assignment slips</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး တာဝန် ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="110"/>
        <source>Print midweek meeting worksheets</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး လေ့ကျင့်ခန်း ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="112"/>
        <source>View weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ကြားရက်အစည်းအဝေး ဇယားကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="114"/>
        <source>Edit weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ပတ်ဆုံးအစည်းအဝေး ဇယားပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="116"/>
        <source>View weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation>ပတ်ဆုံးအစည်းအဝေး စက်တင်ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="118"/>
        <source>Edit weekend meeting settings</source>
        <comment>Access Control</comment>
        <translation>ပတ်ဆုံးအစည်းအဝေး စက်တင်ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="120"/>
        <source>View public talk list</source>
        <comment>Access Control</comment>
        <translation>ဟောပြောချက် စာရင်းကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="122"/>
        <source>Edit public talk list</source>
        <comment>Access Control</comment>
        <translation>ဟောပြောချက် စာရင်းပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="124"/>
        <source>Schedule hospitality</source>
        <comment>Access Control</comment>
        <translation>ဧည့်ဝတ် စီစဥ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="126"/>
        <source>Print weekend meeting schedule</source>
        <comment>Access Control</comment>
        <translation>ပတ်ဆုံးအစည်းအဝေး ဇယား ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="128"/>
        <source>Print weekend meeting worksheets</source>
        <comment>Access Control</comment>
        <translation>ပတ်ဆုံးအစည်းအဝေး တာဝန်ဇယား စာရွက်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="130"/>
        <source>Print speakers schedule</source>
        <comment>Access Control</comment>
        <translation>ဟောပြောသူ အစီအစဥ်ဇယား ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="132"/>
        <source>Print speakers assignments</source>
        <comment>Access Control</comment>
        <translation>ဟောပြောသူ တာဝန် စာရွက်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="134"/>
        <source>Print hospitality</source>
        <comment>Access Control</comment>
        <translation>ဧည့်ဝတ် အစီအစဥ် ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="136"/>
        <source>Print public talk list</source>
        <comment>Access Control</comment>
        <translation>ဟောပြောချက် စာရင်း ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="146"/>
        <source>View public speakers</source>
        <comment>Access Control</comment>
        <translation>ဟောပြောသူ ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="148"/>
        <source>Edit public speakers</source>
        <comment>Access Control</comment>
        <translation>ဟောပြောသူ ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="138"/>
        <source>View publishers</source>
        <comment>Access Control</comment>
        <translation>ကြေညာသူ ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="140"/>
        <source>Edit publishers</source>
        <comment>Access Control</comment>
        <translation>ကြေညာသူ ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="142"/>
        <source>View student data</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="144"/>
        <source>Edit student data</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="150"/>
        <source>View privileges</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="152"/>
        <source>Edit privileges</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="154"/>
        <source>View midweek meeting talk history</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="156"/>
        <source>View availabilities</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="158"/>
        <source>Edit availabilities</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="160"/>
        <source>View permissions</source>
        <comment>Access Control</comment>
        <translation>ခွင့်ပြုချက် ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="162"/>
        <source>Edit permissions</source>
        <comment>Access Control</comment>
        <translation>ခွင့်ပြုချက် ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="164"/>
        <source>View territories</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက် ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="166"/>
        <source>Edit territories</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက် ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="168"/>
        <source>Print territory record</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက်ကဒ် ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="170"/>
        <source>Print territory map card</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက်မြေပုံကဒ် ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="172"/>
        <source>Print territory map and address sheets</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက်မြေပုံနဲ့ လိပ်စာစာရွက် ပရင့်ထုတ်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="174"/>
        <source>View territory settings</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက်စက်တင် ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="176"/>
        <source>Edit territory settings</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက်စက်တင် ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="178"/>
        <source>View territory assignments</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက်တာဝန် ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="180"/>
        <source>View territory addresses</source>
        <comment>Access Control</comment>
        <translation>ရပ်ကွက်လိပ်စာ ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="182"/>
        <source>View congregation settings</source>
        <comment>Access Control</comment>
        <translation>အသင်းတော် စက်တင် ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="184"/>
        <source>Edit congregation settings</source>
        <comment>Access Control</comment>
        <translation>အသင်းတော် စက်တင် ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="186"/>
        <source>View special events</source>
        <comment>Access Control</comment>
        <translation>အထူး အစီအစဥ် ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="188"/>
        <source>Edit special events</source>
        <comment>Access Control</comment>
        <translation>အထူး အစီအစဥ် ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="190"/>
        <source>View song list</source>
        <comment>Access Control</comment>
        <translation>သီချင်း စာရင်း ကြည့်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="192"/>
        <source>Edit song list</source>
        <comment>Access Control</comment>
        <translation>သီချင်း စာရင်း ပြင်</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="194"/>
        <source>Delete cloud data</source>
        <comment>Access Control</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="205"/>
        <source>Elder</source>
        <comment>Roles and access control</comment>
        <translation>အကြီးအကဲ</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="207"/>
        <source>LMM Chairman</source>
        <comment>Roles and access control</comment>
        <translation>ခသင သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="209"/>
        <source>LMM Overseer</source>
        <comment>Roles and access control</comment>
        <translation>ခသင ကြီးကြပ်မှူး</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="211"/>
        <source>Public Talk Coordinator</source>
        <comment>Roles and access control</comment>
        <translation>လူထုဟောပြောချက် စီစဥ်သူ</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="213"/>
        <source>Territory Servant</source>
        <comment>Roles and access control</comment>
        <translation>ရပ်ကွက် အမှုထမ်း</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="215"/>
        <source>Secretary</source>
        <comment>Roles and access control</comment>
        <translation>အတွင်းရေးမှူး</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="217"/>
        <source>Service Overseer</source>
        <comment>Roles and access control</comment>
        <translation>လုပ်ငန်းတော်ကြီးကြပ်မှူး</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="219"/>
        <source>Coordinator of BOE</source>
        <comment>Roles and access control</comment>
        <translation>အကြီးအကဲအဖွဲ့ ညှိနှိုင်းရေးမှူး</translation>
    </message>
    <message>
        <location filename="../accesscontrol.h" line="221"/>
        <source>Administrator</source>
        <comment>Roles and access control</comment>
        <translation>စီမံခန့်ခွဲသူ</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../settings.ui" line="174"/>
        <location filename="../settings.ui" line="177"/>
        <location filename="../settings.ui" line="1610"/>
        <source>Exceptions</source>
        <translation>ခြွင်းချက်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="204"/>
        <location filename="../settings.ui" line="207"/>
        <source>Public Talks</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="773"/>
        <source>Custom templates folder</source>
        <translation>စိတ်ကြိုက်ပုံစံခွက်ဖိုင်တွဲ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="917"/>
        <source>Open database location</source>
        <translation>ဒေတာရင်းမြစ် တည်နေရာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="976"/>
        <source>Backup database</source>
        <translation>Backup database</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1010"/>
        <source>Restore database</source>
        <translation>Restore database</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1454"/>
        <source>Names display order</source>
        <translation>နာမည် ဖော်ပြမှု အစဥ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1521"/>
        <source>Color palette</source>
        <translation>အရောင်စပ်ဝိုင်း</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1466"/>
        <source>By last name</source>
        <translation>နောက်နာမည်ဖြင့်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1478"/>
        <source>By first name</source>
        <translation>ရှေ့နာမည်ဖြင့်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1533"/>
        <source>Light</source>
        <translation>လင်းလင်း</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1545"/>
        <source>Dark</source>
        <translation>မှောင်မှောင်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2188"/>
        <location filename="../settings.ui" line="2338"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;Errors&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2399"/>
        <source>Studies</source>
        <comment>Name of tab to edit Midweek Meeting Study Points</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2497"/>
        <source>Public talks maintenance</source>
        <translation>ဟောပြောချက် ထိန်းသိမ်းမှု</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2589"/>
        <source>Schedule hospitality for public speakers</source>
        <translation>လူထုဟောပြောသူအတွက် ဧည့်ဝတ်စီစဥ်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3105"/>
        <source>Streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3111"/>
        <source>Street types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3361"/>
        <source>Map marker scale:</source>
        <translation>မြေပုံ အမှတ်အသား စကေး။  ။</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3387"/>
        <source>Geo Services</source>
        <translation>Geo ဝန်ဆောင်မှု</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3395"/>
        <source>Google:</source>
        <translation>Google:</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3405"/>
        <location filename="../settings.ui" line="3408"/>
        <source>API Key</source>
        <translation>API Key</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3415"/>
        <source>Here:</source>
        <translation>ဒီမှာ။ ။</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3422"/>
        <source>Default:</source>
        <translation>အရှိအတိုင်း။  ။</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3432"/>
        <source>OpenStreetMap</source>
        <translation>OpenStreetMap</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3437"/>
        <source>Google</source>
        <translation>Google</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3442"/>
        <source>Here</source>
        <translation>ဒီမှာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3457"/>
        <location filename="../settings.ui" line="3460"/>
        <source>App Id</source>
        <translation>App Id</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3467"/>
        <location filename="../settings.ui" line="3470"/>
        <source>App Code</source>
        <translation>App Code</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3506"/>
        <source>Send E-Mail Reminders</source>
        <translation>အီးမေးလ် သတိပေးချက် ပို့မယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3756"/>
        <source>Send reminders when closing TheocBase</source>
        <translation>TheocBase ပိတ်နေတုန်း သတိပေးချက် ပို့ပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3528"/>
        <source>E-Mail Options</source>
        <translation>အီးမေးလ် ရွေးချယ်စရာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3562"/>
        <source>Sender&apos;s e-mail</source>
        <translation>ပို့သူ အီးမေးလ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3572"/>
        <source>Sender&apos;s name</source>
        <translation>ပို့သူ နာမည်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3666"/>
        <source>Test Connection</source>
        <translation>ကွန်ရက် ဆက်သွယ်မှုကို စမ်းသပ်မယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="706"/>
        <source>Printing</source>
        <translation>ပုံနှိပ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3796"/>
        <source>Users</source>
        <translation>အသုံးပြုသူများ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3812"/>
        <source>E-mail:</source>
        <translation>အီးမေးလ်။  ။</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3865"/>
        <source>Rules</source>
        <translation>စည်းမျဥ်း</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="159"/>
        <location filename="../settings.ui" line="162"/>
        <source>General</source>
        <translation>ယေဘုယျ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="839"/>
        <source>Backup</source>
        <translation>Backup</translation>
    </message>
    <message>
        <source>Database backup</source>
        <translation type="vanished">Database backup</translation>
    </message>
    <message>
        <source>Restore backup database</source>
        <translation type="vanished">Restore backup database</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="468"/>
        <source>Current congregation</source>
        <translation>လက်ရှိ အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1302"/>
        <source>User interface</source>
        <translation>User interface</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1386"/>
        <source>User interface language</source>
        <translation>အသုံးပြု ဘာသာစကား</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1063"/>
        <source>Security</source>
        <translation>လုံခြုံရေး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1242"/>
        <source>Enable password</source>
        <translation>Enable password</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1267"/>
        <source>Enable database encryption</source>
        <translation>Enable database encryption</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1168"/>
        <source>Confirm</source>
        <translation>အတည်ပြုမယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="189"/>
        <location filename="../settings.ui" line="192"/>
        <location filename="../settings.ui" line="1820"/>
        <source>Life and Ministry Meeting</source>
        <translation>ခရစ်ယန်အသက်တာနဲ့ အမှုဆောင်လုပ်ငန်းစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2013"/>
        <source>Remove Duplicates</source>
        <translation>ထပ်နေတာတွေ ဖယ်လိုက်မယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2020"/>
        <source>Meeting Items</source>
        <translation>အစည်းအဝေး အပိုင်း</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2119"/>
        <source>Schedule</source>
        <comment>Name of tab to edit Midweek Meeting schedule</comment>
        <translation>အချိန်ဇယား</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2599"/>
        <source>Hide discontinued</source>
        <translation>မဖျောက်ထားတော့ဘူး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2781"/>
        <source>Add songs</source>
        <translation>သီချင်း ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="219"/>
        <location filename="../settings.ui" line="2787"/>
        <source>Songs</source>
        <translation>သီချင်း</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2859"/>
        <location filename="../settings.ui" line="2865"/>
        <source>Add song one at a time</source>
        <translation>သီချင်း တစ်ခုချင်း ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2880"/>
        <source>Song number</source>
        <translation>သီချင်း နံပါတ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2887"/>
        <source>Song title</source>
        <translation>သီချင်း ခေါင်းစဥ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2961"/>
        <source>Cities</source>
        <translation>မြို့</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3033"/>
        <source>Territory types</source>
        <translation>ရပ်ကွက်အမျိုးအစား</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3210"/>
        <source>Addresses</source>
        <translation>လိပ်စာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3216"/>
        <source>Address types</source>
        <translation>လိပ်စာအမျိုးအစားတွေ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3324"/>
        <source>Configuration</source>
        <translation>ချိန်ညှိစရာ</translation>
    </message>
    <message>
        <source>Scale:</source>
        <translation type="vanished">စကေး။</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="246"/>
        <location filename="../settings.cpp" line="2263"/>
        <source>Access Control</source>
        <translation>ဝင်ကြည့်နိုင်မှု ထိန်းချုပ်ရေး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3222"/>
        <source>Type number:</source>
        <translation>အမျိုးအစားနံပါတ်။  ။</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3117"/>
        <location filename="../settings.ui" line="3232"/>
        <location filename="../settings.ui" line="3802"/>
        <source>Name:</source>
        <translation>အမည်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3186"/>
        <location filename="../settings.ui" line="3242"/>
        <source>Color:</source>
        <translation>အရောင်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3127"/>
        <location filename="../settings.ui" line="3252"/>
        <source>#0000ff</source>
        <translation>#0000ff</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3332"/>
        <source>Default address type:</source>
        <translation>အရှိအတိုင်း လိပ်စာအမျိုးအစား။ ။</translation>
    </message>
    <message>
        <source>&lt;a href=&quot;#&quot;&gt;Forgot Password&lt;/a&gt;</source>
        <translation type="vanished">&lt;a href=&quot;#&quot;&gt;စကားဝှက် မေ့သွားတယ်&lt;/a&gt;</translation>
    </message>
    <message>
        <source>Delete Cloud Data</source>
        <translation type="vanished">အွန်လိုင်းဒေတာ ဖျက်မယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="228"/>
        <source>Territories</source>
        <translation>ရပ်ကွက်</translation>
    </message>
    <message>
        <source>TheocBase Cloud</source>
        <translation type="vanished">TheocBase Cloud</translation>
    </message>
    <message>
        <source>Open Database Location</source>
        <translation type="vanished">ဒေတာရင်းမြစ် တည်နေရာ ဖွင့်မယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="386"/>
        <location filename="../settings.cpp" line="1101"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="624"/>
        <source>Weekend Meeting</source>
        <translation>တနင်္ဂနွေစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="435"/>
        <source>Current Circuit Overseer</source>
        <translation>လက်ရှိ တိုက်နယ်ကြီးကြပ်မှုး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="668"/>
        <source>Click to edit</source>
        <translation>ပြန်ပြင်ဖို့ နှိပ်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1111"/>
        <source>Username</source>
        <translation>အသုံးပြုသူနာမည်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1132"/>
        <location filename="../settings.ui" line="3673"/>
        <source>Password</source>
        <translation>လျှို့ဝှက်စာသား</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="529"/>
        <location filename="../settings.ui" line="1838"/>
        <location filename="../settings.ui" line="1902"/>
        <source>Mo</source>
        <translation>တနင်္လာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="534"/>
        <location filename="../settings.ui" line="1843"/>
        <location filename="../settings.ui" line="1907"/>
        <source>Tu</source>
        <translation>အင်္ဂါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="539"/>
        <location filename="../settings.ui" line="1848"/>
        <location filename="../settings.ui" line="1912"/>
        <source>We</source>
        <translation>ဗုဒ္ဓဟူး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="544"/>
        <location filename="../settings.ui" line="1853"/>
        <location filename="../settings.ui" line="1917"/>
        <source>Th</source>
        <translation>ကြာသပတေး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="549"/>
        <location filename="../settings.ui" line="1858"/>
        <location filename="../settings.ui" line="1922"/>
        <source>Fr</source>
        <translation>သောကြာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="554"/>
        <location filename="../settings.ui" line="1863"/>
        <location filename="../settings.ui" line="1927"/>
        <source>Sa</source>
        <translation>စနေ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="559"/>
        <location filename="../settings.ui" line="1868"/>
        <location filename="../settings.ui" line="1932"/>
        <source>Su</source>
        <translation>တနင်္ဂနွေ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1881"/>
        <source>Public Talk and Watchtower study</source>
        <translation>လူတုဟောပြောချက် နဲ့ ကင်းမျှော်စင်သင်တန်း</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1754"/>
        <location filename="../settings.ui" line="1783"/>
        <location filename="../settings.ui" line="2210"/>
        <location filename="../settings.ui" line="2271"/>
        <location filename="../settings.ui" line="2321"/>
        <location filename="../settings.ui" line="2360"/>
        <location filename="../settings.ui" line="2420"/>
        <location filename="../settings.ui" line="2443"/>
        <location filename="../settings.ui" line="2530"/>
        <location filename="../settings.ui" line="2730"/>
        <location filename="../settings.ui" line="2808"/>
        <location filename="../settings.ui" line="2831"/>
        <location filename="../settings.ui" line="2906"/>
        <location filename="../settings.ui" line="2982"/>
        <location filename="../settings.ui" line="3008"/>
        <location filename="../settings.ui" line="3054"/>
        <location filename="../settings.ui" line="3080"/>
        <location filename="../settings.ui" line="3149"/>
        <location filename="../settings.ui" line="3175"/>
        <location filename="../settings.ui" line="3271"/>
        <location filename="../settings.ui" line="3297"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <source>Custom Templates Folder</source>
        <translation type="vanished">စိတ်ကြိုက်ပုံစံခွက်ဖိုင်တွဲ</translation>
    </message>
    <message>
        <source>iCal Export</source>
        <translation type="vanished">iCal တင်ပို့</translation>
    </message>
    <message>
        <source>Events grouped by date</source>
        <translation type="vanished">ရက်စွဲအလိုက် အစီအစဥ်များ</translation>
    </message>
    <message>
        <source>All day events</source>
        <translation type="vanished">တစ်နေကုန် အစီအစဥ်များ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1652"/>
        <source>Circuit Overseer&apos;s visit</source>
        <translation>တိုက်နယ်ကြီးကြပ်မှူးလည်ပတ်မှု</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1657"/>
        <source>Convention</source>
        <translation>စည်းဝေးကြီး</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1662"/>
        <source>Memorial</source>
        <translation>အောက်မေ့ရာပွဲ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1667"/>
        <source>Zone overseer&apos;s talk</source>
        <translation>ဇုံကြီးကြပ်မှုး ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1672"/>
        <source>Other exception</source>
        <translation>တခြား ခြွင်းချက်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1685"/>
        <location filename="../settings.cpp" line="608"/>
        <source>Start date</source>
        <translation>ရက်စွဲကို စတင်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1716"/>
        <location filename="../settings.cpp" line="609"/>
        <source>End date</source>
        <translation>ရက်စွဲကို အဆုံးသတ်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1833"/>
        <location filename="../settings.ui" line="1897"/>
        <source>No meeting</source>
        <translation>အစည်းအဝေး မရှိပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1799"/>
        <source>Description</source>
        <translation>ဖော်ပြချက်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2053"/>
        <source>Import Christian Life and Ministry Meeting Workbook</source>
        <translation>ခရစ်ယာန်အသက်တာနဲ့ အမှုဆောင်လုပ်ငန်း လေ့ကျင့်ခန်းစာစောင် တင်သွင်းမယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1981"/>
        <source>Main</source>
        <comment>Main Midweek meeting tab</comment>
        <translation>ပင်မ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2132"/>
        <source>Year</source>
        <translation>နှစ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="486"/>
        <location filename="../settings.ui" line="2170"/>
        <source>Midweek Meeting</source>
        <translation>ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;Errors&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head /&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; color:#540000;&quot;&gt;ပြဿနာ&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2297"/>
        <source>Midweek Meeting Schedule for selected Meeting above</source>
        <translation>အပေါ်မှာ ရွေးထားတဲ့ စည်းဝေးအတွက် ကြားရက်စည်းဝေး အစီအစဥ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3584"/>
        <source>Custom settings</source>
        <translation>စိတ်ကြိုက် ဇာတ်အိမ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3705"/>
        <source>Authorize</source>
        <translation>လုပ်ပိုင်ခွင့်ပေးမယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3715"/>
        <source>Status</source>
        <translation>အခြေအနေ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2788"/>
        <source>Synchronize</source>
        <translation>တပြေးညီ ချိန်ညှိမယ်</translation>
    </message>
    <message>
        <source>Username or Email</source>
        <translation type="vanished">အသုံးပြုသူနာမည်/အီးမေးလ်</translation>
    </message>
    <message>
        <source>&lt;a href=&quot;http://register.theocbase.net/index.php&quot;&gt;Create an account&lt;/a&gt;</source>
        <translation type="vanished">&lt;a href=&quot;http://register.theocbase.net/index.php&quot;&gt;အကောင့် အသစ် လုပ်မယ်&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="237"/>
        <source>Reminders</source>
        <translation>သတိပေးချက်များ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="3932"/>
        <source>Run a file provided by the Help Desk</source>
        <translation>Help Desk ကနေပေးတဲ့ ဖိုင်ကို run ပါ</translation>
    </message>
    <message>
        <source>Run Special Command</source>
        <translation type="vanished">အထူး ညွှန်ကြားချက် ကို လုပ်ဆောင်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="26"/>
        <source>Settings</source>
        <comment>This means the &apos;Options&apos; of TheocBase</comment>
        <translation>ချိန်ညှိစရာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="1987"/>
        <source>Number of classes</source>
        <translation>သင်တန်း အရေအတွက်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2741"/>
        <source>Studies</source>
        <translation>သင်အံမူများ</translation>
    </message>
    <message>
        <source>Add subjects</source>
        <translation type="vanished">အကြောင်းအရာ ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2613"/>
        <location filename="../settings.ui" line="2619"/>
        <source>Add subject one at a time</source>
        <translation>တစ်ချိန်မှာ အကြောင်းအရာ တစ်ခု ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2653"/>
        <source>Public talk number</source>
        <translation>လူထုဟောပြောချက် နံပါတ်</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2660"/>
        <source>Public talk subject</source>
        <translation>လူတုဟောပြောချက် အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2670"/>
        <location filename="../settings.ui" line="2917"/>
        <source>Language</source>
        <translation>ဘာသာစကား</translation>
    </message>
    <message>
        <location filename="../settings.ui" line="2702"/>
        <source>Add congregations and speakers</source>
        <translation>အသင်းတော် နဲ့ ဟောပြောသူ ထည့်မယ်</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">ပိတ်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="828"/>
        <location filename="../settings.cpp" line="1103"/>
        <location filename="../settings.cpp" line="1171"/>
        <source>Number</source>
        <translation>နံပါတ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2558"/>
        <source>Source</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="556"/>
        <source>Select a backup file</source>
        <translation>Backup ဖိုင် ရွေးပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="830"/>
        <source>Revision</source>
        <comment>Public talk outline revision</comment>
        <translation type="unfinished">ပြန်သုံးသပ်ချက်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="934"/>
        <location filename="../settings.cpp" line="1256"/>
        <location filename="../settings.cpp" line="1553"/>
        <location filename="../settings.cpp" line="1621"/>
        <location filename="../settings.cpp" line="1705"/>
        <location filename="../settings.cpp" line="1801"/>
        <source>Remove selected row?</source>
        <translation>ရွေးချယ်ထားတဲ့ အတန်းကို ဖယ်မလား?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1032"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>ဟောပြောချက် နံပါတ်တူတာ သိမ်းပြီးသား ဖြစ်နေတယ်!
အရင်တစ်ခုကို ဖျက်လိုက်တော့မလား?

စီစဥ်ထားတဲ့ ဟောပြောချက်တွေ လုပ်ရမယ့်စာရင်းထဲ ရောက်သွားမယ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1172"/>
        <source>Title</source>
        <translation>ခေါင်းစဥ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1272"/>
        <source>Song number missing</source>
        <translation>သီချင်း နံပါတ် မပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1275"/>
        <source>Song title missing</source>
        <translation>သီချင်း ခေါင်းစဥ် မပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1285"/>
        <source>Song is already saved!</source>
        <translation>သီချင်းကို သိမ်းပြီးသား ဖြစ်နေတယ်!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1294"/>
        <source>Song added to database</source>
        <translation>အချက်အလက်ရင်းမြစ် ထဲ သီချင်းထည့်လိုက်ပြီ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1331"/>
        <source>City</source>
        <translation>မြို့</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1367"/>
        <location filename="../settings.cpp" line="1451"/>
        <source>Type</source>
        <translation>အမျိုးအစား</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1517"/>
        <source>City name missing</source>
        <translation>မြို့နာမည် လိုနေတယ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1526"/>
        <source>City is already saved!</source>
        <translation>မြို့ ကို သိမ်းပြီးသား!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1534"/>
        <source>City added to database</source>
        <translation>Database ထဲ မြို့ ထည့်လိုက်ပြီ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1585"/>
        <source>Territory type name missing</source>
        <translation>ရပ်ကွက်အမျိုးအစားနာမည် လိုနေတယ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1594"/>
        <source>Territory type is already saved!</source>
        <translation>ရပ်ကွက်အမျိုးအစား သိမ်းပြီးသား!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1602"/>
        <source>Territory type added to database</source>
        <translation>Database ထဲ ရပ်ကွက်အမျိုးအစား ထည့်လိုက်ပြီ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1671"/>
        <source>Name of the street type is missing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1678"/>
        <source>Street type is already saved!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1686"/>
        <source>Street type added to database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2026"/>
        <source>This is no longer an option. Please request help in the forum if this is needed.</source>
        <translation>ရွေးချယ်စရာ မရှိတော့ပါ။ ဒါကို လိုအပ်မယ်ဆိုရင် ဖိုရမ်မှာ အကူအညီတောင်းပါ။</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2263"/>
        <source>Remove permissions for the selected user?</source>
        <translation>ရွေးထားတဲ့ အသုံးပြုသူကို ခွင့်ပြုချက်တွေ ဖယ်လိုက်မလား?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2495"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="829"/>
        <location filename="../settings.cpp" line="2557"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="831"/>
        <source>Released on</source>
        <comment>Release date of the public talk outline</comment>
        <translation>ထုတ်တဲ့ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="832"/>
        <source>Discontinued on</source>
        <comment>Date after which the public talk outline should no longer be used</comment>
        <translation>ဆက်မထုတ်တော့တဲ့ ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="985"/>
        <source>Discontinuing this talk will move talks scheduled with this outline to the To Do List.

</source>
        <translation>ဒီဟောပြောချက်ကို ဆက်မလုပ်တော့ရင် ဒီအောက်လိုင်းနဲ့ ဟောပြောချက်တွေ လုပ်ရမယ့်စာရင်းထဲ ရောက်သွားမယ်

</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1104"/>
        <source>Revision</source>
        <translation>ပြန်သုံးသပ်ချက်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1408"/>
        <location filename="../settings.cpp" line="1452"/>
        <location filename="../settings.cpp" line="2140"/>
        <location filename="../settings.cpp" line="2196"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1409"/>
        <location filename="../settings.cpp" line="1453"/>
        <source>Color</source>
        <translation>အရောင်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1758"/>
        <source>Number of address type is missing</source>
        <translation>လိပ်စာအမျိုးအစား နံပါတ် မရှိဘူးဖြစ်နေတယ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1762"/>
        <source>Name of address type is missing</source>
        <translation>လိပ်စာအမျိုးအစား နာမည် မရှိဘူးဖြစ်နေတယ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1771"/>
        <source>Address type is already saved!</source>
        <translation>လိပ်စာအမျိုးအစား သိမ်းပြီးသွားပြီ!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1781"/>
        <source>Address type added to database</source>
        <translation>လိပ်စာအမျိုးအစားကို ဒေတာရင်းမြစ်မှာ ထည့်လိုက်ပြီ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1879"/>
        <source>Error sending e-mail</source>
        <translation>အီးမေးလ် ပို့လို့ မရပါ</translation>
    </message>
    <message>
        <source>Sign In</source>
        <translation type="vanished">ဝင်မယ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2790"/>
        <source>Sign Out</source>
        <translation>ထွက်မယ်</translation>
    </message>
    <message>
        <source>Login Failed</source>
        <translation type="vanished">ဝင်လို့မရပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1931"/>
        <source>Authorized. (Re-authorization only needed if you are getting error messages when sending mail.)</source>
        <translation>လုပ်ပိုင်ခွင့် ပေးပြီးပြီ။ (မေးလ်ပို့လို့မရမှ လုပ်ပိုင်ခွင့် ပြန်ပေးဖို့ လိုတယ်)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1933"/>
        <source>Click the button to authorize theocbase to send email on your behalf</source>
        <translation>ကိုယ့်အစား TheocBase ကမေးလ်ပို့ပေးလို့ရအောင် လုပ်ပိုင်ခွင့်ပေးဖို့ ဒီခလုတ်ကို နှိပ်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2002"/>
        <location filename="../settings.cpp" line="2683"/>
        <source>Select ePub file</source>
        <translation>ePub ဖိုင် ရွေးပါ</translation>
    </message>
    <message>
        <source>TMS History Copied (up to 2 years of assignments)</source>
        <translation type="vanished">TMS မှတ်တမ်း ကူးပြီးပြီ (အများဆုံး ၂ နှစ်စာ တာဝန်)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2031"/>
        <source>Warning: Make sure this file comes from a trusted source. Continue?</source>
        <translation>သတိပေးချက်။ ။ဒီဖိုင်က ယုံကြည်ရတဲ့နေရာက လာတယ်ဆိုတာ သေချာအောင် လုပ်ပါ။ ဆက်လုပ်မလား?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2035"/>
        <source>Command File</source>
        <translation>ညွှန်ကြားချက် ဖိုင်</translation>
    </message>
    <message>
        <source>Last synchronized</source>
        <translation type="vanished">နောက်ဆုံး တပြေးညီ ချိန်ညှိခဲ့</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2416"/>
        <location filename="../settings.cpp" line="2466"/>
        <source>Meeting</source>
        <translation>အစည်းအဝေး</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2416"/>
        <source>Remove the whole meeting? (Use only to remove invalid data from database)</source>
        <translation>စည်းဝေးတစ်ခုလုံး ဖယ်လိုက်မလား? (Database က မဆိုင်တဲ့ အချက်အလက်တွေကို ဖယ်ဖို့အတွက်ပဲ သုံးပါ)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2458"/>
        <source>Enter source material here</source>
        <translation>ရင်းမြစ် အချက်အလက် ဒီနေရာမှာ ရိုက်ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2466"/>
        <source>Remove this talk? (Use only to remove invalid data from database)</source>
        <translation>ဒီဟောပြောချက်ကို ဖယ်လိုက်မလား? (Database က မဆိုင်တဲ့ အချက်အလက်တွေကို ဖယ်ဖို့အတွက်ပဲ သုံးပါ)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2496"/>
        <source>Bible Reading</source>
        <translation>ကျမ်းစာဖတ်ရှုခြင်း</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2497"/>
        <source>Song 1</source>
        <translation>သီချင်း ၁</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2498"/>
        <source>Song 2</source>
        <translation>သီချင်း ၂</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2499"/>
        <source>Song 3</source>
        <translation>သီချင်း ၃</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2556"/>
        <source>Meeting Item</source>
        <translation>စည်းဝေးသုံး</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2559"/>
        <source>Timing</source>
        <translation>အချိန်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2703"/>
        <source>Study Number</source>
        <translation>သင်ခန်းစာနံပါတ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2704"/>
        <source>Study Name</source>
        <translation>သင်ခန်းစာခေါင်းစဥ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2771"/>
        <source>Are you sure you want to permanently delete your cloud data?</source>
        <translation>အွန်လိုင်း ဒေတာ အတည်ဖျက်ချင်တာ သေချာလား?</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2784"/>
        <source>The cloud data has now been deleted.</source>
        <translation>အွန်လိုင်း ဒေတာ ဖျက်လိုက်ပြီ။</translation>
    </message>
    <message>
        <source>Email</source>
        <translation type="vanished">အီးမေးလ်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="833"/>
        <location filename="../settings.cpp" line="1105"/>
        <location filename="../settings.cpp" line="1173"/>
        <location filename="../settings.cpp" line="1332"/>
        <location filename="../settings.cpp" line="1368"/>
        <location filename="../settings.cpp" line="1454"/>
        <source>Language id</source>
        <translation>ဘာသာစကား အမှတ်အသား</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1004"/>
        <source>Public talk number missing</source>
        <translation>ဟောပြောချက် နံပါတ် မထည့်ရသေး</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1007"/>
        <source>Public talk subject missing</source>
        <translation>ဟောပြောချက် အကြောင်းအရာ မထည့်ရသေး</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1028"/>
        <source>Public talk is already saved!</source>
        <translation>ဟောပြောချက် သိမ်းထားပြီးသား!</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1062"/>
        <source>Public talk added to database</source>
        <translation>အချက်အလက် ရင်းမြစ်မှာ ဟောပြောချက် ထည့်လိုက်ပြီ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1069"/>
        <location filename="../settings.cpp" line="1301"/>
        <location filename="../settings.cpp" line="1540"/>
        <location filename="../settings.cpp" line="1608"/>
        <location filename="../settings.cpp" line="1692"/>
        <location filename="../settings.cpp" line="1788"/>
        <source>Adding failed</source>
        <translation>ပေါင်းထည့်တာ မအောင်မြင်ပါ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="607"/>
        <source>Exception</source>
        <translation>ခြွင်းချက်</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="610"/>
        <source>Meeting 1</source>
        <translation>အစည်းအဝေး ၁</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="611"/>
        <source>Meeting 2</source>
        <translation>အစည်းအဝေး ၂</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="574"/>
        <source>Database restored. The program will be restarted.</source>
        <translation>Database restored.The program will be restarted.</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="2742"/>
        <source>Remove ALL studies? (Use only to remove invalid data from database)</source>
        <translation>သွန်သင်ချက် အားလုံး ဖယ်လိုက်မလား? (အချက်အလက် ရင်းမြစ်က မမှန်တဲ့ အချက်အလက်တွေကို ဖျက်ဖို့အတွက်ပဲ သုံးပါ)</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1100"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1099"/>
        <source>Id</source>
        <translation>အမှတ်အသား</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="1102"/>
        <source>Phone</source>
        <translation>ဖုန်း</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="538"/>
        <source>Save database</source>
        <translation>Save database</translation>
    </message>
    <message>
        <location filename="../settings.cpp" line="546"/>
        <source>Database backuped</source>
        <translation>Database backuped.</translation>
    </message>
</context>
<context>
    <name>SmtpClient</name>
    <message>
        <location filename="../smtp/smtpclient.cpp" line="392"/>
        <source>Sender&apos;s name</source>
        <translation>ပို့သူ နာမည်</translation>
    </message>
    <message>
        <location filename="../smtp/smtpclient.cpp" line="397"/>
        <source>Sender&apos;s email</source>
        <translation>ပို့သူ အီးမေးလ်</translation>
    </message>
</context>
<context>
    <name>StartSliderPage1.ui</name>
    <message>
        <location filename="../startup/StartSliderPage1.ui.qml" line="13"/>
        <source>Welcome to theocbase</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StudentAssignmentDialog</name>
    <message>
        <source>Theme</source>
        <translation type="vanished">အဓိကအကြောင်းအရာ</translation>
    </message>
    <message>
        <source>Source</source>
        <translation type="vanished">ရင်းမြစ်</translation>
    </message>
    <message>
        <source>Student</source>
        <translation type="vanished">ကျောင်းသား</translation>
    </message>
    <message>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation type="vanished">အားလုံး</translation>
    </message>
    <message>
        <source>Assistant</source>
        <translation type="vanished">အကူ</translation>
    </message>
    <message>
        <source>With Student</source>
        <comment>Dropdown column title</comment>
        <translation type="vanished">ကျောင်းသားပါတာ</translation>
    </message>
    <message>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation type="vanished">အကူက ဆန့်ကျင်ဘက် လိင် မဖြစ်သင့်ဘူး။</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation type="vanished">ဇာတ်အိမ်</translation>
    </message>
    <message>
        <source>Result</source>
        <translation type="vanished">ရလဒ်</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation type="vanished">ပြီးသွားပြီ</translation>
    </message>
    <message>
        <source>Volunteer</source>
        <translation type="vanished">စေတနာ့ဝန်ထမ်း</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation type="vanished">အချိန်</translation>
    </message>
    <message>
        <source>Current Study</source>
        <translation type="vanished">လက်ရှိ သွန်သင်ချက်</translation>
    </message>
    <message>
        <source>Exercise Completed</source>
        <translation type="vanished">ပြီးသွားတဲ့ လေ့ကျင့်ခန်း</translation>
    </message>
    <message>
        <source>Next Study</source>
        <translation type="vanished">နောက်သွန်သင်ချက်</translation>
    </message>
    <message>
        <source>Note</source>
        <translation type="vanished">မှတ်စုတို</translation>
    </message>
    <message>
        <source>Cancel</source>
        <comment>Cancel button</comment>
        <translation type="vanished">ဖျက်သိမ်းပါ</translation>
    </message>
</context>
<context>
    <name>StudentAssignmentPanel</name>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="65"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="79"/>
        <source>Source</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="94"/>
        <source>Student</source>
        <translation>ကျောင်းသား</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="102"/>
        <location filename="../qml/StudentAssignmentPanel.qml" line="149"/>
        <source>All</source>
        <comment>Dropdown column title</comment>
        <translation>အားလုံး</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="138"/>
        <source>Assistant</source>
        <translation>အကူ</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="148"/>
        <source>With Student</source>
        <comment>Dropdown column title</comment>
        <translation>ကျောင်းသားနဲ့</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="163"/>
        <source>The assistant should not be someone of the opposite sex.</source>
        <translation>အကူက လိင်ခြားသူ မဖြစ်သင့်။</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="185"/>
        <source>Study point</source>
        <translation>သင်ခန်းစာအချက်</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="202"/>
        <source>Result</source>
        <translation>ရလဒ်</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="208"/>
        <source>Completed</source>
        <translation>ပြီးပြီ</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="225"/>
        <source>Volunteer</source>
        <translation>စေတနာ့ဝန်ထမ်း</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="260"/>
        <source>Timing</source>
        <translation>အချိန်</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="277"/>
        <source>Current Study</source>
        <translation>လက်ရှိသင်ခန်းစာ</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="293"/>
        <source>Exercise Completed</source>
        <translation>လေ့ကျင့်ခန်း ပြီးပြီ</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="303"/>
        <source>Next Study</source>
        <translation>နောက်သင်ခန်းစာ</translation>
    </message>
    <message>
        <location filename="../qml/StudentAssignmentPanel.qml" line="324"/>
        <source>Note</source>
        <translation>မှတ်ချက်</translation>
    </message>
</context>
<context>
    <name>TerritoryAddAddressForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="53"/>
        <source>Address</source>
        <translation>လိပ်စာ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="115"/>
        <source>Street:</source>
        <translation>လမ်း</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="125"/>
        <source>Postalcode:</source>
        <translation>စာပို့သင်္ကေတ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="147"/>
        <source>Search</source>
        <comment>Search address</comment>
        <translation>ရှာပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="162"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>နိုင်ငံ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="179"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>ပြည်နယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="197"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>တိုင်း/ပြည်နယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="75"/>
        <source>Country:</source>
        <translation>နိုင်ငံ:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="85"/>
        <source>State:</source>
        <comment>Administrative area level 1</comment>
        <translation>ပြည်နယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="95"/>
        <source>City:</source>
        <comment>Locality</comment>
        <translation>မြို့</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="105"/>
        <source>District:</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>ခရိုင်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="135"/>
        <source>No.:</source>
        <translation>နံပါတ်:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="215"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>မြို့</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="232"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>ခရိုင်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="249"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>လမ်း</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="266"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>နံပါတ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="283"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>စာပို့သင်္ကေတ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="300"/>
        <source>Latitude</source>
        <translation>လတ္တီတွဒ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="317"/>
        <source>Longitude</source>
        <translation>လောင်ဂျီတွဒ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="349"/>
        <source>OK</source>
        <translation>အိုကေ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddAddressForm.ui.qml" line="354"/>
        <source>Cancel</source>
        <translation>ဖျက်သိမ်းပါ</translation>
    </message>
</context>
<context>
    <name>TerritoryAddStreetForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="32"/>
        <source>Select the streets to be added to the territory:</source>
        <comment>Add street names to territoy</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="53"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation type="unfinished">လမ်း</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="74"/>
        <source>OK</source>
        <translation type="unfinished">အိုကေ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddStreetForm.ui.qml" line="79"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryAddressList</name>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="156"/>
        <source>Address</source>
        <translation>လိပ်စာ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="197"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="256"/>
        <source>Undefined [%1]</source>
        <comment>Undefined territory address type</comment>
        <translation>မသတ်မှတ်ရသေး [%1]</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="299"/>
        <source>Edit address</source>
        <translation>လိပ်စာ ပြင်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="299"/>
        <source>Add address</source>
        <translation type="unfinished">လိပ်စာ ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="347"/>
        <source>Add address</source>
        <comment>Add address to territory</comment>
        <translation>လိပ်စာ ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="348"/>
        <source>Please select an address in the list of search results.</source>
        <comment>Add address to territory</comment>
        <translation>ကျေးဇူးပြု၍ ရှာဖွေမှု ရလဒ်စာရင်းတွေထဲက လိပ်စာတစ်ခုကို ရွေးပါ။</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="407"/>
        <location filename="../qml/TerritoryAddressList.qml" line="417"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>လိပ်စာ ရှာမယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressList.qml" line="408"/>
        <source>No address found.</source>
        <comment>Add or edit territory address</comment>
        <translation>လိပ်စာ မတွေ့ပါ</translation>
    </message>
</context>
<context>
    <name>TerritoryAddressListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="39"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="61"/>
        <source>Territory-ID</source>
        <translation>ရက်ကွက် ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="83"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>နိုင်ငံ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="106"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>ပြည်နယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="129"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>တိုင်း/ပြည်နယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="173"/>
        <source>District</source>
        <comment>Sublocality</comment>
        <translation>ခရိုင်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="151"/>
        <source>City</source>
        <comment>Locality</comment>
        <translation>မြို့</translation>
    </message>
    <message>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation type="vanished">လမ်း</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="196"/>
        <source>Street</source>
        <comment>Street name</comment>
        <translation type="unfinished">လမ်း</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="221"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>နံပါတ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="245"/>
        <source>Postal code</source>
        <comment>Mail code, ZIP</comment>
        <translation>စာပို့သင်္ကေတ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="268"/>
        <source>Geometry</source>
        <comment>Coordinate geometry of the address</comment>
        <translation>ဂျီသြမေထြီ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="290"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="300"/>
        <source>Type</source>
        <comment>Type of address</comment>
        <translation>အမျိုးအစား</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="325"/>
        <source>Add new address</source>
        <translation>လိပ်စာအသစ် ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="336"/>
        <source>Edit selected address</source>
        <translation>ရွေးထားတဲ့ လိပ်စာကို ပြင်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryAddressListForm.ui.qml" line="347"/>
        <source>Remove selected address</source>
        <translation>ရွေးချယ်ထားတဲ့လိပ်စာကို ဖယ်ရှားပါ</translation>
    </message>
</context>
<context>
    <name>TerritoryImportForm.ui</name>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="78"/>
        <source>Filename:</source>
        <translation>ဖိုင်နာမည်။  ။</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="133"/>
        <source>Match KML Fields</source>
        <comment>Choose which data fields in kml-file correspond to which territory properties</comment>
        <translation>KML အကွက် တိုက်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="55"/>
        <source>Territory boundaries</source>
        <comment>Territory data import option</comment>
        <translation>ရပ်ကွက် နယ်နိမိတ်မျဥ်းများ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="63"/>
        <source>Addresses</source>
        <comment>Territory data import option</comment>
        <translation>လိပ်စာများ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="150"/>
        <source>Name:</source>
        <comment>Territory data import KML Field</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="163"/>
        <source>Description:</source>
        <comment>Territory data import KML Field</comment>
        <translation>ဖော်ပြချက်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="180"/>
        <source>Search by &quot;Description&quot; if territory is not found by &quot;Name&quot;</source>
        <translation>ရပ်ကွက်ကို &quot;နာမည်&quot; နဲ့ ရှာလို့မတွေ့ရင် &quot;ဖော်ပြချက်&quot; နဲ့ ရှာပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="195"/>
        <source>Territory No.</source>
        <comment>Territory number</comment>
        <translation>ရပ်ကွက် နံပါတ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="201"/>
        <source>Locality</source>
        <comment>Territory locality</comment>
        <translation>နယ်မြေ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="207"/>
        <source>Remark</source>
        <translation>မှတ်ချက်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="238"/>
        <source>Match Fields</source>
        <comment>Choose which fields in the import-file correspond to the address data</comment>
        <translation>အကွက်တွေ တိုက်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="255"/>
        <source>Address:</source>
        <comment>Territory address import field</comment>
        <translation>လိပ်စာ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="266"/>
        <source>Name:</source>
        <comment>Territory address import field</comment>
        <translation>နာမည်။  ။</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="304"/>
        <source>Address type:</source>
        <comment>Address type for territory address import</comment>
        <translation>လိပ်စာအမျိုးအစား</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="326"/>
        <source>Output filename for failed addresses:</source>
        <comment>Territory address import</comment>
        <translation>အသုံးပြုလို့မရတဲ့ လိပ်စာတွေအတွက် အထွက်ဖိုင်နာမည်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="396"/>
        <source>Import</source>
        <translation>တင်သွင်းမယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryImportForm.ui.qml" line="405"/>
        <source>Close</source>
        <translation>ပိတ်မယ်</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">အိုကေ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">ဖျက်သိမ်းပါ</translation>
    </message>
</context>
<context>
    <name>TerritoryManagement</name>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="132"/>
        <source>Group by:</source>
        <translation>အုပ်စုအလိုက်စီမယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="139"/>
        <source>City</source>
        <comment>Group territories by city</comment>
        <translation>မြို့</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="140"/>
        <source>Publisher</source>
        <comment>Group territories by publisher</comment>
        <translation type="unfinished">ကြေညာသူ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="141"/>
        <source>Type</source>
        <comment>Group territories by type of the territory</comment>
        <translation>အမျိုးအစား</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="142"/>
        <source>Worked through</source>
        <comment>Group territories by time frame they have been worked through</comment>
        <translation>ပြီးစီး</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="214"/>
        <source>Number</source>
        <comment>Territory number according to the S-12 Territory Map Card</comment>
        <translation type="unfinished">နံပါတ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="220"/>
        <source>Locality</source>
        <comment>Locality according to the S-12 Territory Map Card</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="242"/>
        <source>Add new territory</source>
        <translation>ရပ်ကွက်အသစ်ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="257"/>
        <source>Remove selected territory</source>
        <translation>ရွေးထားတဲ့ ရပ်ကွက်ကို ဖယ်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="353"/>
        <source>Remark</source>
        <translation>မှတ်ချက်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="372"/>
        <source>Type:</source>
        <comment>Territory-type that is used to group territories</comment>
        <translation>အမျိူးအစား</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="399"/>
        <source>City:</source>
        <comment>Cityname that is used to group territories</comment>
        <translation>မြို့</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="464"/>
        <source>Assignments</source>
        <translation>တာဝန်များ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="527"/>
        <source>Publisher</source>
        <translation type="unfinished">ကြေညာသူ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="831"/>
        <source>Streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="870"/>
        <source>Addresses</source>
        <translation>လိပ်စာများ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="298"/>
        <source>No.:</source>
        <translation>နံပါတ်:</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="309"/>
        <source>Number</source>
        <translation>နံပါတ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="320"/>
        <source>Locality:</source>
        <translation>နယ်မြေ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="330"/>
        <source>Locality</source>
        <translation>နယ်မြေ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="434"/>
        <source>Map</source>
        <comment>Territory map</comment>
        <translation>မြေပုံ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="499"/>
        <source>ID</source>
        <translation>ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="513"/>
        <source>Publisher-ID</source>
        <translation>ကြေညာသူ ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="627"/>
        <source>Checked out</source>
        <comment>Date when the publisher obtained the territory</comment>
        <translation>ရပ်ကွက်ထုတ်ယူတဲ့ ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="633"/>
        <source>Checked back in</source>
        <comment>Date when the territory is returned</comment>
        <translation>ရပ်ကွက်ပြန်အပ်တဲ့ ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="784"/>
        <source>Add new assignment</source>
        <translation>တာဝန်အသစ်ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="808"/>
        <source>Remove selected assignment</source>
        <translation>ရွေးထားတဲ့ တာဝန်ကို ဖယ်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryManagement.qml" line="592"/>
        <source>Publisher</source>
        <comment>Dropdown column title</comment>
        <translation>ကြေညာသူ</translation>
    </message>
</context>
<context>
    <name>TerritoryMap</name>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="334"/>
        <source>Search address</source>
        <comment>Add or edit territory address</comment>
        <translation>လိပ်စာ ရှာမယ်</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="760"/>
        <source>The new boundary overlaps %n territory(ies):</source>
        <comment>Add or edit territory boundary</comment>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="762"/>
        <source>Do you want to assign overlapping areas to the current territory?
Select &apos;No&apos; if overlapping areas should remain in their territories and to add only the part, that doesn&apos;t overlap other territories.</source>
        <comment>Add or edit territory boundary</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="796"/>
        <source>Add address to selected territory</source>
        <translation>ရွေးထားတဲ့ ရပ်ကွက်တွေကို လိပ်စာထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="804"/>
        <source>Assign to selected territory</source>
        <comment>Reassign territory address</comment>
        <translation>ရွေးထားတဲ့ ရပ်ကွက် တာဝန်ပေးပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="812"/>
        <source>Remove address</source>
        <comment>Delete territory address</comment>
        <translation>လိပ်စာ ဖြုတ်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="843"/>
        <source>Import territory data</source>
        <comment>Territory data import dialog</comment>
        <translation type="unfinished">ရပ်ကွက်အချက်အလက်များကို ထည့်သွင်းပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="902"/>
        <source>%1 of %2 address(es) imported.</source>
        <comment>Territory address import progress</comment>
        <translation>%2 ထဲက %1 လိပ်စာ(တွေ) တင်သွင်းပြီးပြီ။</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="917"/>
        <source>Import territory boundaries</source>
        <comment>Territory import dialog</comment>
        <translation>ရပ်ကွက် နယ်နိမိတ်မျဥ်းကို ထည့်သွင်းပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="918"/>
        <source>The selected fields should be different.</source>
        <comment>Territory import from KML file</comment>
        <translation type="unfinished">ရွေးနှိပ်ထားတဲ့ အကွက်တွေ မတူရဘူး</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="927"/>
        <location filename="../qml/TerritoryMap.qml" line="938"/>
        <location filename="../qml/TerritoryMap.qml" line="953"/>
        <location filename="../qml/TerritoryMap.qml" line="962"/>
        <location filename="../qml/TerritoryMap.qml" line="995"/>
        <location filename="../qml/TerritoryMap.qml" line="1004"/>
        <source>Import territory data</source>
        <comment>Territory import dialog</comment>
        <translation>ရပ်ကွက်အချက်အလက်များကို ထည့်သွင်းပါ</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="928"/>
        <source>%n territory(ies) imported or updated.</source>
        <comment>Number of territories imported or updated</comment>
        <translation>
            <numerusform>%n ရပ်ကွက်(တွေ) တင်သွင်း/အသစ်ပြင်ပြီးပြီ။</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="939"/>
        <source>The import file could not be read.</source>
        <comment>Territory address import from file</comment>
        <translation type="unfinished">တင်သွင်းလိုက်တဲ့ဖိုင်ကို ဖတ်လို့မရဘူး။</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="954"/>
        <source>Please select the address and name fields.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>လိပ်စာနဲ့ နာမည် အကွက်တွေကို ရွေးနှိပ်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="963"/>
        <source>The selected fields should be different.</source>
        <comment>Fields for territory address import from file</comment>
        <translation>ရွေးနှိပ်ထားတဲ့ အကွက်တွေ မတူရဘူး</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="971"/>
        <source>Import territory addresses</source>
        <comment>Territory import dialog</comment>
        <translation>ရပ်ကွက် လိပ်စာများကို ထည့်သွင်းပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="972"/>
        <source>The addresses will be added to the current territory. Please select a territory first.</source>
        <comment>Territory address import from file</comment>
        <translation>လိပ်စာတွေကို လက်ရှိရပ်ကွက်ထဲမှာ ထည့်မှာဖြစ်တယ်။ ကျေးဇူးပြု၍ ရပ်ကွက်ကို အရင်ရွေးချယ်ပါ။</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/TerritoryMap.qml" line="996"/>
        <source>%n address(es) imported.</source>
        <comment>Number of addresses imported</comment>
        <translation>
            <numerusform>%n လိပ်စာ(တွေ) တင်သွင်းပြီးပြီ။</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1006"/>
        <source>No valid territory selected.</source>
        <comment>Territory boundary import</comment>
        <translation>အသုံးပြုလို့ရတဲ့ ရပ်ကွက် မရွေးထားပါ။</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1008"/>
        <source>The import file could not be read.</source>
        <comment>Territory boundary import</comment>
        <translation>တင်သွင်းလိုက်တဲ့ဖိုင်ကို ဖတ်လို့မရဘူး။</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1040"/>
        <source>Address</source>
        <comment>Default Address-field for territory address import</comment>
        <translation>လိပ်စာ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1042"/>
        <source>Name</source>
        <comment>Default Name-field for territory address import</comment>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1071"/>
        <location filename="../qml/TerritoryMap.qml" line="1079"/>
        <source>Open file</source>
        <translation>ဖိုင် ဖွင့်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1072"/>
        <location filename="../qml/TerritoryMap.qml" line="1073"/>
        <source>KML files (*.kml)</source>
        <comment>Filedialog pattern</comment>
        <translation>KML ဖိုင်များ (*.kml)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1072"/>
        <location filename="../qml/TerritoryMap.qml" line="1080"/>
        <location filename="../qml/TerritoryMap.qml" line="1088"/>
        <source>All files (*)</source>
        <comment>Filedialog pattern</comment>
        <translation>ဖိုင် အားလုံး (*)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1080"/>
        <location filename="../qml/TerritoryMap.qml" line="1081"/>
        <location filename="../qml/TerritoryMap.qml" line="1088"/>
        <location filename="../qml/TerritoryMap.qml" line="1089"/>
        <source>CSV files (*.csv)</source>
        <comment>Filedialog pattern</comment>
        <translation>CSV ဖိုင်များ (*.csv)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1080"/>
        <location filename="../qml/TerritoryMap.qml" line="1088"/>
        <source>Text files (*.txt)</source>
        <comment>Filedialog pattern</comment>
        <translation>Text ဖိုင်များ (*.txt)</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMap.qml" line="1087"/>
        <source>Save file</source>
        <translation>ဖိုင် သိမ်းမယ်</translation>
    </message>
</context>
<context>
    <name>TerritoryMapForm.ui</name>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="65"/>
        <source>search</source>
        <comment>Search address in territory map</comment>
        <translation>ရှာပါ</translation>
    </message>
    <message>
        <source>Import boundaries</source>
        <comment>Import territory boundaries from a file</comment>
        <translation type="vanished">နယ်နိမိတ်မျဥ်းတွေကို ထည့်သွင်းပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="149"/>
        <source>Import data</source>
        <comment>Import territory data from a file</comment>
        <translation>အချက်အလက်ကို ထည့်သွင်းပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="193"/>
        <source>Switch edit mode</source>
        <comment>Switch edit mode on territory map</comment>
        <translation>ပြင်ဆင်နည်း ပြောင်းမယ်</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="207"/>
        <source>Create boundary</source>
        <comment>Create a new boundary for the territory</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="219"/>
        <source>Remove boundary</source>
        <comment>Remove boundary or geometry of the territory</comment>
        <translation>နယ်နိမိတ်မျဥ်းကို ပယ်ဖျက်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="231"/>
        <source>Split territory</source>
        <comment>Cut territory in two parts</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="160"/>
        <source>Zoom full</source>
        <comment>Zoom full to display all territories</comment>
        <translation>မျက်နှာပြင်အပြည့် ချဲ့ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="170"/>
        <source>Show/hide territories</source>
        <comment>Show/hide boundaries of territories</comment>
        <translation>ရပ်ကွက်တွေကို ပြပါ/ဝှက်ပါ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryMapForm.ui.qml" line="181"/>
        <source>Show/hide markers</source>
        <comment>Show/hide address markers of territories</comment>
        <translation>ညွှန်မှတ်တွေကို ပြပါ/ဝှက်ပါ</translation>
    </message>
</context>
<context>
    <name>TerritoryStreetList</name>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="102"/>
        <source>Street name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="209"/>
        <source>Undefined [%1]</source>
        <comment>Undefined street type</comment>
        <translation type="unfinished">မသတ်မှတ်ရသေး [%1]</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="251"/>
        <source>Add streets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="269"/>
        <location filename="../qml/TerritoryStreetList.qml" line="280"/>
        <source>Add streets</source>
        <comment>Add streets to a territory</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetList.qml" line="270"/>
        <source>No streets found.</source>
        <comment>Add streets to a territory</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryStreetListForm.ui</name>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="38"/>
        <source>ID</source>
        <translation type="unfinished">ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="60"/>
        <source>Territory-ID</source>
        <translation type="unfinished">ရက်ကွက် ID</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="82"/>
        <source>Street name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="91"/>
        <source>From number</source>
        <comment>Number range of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="100"/>
        <source>To number</source>
        <comment>Number range of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="109"/>
        <source>Quantity</source>
        <comment>Quantity of addresses in the street</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="119"/>
        <source>Type</source>
        <comment>Type of street</comment>
        <translation type="unfinished">အမျိုးအစား</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="128"/>
        <source>Geometry</source>
        <comment>Line geometry of the street</comment>
        <translation type="unfinished">ဂျီသြမေထြီ</translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="166"/>
        <source>Add new street</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/TerritoryStreetListForm.ui.qml" line="177"/>
        <source>Remove selected street</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TerritoryTreeModel</name>
    <message>
        <location filename="../territorymanagement.cpp" line="118"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>&lt; ၆လ</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="119"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>၆လ ကနေ ၁၂လ</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="120"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>လွန်ခဲ့တဲ့ ၁၂လ အထက်</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="303"/>
        <source>Not worked</source>
        <comment>Group text for territories that are not worked yet.</comment>
        <translation>မလုပ်ရသေး</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="305"/>
        <location filename="../territorymanagement.cpp" line="457"/>
        <location filename="../territorymanagement.cpp" line="468"/>
        <source>Not assigned</source>
        <comment>Value of the field, the territories are grouped by, is empty.</comment>
        <translation>မချရသေး</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territory</source>
        <translation>ရပ်ကွက်</translation>
    </message>
    <message>
        <location filename="../territorymanagement.cpp" line="316"/>
        <location filename="../territorymanagement.cpp" line="343"/>
        <location filename="../territorymanagement.cpp" line="485"/>
        <source>territories</source>
        <translation>ရပ်ကွက်</translation>
    </message>
</context>
<context>
    <name>TodoPanel</name>
    <message>
        <location filename="../qml/TodoPanel.qml" line="114"/>
        <source>To Do List</source>
        <translation>လုပ်စရာ စာရင်း</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="209"/>
        <source>Cannot schedule this item until these fields are fixed: %1</source>
        <translation>ဒီအပိုင်းကို စီစဥ်ဆွဲလို့ မရဘူး၊ ဒီအကွက်တွေကို မပြင်ဘူးဆိုရင် %1</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="284"/>
        <source>Incoming</source>
        <comment>incoming speaker</comment>
        <translation>ဧည့်ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../qml/TodoPanel.qml" line="284"/>
        <source>Outgoing</source>
        <comment>outgoing speaker</comment>
        <translation>သွားလည်ဟောပြောသူ</translation>
    </message>
</context>
<context>
    <name>ValidationTextInput</name>
    <message>
        <location filename="../qml/ValidationTextInput.qml" line="10"/>
        <source>Text</source>
        <translation>စာသား</translation>
    </message>
</context>
<context>
    <name>WEMeetingChairmanPanel</name>
    <message>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="69"/>
        <source>Song</source>
        <translation>သီချင်း</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingChairmanPanel.qml" line="45"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
</context>
<context>
    <name>WEMeetingFinalTalkPanel</name>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="48"/>
        <source>Service Talk</source>
        <comment>Circuit overseer&apos;s talk in the wekeend meeting</comment>
        <translation>လုပ်ငန်းတော်ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingFinalTalkPanel.qml" line="66"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
</context>
<context>
    <name>WEMeetingModule</name>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="97"/>
        <source>Weekend Meeting</source>
        <translation>အပတ်ဆုံး အစည်းအဝေး</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="109"/>
        <location filename="../qml/WEMeetingModule.qml" line="316"/>
        <source>Song and Prayer</source>
        <translation>သီချင်း နဲ့ ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="110"/>
        <location filename="../qml/WEMeetingModule.qml" line="317"/>
        <source>Song %1 and Prayer</source>
        <translation>သီချင်း %1 နဲ့ ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="145"/>
        <source>PUBLIC TALK</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="163"/>
        <source>Send to To Do List</source>
        <translation>လုပ်စရာ စာရင်း ထဲကို ပို့မယ်</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="169"/>
        <source>Move to different week</source>
        <translation>တခြားအပတ်ကို ရွှေ့မယ်</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="178"/>
        <source>Clear Public Talk selections</source>
        <translation>လူထုဟောပြောချက် ရွေးချယ်မှု ဖျက်မယ်</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="226"/>
        <source>WATCHTOWER STUDY</source>
        <translation>ကင်းမျှော်စင်သင်တန်း</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="235"/>
        <source>Song %1</source>
        <translation>သီချင်း %1</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="256"/>
        <source>Import WT...</source>
        <translation>ကင်းမျှော်စင် တင်သွင်း...</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="284"/>
        <source>Conductor</source>
        <translation>သင်တန်းမှူး</translation>
    </message>
    <message>
        <location filename="../qml/WEMeetingModule.qml" line="285"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
</context>
<context>
    <name>WatchtowerSongPanel</name>
    <message>
        <location filename="../qml/WatchtowerSongPanel.qml" line="25"/>
        <source>Song</source>
        <translation>သီချင်း</translation>
    </message>
</context>
<context>
    <name>WatchtowerStudyPanel</name>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="46"/>
        <source>Watchtower Issue</source>
        <translation>ကင်းမျှော်စင်လထုတ်</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="68"/>
        <source>Article</source>
        <comment>The number of Watchtower article</comment>
        <translation>ဆောင်းပါး</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="80"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="99"/>
        <source>Conductor</source>
        <comment>Watchtower study conductor</comment>
        <translation>သင်တန်းမှူး</translation>
    </message>
    <message>
        <location filename="../qml/WatchtowerStudyPanel.qml" line="121"/>
        <source>Reader</source>
        <comment>Watchtower study reader</comment>
        <translation>စာဖတ်သူ</translation>
    </message>
</context>
<context>
    <name>cPersonComboBox</name>
    <message>
        <source>Outside speaker</source>
        <translation type="vanished">ဧည့်ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="238"/>
        <source>Brother has other meeting parts</source>
        <translation>ညီကို တခြားတာဝန် ရှိနေတယ်</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="233"/>
        <source>Person unavailable</source>
        <translation>သူ မအားပါ</translation>
    </message>
    <message>
        <location filename="../cpersoncombobox.cpp" line="228"/>
        <source>Outgoing speaker</source>
        <translation>သွားလည် ဟောပြောသူ</translation>
    </message>
</context>
<context>
    <name>ccongregation</name>
    <message>
        <location filename="../ccongregation.cpp" line="118"/>
        <source>(Missing Record)</source>
        <comment>database is now missing this entry</comment>
        <translation>(မှတ်တမ်း မရှိပါ)</translation>
    </message>
</context>
<context>
    <name>checkupdates</name>
    <message>
        <location filename="../checkupdates.cpp" line="100"/>
        <source>New version available</source>
        <translation>Version အသစ် ရပြီ</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="102"/>
        <source>Download</source>
        <translation>ဒေါင်းလုပ် ဆွဲယူပါ</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="105"/>
        <source>New version...</source>
        <translation>Version အသစ်...</translation>
    </message>
    <message>
        <location filename="../checkupdates.cpp" line="116"/>
        <source>No new update available</source>
        <translation>အသစ်တင်ထားတာ မရှိပါ</translation>
    </message>
</context>
<context>
    <name>csync</name>
    <message>
        <location filename="../csync.cpp" line="112"/>
        <source>File reading failed</source>
        <translation>ဖိုင် ဖတ်လို့မရပါ</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="450"/>
        <source>XML file generated in the wrong version.</source>
        <translation>XML file generated in the wrong version.</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="517"/>
        <source>Persons - added </source>
        <translation>လူ - ပေါင်းထည့်ပြီးပြီ </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="555"/>
        <source>Persons - updated </source>
        <translation>လူ - ပြင်ပြီးပြီ </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="586"/>
        <source>Theocratic ministry school - schedule added </source>
        <translation>သီအိုကရက်တစ် ဓမ္မအမှုကျောင်း - အစီအစဥ် ပေါင်းထည့်ပြီးပြီ </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="588"/>
        <source>Theocratic ministry school - schedule updated </source>
        <translation>သီအိုကရက်တစ် ဓမ္မအမှုကျောင်း - အစီအစဥ် အသစ်ပြင်ပြီးပြီ </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="784"/>
        <source>Theocratic Ministry School - Study updated</source>
        <translation>သီအိုကရက်တစ် ဓမ္မအမှုကျောင်း - သွန်သင်ချက် အသစ်တင်ပြီးပြီ</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="798"/>
        <source>Theocratic Ministry School - Study added</source>
        <translation>သီအိုကရက်တစ် ဓမ္မအမှုကျောင်း - သွန်သင်ချက် ထပ်ထည့်ထားတယ်</translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="834"/>
        <source>Public talks - theme added </source>
        <translation>လူထုဟောပြောချက် - အကြောင်းအရာ ပေါင်းထည့်ပြီးပြီ </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="892"/>
        <source>Public Talk and WT - schedule updated </source>
        <translation>လူထုဟောပြောချက် နဲ့ ကင်းမျှော်စင် - အစီအစဥ် အသစ်ပြင်ပြီးပြီ </translation>
    </message>
    <message>
        <location filename="../csync.cpp" line="898"/>
        <source>Public Talk and WT - schedule added </source>
        <translation>လူထုဟောပြောချက် နဲ့ ကင်းမျှော်စင် - အစီအစဥ် ပေါင်းထည့်ပြီးပြီ </translation>
    </message>
    <message>
        <source>Congregation Bible Study - schedule added </source>
        <translation type="vanished">အသင်းတော်ကျမ်းစာလေ့လာမှု - အစီအစဥ် ပေါင်းထည့်ပြီးပြီ </translation>
    </message>
    <message>
        <source>Congregation Bible Study - schedule updated </source>
        <translation type="vanished">အသင်းတော်ကျမ်းစာလေ့လာမှု - အစီအစဥ် အသစ်ပြင်ပြီးပြီ </translation>
    </message>
    <message>
        <source>Service Meeting - schedule added </source>
        <translation type="vanished">လုပ်ငန်းတော်စည်းဝေး - အစီအစဥ် ပေါင်းထည့်ပြီးပြီ </translation>
    </message>
    <message>
        <source>Service Meeting - schedule updated </source>
        <translation type="vanished">လုပ်ငန်းတော်စည်းဝေး - အစီအစဥ် အသစ်ပြင်ပြီးပြီ </translation>
    </message>
</context>
<context>
    <name>cterritories</name>
    <message>
        <location filename="../cterritories.cpp" line="210"/>
        <source>Do not call</source>
        <comment>Territory address type</comment>
        <translation>မဟောပါနဲ့</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1512"/>
        <source>Save failed addresses</source>
        <comment>Territory address import</comment>
        <translation>အသုံးပြုလို့မရတဲ့ လိပ်စာတွေ သိမ်းမယ်</translation>
    </message>
    <message>
        <location filename="../cterritories.cpp" line="1513"/>
        <source>The file is in read only mode</source>
        <comment>Save failed addresses in territory data import</comment>
        <translation>ဖိုင်ကို ဖတ်လို့ပဲ ရတယ်</translation>
    </message>
</context>
<context>
    <name>family</name>
    <message>
        <location filename="../family.cpp" line="124"/>
        <source>Not set</source>
        <translation>မချိန်/မသတ်မှတ်ရသေး</translation>
    </message>
</context>
<context>
    <name>googleMediator</name>
    <message>
        <location filename="../googlemediator.cpp" line="198"/>
        <source>OK</source>
        <translation>အိုကေ</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="200"/>
        <source>OK but JSON not available</source>
        <translation>အိုကေ ဒါပေမဲ့ JSON မရပါ</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="202"/>
        <source>Authorizing</source>
        <translation>လုပ်ပိုင်ခွင့် ပေးနေတယ်</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="204"/>
        <source>Authorization Failed</source>
        <translation>လုပ်ပိုင်ခွင့် ပေးတာ မအောင်မြင်ပါ</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="210"/>
        <source>Missing Client ID</source>
        <translation>သုံးစွဲသူရဲ့ အမှတ်အသား မရှိဘူး ဖြစ်နေတယ်</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="212"/>
        <source>Missing Client Secret</source>
        <translation>သုံးစွဲသူရဲ့ လျှို့ဝှက်ချက် မရှိဘူး ဖြစ်နေတယ်</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="214"/>
        <source>Need Authorization Code</source>
        <translation>လုပ်ပိုင်ခွင့် ရ ကုဒ်နံပါတ် လိုတယ်</translation>
    </message>
    <message>
        <location filename="../googlemediator.cpp" line="216"/>
        <source>Need Token Refresh</source>
        <translation>Token Refresh လုပ်ဖို့လိုတယ်</translation>
    </message>
</context>
<context>
    <name>historytable</name>
    <message>
        <location filename="../historytable.ui" line="196"/>
        <source>Number of weeks after selected date</source>
        <translation>ရွေးထားတဲ့ ရက်စွဲ နောက် အပတ်အရေအတွက်</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="246"/>
        <source>Number of weeks to gray after an assignment</source>
        <translation>တာဝန်တစ်ခုပြီးနောက် မီးခိုးရောင်ပြထားရမယ့် အပတ်အရေအတွက်</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="220"/>
        <source>weeks</source>
        <translation>အပတ်တွေ</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="122"/>
        <source>Timeline</source>
        <translation>အချိန်ဇယား</translation>
    </message>
    <message>
        <location filename="../historytable.ui" line="180"/>
        <source>Number of weeks before selected date</source>
        <translation>ရွေးထားတဲ့ ရက်စွဲ မတိုင်ခင် အပတ်အရေအတွက်</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="418"/>
        <source>Publishers</source>
        <comment>History Table (column title)</comment>
        <translation>ကြေညာသူ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="582"/>
        <location filename="../historytable.cpp" line="589"/>
        <source>CBS</source>
        <translation>CBS</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="582"/>
        <location filename="../historytable.cpp" line="684"/>
        <source>C</source>
        <comment>abbreviation of the &apos;conductor&apos;</comment>
        <translation>C</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="589"/>
        <location filename="../historytable.cpp" line="614"/>
        <source>R</source>
        <comment>abbreviation of the &apos;reader&apos;</comment>
        <translation>R</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="611"/>
        <source>H</source>
        <comment>abbreviation of the &apos;highlights&apos;</comment>
        <translation>H</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="623"/>
        <source>TMS</source>
        <translation>TMS</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="750"/>
        <source>SM</source>
        <translation>SM</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="768"/>
        <source>PT</source>
        <comment>abbreviation of &apos;public talk&apos; for history table</comment>
        <translation>ဟောပြော</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="774"/>
        <source>Ch</source>
        <comment>abbreviation of public talk &apos;chairman&apos; for history table</comment>
        <translation>သဘာ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="783"/>
        <source>WT-R</source>
        <comment>abbreviation of &apos;watchtower reader&apos; for history table</comment>
        <translation>စာဖတ်</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="999"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1000"/>
        <source>Theme</source>
        <translation>အဓိကအကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1001"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1002"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1003"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../historytable.cpp" line="1004"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
</context>
<context>
    <name>importwizard</name>
    <message>
        <location filename="../importwizard.cpp" line="40"/>
        <source>Next &gt;</source>
        <translation>အရှေ့&gt;</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="39"/>
        <source>&lt; Back</source>
        <translation>&lt;အနောက်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="41"/>
        <source>Cancel</source>
        <translation>ဖျက်သိမ်းပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="67"/>
        <source>Theocratic school schedule import. Copy full schedule from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>ဟောပြောသွန်သင်ခြင်းသင်တန်း အစီစဥ်ဇယားကို တင်သွင်းပါ။ ကင်းမျှော်စင်စာကြည့်တိုက်ကနေ အစီအစဥ်ဇယားကို ကူးယူပြီး အောက်မှာ ဖြည့်ပါ (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="68"/>
        <source>Check schedule</source>
        <translation>အစီအစဥ်ဇယားကို စစ်ပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="71"/>
        <source>Studies import. Copy studies from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>သွန်သင်ချက်တွေ တင်သွင်းပါ။ ကင်းမျှော်စင်စာကြည့်တိုက် ကနေ သွန်သင်ချက်တွေ ကူးပြီး အောက်မှာ ဖြည့်ပါ (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="72"/>
        <source>Check studies</source>
        <translation>သွန်သင်ချက်တွေ စစ်ပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="75"/>
        <source>Settings import. Copy settings from WTLibrary and paste below (Ctrl + V / cmd + V)</source>
        <translation>ဇာတ်အိမ်တွေ တင်သွင်းပါ။ WTLibrary ကနေ ဇာတ်အိမ်တွေ ကူးပြီး အောက်မှာ ဖြည့်ပါ (Ctrl + V / cmd + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="76"/>
        <source>Check settings</source>
        <translation>ဇာတ်အိမ်တွေ စစ်ပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="80"/>
        <source>Check subjects</source>
        <translation>အကြောင်းအရာတွေ စစ်ပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="83"/>
        <source>Add speakers and congregations. Copy all data to clipboard and paste below (Ctrl + V)</source>
        <translation>ဟောပြောသူ နဲ့ အသင်းတော် ပေါင်းထည့်ပါ။ အချက်အလက်အားလုံး ကူးပြီး အောက်မှာ ဖြည့်ပါ (Ctrl + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="84"/>
        <source>Check data</source>
        <translation>အချက်အလက် စစ်ပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="87"/>
        <source>Add songs. Copy all data to clipboard and paste below (Ctrl + V)</source>
        <translation>သီချင်း ထည့်ပါ။ အချက်အလက်အားလုံးကို ကူးပြီး အောက်မှာ ဖြည့်ပါ (Ctrl + V)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="88"/>
        <source>Check songs</source>
        <translation>သီချင်း စစ်ပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="100"/>
        <source>No schedule to import.</source>
        <translation>တင်သွင်းဖို့ အစီအစဥ်ဇယား မရှိပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="141"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="142"/>
        <source>Please add start date YYYY-MM-DD (eg. 2011-01-03)</source>
        <translation>စမယ့် ရက်စွဲ ထည့်ပါ နှစ်-လ-ရက် (ဥပမာ ၂၀၁၁-၀၁-၀၃)</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="149"/>
        <source>The date is not first day of week (Monday)</source>
        <translation>ရက်စွဲက ရက်တတ်တစ်ပါတ်ရဲ့ ပထမနေ့ (တနင်္လာနေ့) မဟုတ်ဘူး</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="202"/>
        <source>Import songs</source>
        <translation>သီချင်း တင်သွင်းပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="244"/>
        <source>Only brothers</source>
        <translation>ညီကို များသာ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="474"/>
        <source>Subject</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="517"/>
        <source>id</source>
        <translation>id</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="520"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="523"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="526"/>
        <source>Phone</source>
        <translation>ဖုန်း</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="529"/>
        <source>Public talks</source>
        <translation>လူထုဟောပြောချက်များ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="532"/>
        <source>Language</source>
        <translation>ဘာသာစကား</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="549"/>
        <location filename="../importwizard.cpp" line="550"/>
        <source>First name</source>
        <translation>ရှေ့နာမည်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="549"/>
        <location filename="../importwizard.cpp" line="550"/>
        <source>Last name</source>
        <translation>နောက်နာမည်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="178"/>
        <source>Import subjects</source>
        <translation>အကြောင်းအရာ တင်သွင်းပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="179"/>
        <location filename="../importwizard.cpp" line="203"/>
        <source>Choose language</source>
        <translation>ဘာသာစကား ရွေးပါ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="38"/>
        <source>Save to database</source>
        <translation>Save to database</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="79"/>
        <source>Add public talk&apos;s subjects. Copy themes and paste below (Ctrl + V / cmd + V).
Number should be in the first column and theme in the second.</source>
        <translation>လူထုဟောပြောချက် အကြောင်းအရာ ထည့်ပါ။ အကြောင်းအရာကို အောက်မှာ ကူးထည့်ပါ (Ctrl + V / cmd + V). 
ပထမတိုင်မှာ နံပါတ်၊ ဒုတိယတိုင်မှာ အကြောင်းအရာ ရှိသင့်တယ်။</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="233"/>
        <source>date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="236"/>
        <source>number</source>
        <translation>နံပါတ်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="239"/>
        <source>subject</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="242"/>
        <source>material</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="353"/>
        <source>setting</source>
        <translation>ဇာတ်အိမ်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="414"/>
        <source>study</source>
        <translation>သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="600"/>
        <source>Title</source>
        <translation>ခေါင်းစဥ်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="746"/>
        <source>A public talk with the same number is already saved!
Do you want to discontinue the previous talk?

Scheduled talks will be moved to the To Do List.</source>
        <translation>ဟောပြောချက် နံပါတ်တူတာ သိမ်းပြီးသား ဖြစ်နေတယ်!
အရင်တစ်ခုကို ဖျက်လိုက်တော့မလား?

စီစဥ်ထားတဲ့ ဟောပြောချက်တွေ လုပ်ရမယ့်စာရင်းထဲ ရောက်သွားမယ်</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="749"/>
        <source>Previous talk: </source>
        <translation>အရင် ဟောပြောချက်: </translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="750"/>
        <source>New talk: </source>
        <translation>ဟောပြောချက်အသစ်: </translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="871"/>
        <source>Public talk themes not found. Add themes and try again!</source>
        <translation>လူထုဟောပြောချက် အကြောင်းအရာ ရှာမတွေ့ပါ၊ အကြောင်းအရာ ထည့်ပြီး ပြန်ကြိုးစားပါ!</translation>
    </message>
    <message>
        <location filename="../importwizard.cpp" line="898"/>
        <source> rows added</source>
        <translation> rows added</translation>
    </message>
</context>
<context>
    <name>lmmtalktypeedit</name>
    <message>
        <source>Tak Type Editor</source>
        <comment>dialog name</comment>
        <translation type="vanished">ဟောပြောချက်အမျိုးအစား အယ်ဒီတာ</translation>
    </message>
    <message>
        <source>Continue</source>
        <comment>dialog button name</comment>
        <translation type="vanished">ဆက်လုပ်မယ်</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="14"/>
        <source>Talk Name in the Workbook</source>
        <translation>လေ့ကျင့်ခန်းစာစောင်မှာ ဟောပြောချက်နာမည်</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="16"/>
        <source>Meeting Item</source>
        <translation>အစည်းအဝေး အပိုင်း</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.cpp" line="42"/>
        <source>Unknown</source>
        <comment>Unknown talk name</comment>
        <translation>မသိရ</translation>
    </message>
    <message>
        <source>Talk Type Editor</source>
        <comment>dialog name</comment>
        <translation type="vanished">ဟောပြောချက် အမျိုးအစား အယ်ဒီတာ</translation>
    </message>
    <message>
        <location filename="../lmmtalktypeedit.ui" line="14"/>
        <source>Talk Type Editor</source>
        <extracomment>dialog name</extracomment>
        <translation type="unfinished">ဟောပြောချက် အမျိုးအစား အယ်ဒီတာ</translation>
    </message>
</context>
<context>
    <name>logindialog</name>
    <message>
        <location filename="../logindialog.ui" line="33"/>
        <source>Username</source>
        <translation>အသုံးပြုသူနာမည်</translation>
    </message>
    <message>
        <location filename="../logindialog.ui" line="50"/>
        <source>Password</source>
        <translation>လျှို့ဝှက်စာသား</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <source>WEEK STARTING %1</source>
        <translation type="vanished">%1 စမယ့် အပတ်</translation>
    </message>
    <message>
        <source>Midweek Meeting</source>
        <translation type="vanished">ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <source>Weekend Meeting</source>
        <translation type="vanished">တနင်္ဂနွေစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../qml/main.qml" line="292"/>
        <source>Select an assignment on the left to edit</source>
        <translation>ဘယ်ဘက်က တာဝန်ကို ရွေးပြီး ပြင်ပါ</translation>
    </message>
</context>
<context>
    <name>personsui</name>
    <message>
        <location filename="../personsui.ui" line="366"/>
        <source>Sister</source>
        <translation>ညီမ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="390"/>
        <source>Brother</source>
        <translation>ညီကို</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="342"/>
        <source>Servant</source>
        <translation>အမှုထမ်း</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="23"/>
        <source>Publishers</source>
        <translation>ကြေညာသူ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="782"/>
        <source>General</source>
        <translation>ယေဘုယျ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="990"/>
        <source>Assistant</source>
        <translation>အကူ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1189"/>
        <source>Public talks</source>
        <translation>လူထုဟောပြောချက်များ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="929"/>
        <location filename="../personsui.ui" line="1182"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1162"/>
        <source>Weekend Meeting</source>
        <translation>တနင်္ဂနွေစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1246"/>
        <source>Watchtower reader</source>
        <translation>ကင်းမျှော်စင် ဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="414"/>
        <source>Prayer</source>
        <translation>ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1120"/>
        <source>Cong. Bible Study reader</source>
        <translation>အသင်းတော်ကျမ်းစာလေ့လာမှု စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1275"/>
        <source>Meeting for field ministry</source>
        <translation>လယ်ကွင်းစုဆုံမှု</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="542"/>
        <location filename="../personsui.cpp" line="551"/>
        <source>First name</source>
        <translation>ရှေ့နာမည်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="642"/>
        <location filename="../personsui.cpp" line="550"/>
        <source>Last name</source>
        <translation>နောက်နာမည်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="581"/>
        <source>Phone</source>
        <translation>ဖုန်း</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="481"/>
        <source>E-mail</source>
        <translation>အီးမေးလ်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1331"/>
        <source>Assignment</source>
        <translation>တာဝန်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1448"/>
        <source>Current Study</source>
        <translation>လက်ရှိ သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1716"/>
        <location filename="../personsui.ui" line="1739"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1212"/>
        <source>Watchtower Study Conductor</source>
        <translation>ကင်းမျှော်စင်လေ့လာမှု သင်တန်းကိုင်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="354"/>
        <source>Family Head</source>
        <translation>အိမ်ထောင်ဦးစီး</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="426"/>
        <source>Family member linked to</source>
        <translation>မိသားစုဝင်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="892"/>
        <source>Midweek Meeting</source>
        <translation>ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1066"/>
        <source>Only Auxiliary Classes</source>
        <translation>အရန်သင်တန်းသာ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1039"/>
        <source>Only Main Class</source>
        <translation>ပင်မသင်တန်းသာ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="330"/>
        <source>Active</source>
        <translation>လှုပ်ရှား</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="953"/>
        <source>Treasures From God&apos;s Word</source>
        <translation>ကျမ်းစာထဲက အဖိုးတန်ဘဏ္ဍာများ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1004"/>
        <source>Initial Call</source>
        <translation>ဦးဆုံးတွေ့ဆုံချိန်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="967"/>
        <source>Bible Reading</source>
        <translation>ကျမ်းစာဖတ်ရှုခြင်း</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1073"/>
        <source>All Classes</source>
        <translation>သင်တန်းအားလုံး</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="997"/>
        <source>Bible Study</source>
        <translation>ကျမ်းစာသင်အံမှု</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1113"/>
        <source>Congregation Bible Study</source>
        <translation>အသင်းတော်ကျမ်းစာလေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1106"/>
        <source>Living as Christians Talks</source>
        <translation>ခရစ်ယာန်အသက်တာလမ်းစဥ် ဟောပြောချက်တွေ</translation>
    </message>
    <message>
        <source>Prepare This Month&apos;s Presentations</source>
        <translation type="vanished">ဒီလရဲ့တင်ဆက်မှုအတွက် ပြင်ဆင်မယ်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="960"/>
        <source>Digging for Spiritual Gems</source>
        <translation>ရှာဖွေလေ့လာ ဘုရားရေးရာရတနာ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1011"/>
        <source>Return Visit</source>
        <translation>ပြန်လည်ပတ်မှု</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="307"/>
        <source>Personal Info</source>
        <translation>ကိုယ်ရေး အချက်အလက်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="378"/>
        <source>Host for Public Speakers</source>
        <translation>လူထုဟောပြောသူအတွက် အိမ်ရှင်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="709"/>
        <source>Mobile</source>
        <translation>မိုဘိုင်း</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="751"/>
        <source>Details</source>
        <translation>အသေးစိတ်များ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1086"/>
        <source>Talk</source>
        <translation>ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1287"/>
        <source>History</source>
        <translation>မှတ်တမ်း</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1321"/>
        <source>date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1326"/>
        <source>no</source>
        <translation>နံပါတ်</translation>
    </message>
    <message>
        <source>material</source>
        <translation type="vanished">ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1336"/>
        <source>Note</source>
        <translation>မှတ်ချက်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1341"/>
        <source>Time</source>
        <translation>အချိန်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1346"/>
        <source>Together</source>
        <comment>The column header text to show partner in student assignment</comment>
        <translation>အတူ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1355"/>
        <source>Studies</source>
        <translation>သွန်သင်ချက်တွေ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1420"/>
        <source>Study</source>
        <translation>သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1425"/>
        <source>Date assigned</source>
        <translation>တာဝန်ကျ ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1430"/>
        <source>Exercises</source>
        <translation>လေ့ကျင့်ခန်း</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1435"/>
        <source>Date completed</source>
        <translation>ပြီးတဲ့ ရက်စွဲ</translation>
    </message>
    <message>
        <source>Settings#S</source>
        <comment>for sisters assignment</comment>
        <translation type="vanished">ဇာတ်အိမ်တွေ</translation>
    </message>
    <message>
        <source>setting</source>
        <translation type="vanished">ဇာတ်အိမ်</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1615"/>
        <source>Unavailable</source>
        <translation>မအားတဲ့ နေ့</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1649"/>
        <source>Start</source>
        <translation>အစ</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="1654"/>
        <source>End</source>
        <translation>အဆုံး</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="131"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../personsui.ui" line="181"/>
        <location filename="../personsui.ui" line="1585"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="382"/>
        <source>A person with the same name already exists: &apos;%1&apos;. Do you want to change the name?</source>
        <translation>&apos;%1&apos; နာမည်တူ ရှိပြီးသား ဖြစ်နေတယ်၊ နာမည်ပြောင်းချင်လား?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="601"/>
        <source>Remove student?</source>
        <translation>ကျောင်းသား ဖယ်မလား?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="578"/>
        <source>Remove student</source>
        <translation>ကျောင်းသား ဖယ်မယ်</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="405"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove him as speaker.
Remove him as speaker?</source>
        <translation>%1 ကို ဟောပြောချက်အတွက် တာဝန်ပေးထားတယ်။ သူ့ကို ဟောပြောသူ အဖြစ်ကနေ ဖယ်မယ်ဆိုရင် ဒီဟောပြောချက်တွေကို လုပ်ရမယ့်စာရင်းထဲ ရွှေ့လိုက်မယ်။ သူ့ကို ဟောပြောသူ အဖြစ်ကနေ ဖယ်မလား?</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="592"/>
        <source>%1 is scheduled for public talks! These talks will
be moved to the To Do List if you remove the student.</source>
        <translation>%1 ကို ဟောပြောချက်အတွက် တာဝန်ပေးထားတယ်။ သူ့ကို ကျောင်းသား အဖြစ်ကနေ ဖယ်မယ်ဆိုရင် ဒီဟောပြောချက်တွေကို လုပ်ရမယ့်စာရင်းထဲ ရွှေ့လိုက်မယ်။</translation>
    </message>
    <message>
        <location filename="../personsui.cpp" line="1077"/>
        <source>Remove study</source>
        <translation>သွန်သင်ချက် ဖယ်မယ်</translation>
    </message>
</context>
<context>
    <name>printui</name>
    <message>
        <location filename="../printui.ui" line="1098"/>
        <source>Copy to the clipboard</source>
        <translation>ကူးမယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1101"/>
        <source>Copy</source>
        <translation>ကူးမယ်</translation>
    </message>
    <message>
        <source>Assignment slip for assistant</source>
        <translation type="vanished">အကူအတွက် တာဝန်ပေး စာရွက်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="235"/>
        <location filename="../printui.ui" line="326"/>
        <source>Schedule</source>
        <translation>အချိန်ဇယား</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="14"/>
        <source>Print</source>
        <translation>စာရွက်ပရင့်ထုတ်မယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="390"/>
        <source>Call List and Hospitality Schedule</source>
        <translation>ဖုန်းနံပါတ်စာရင်းနဲ့ ဧည့်ဝတ်အစီအစဥ်ဇယား</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="751"/>
        <source>Midweek Meeting Title</source>
        <translation>ကြားရက်စည်းဝေးခေါင်းစဥ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="732"/>
        <source>Weekend Meeting Title</source>
        <translation>တနင်္ဂနွေစည်းဝေးခေါင်းစဥ်</translation>
    </message>
    <message>
        <source>Show Time</source>
        <translation type="vanished">အချိန် ပြပါ</translation>
    </message>
    <message>
        <source>Show Duration</source>
        <translation type="vanished">ကြာချိန် ပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="674"/>
        <source>Show Section Titles</source>
        <translation>အပိုင်းလိုက် ခေါင်းစဥ် ပြပါ</translation>
    </message>
    <message>
        <source>Generate QR Code
and upload file to TheocBase Cloud</source>
        <translation type="vanished">QR Code ထုတ်မယ်
ပြီးတော့ TheocBase Cloud ဆီကို ဖိုင်တင်မယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="799"/>
        <source>Review/Preview/Announcements Title</source>
        <comment>See S-140</comment>
        <translation>ပြန်လည်/ကြိုသုံးသပ်ချက်/ကြေညာချက် ခေါင်းစဥ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="780"/>
        <source>Opening Comments Title</source>
        <comment>See S-140</comment>
        <translation>နိဒါန်း ခေါင်းစဥ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="258"/>
        <source>Assignment Slips for Assistants</source>
        <translation>အကူအတွက် တာဝန်စာရွက်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="361"/>
        <source>Outgoing Speakers Schedules</source>
        <translation>သွားလည်ဟောပြောသူ အစီအစဥ်ဇယား</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="377"/>
        <source>Outgoing Speakers Assignments</source>
        <translation>သွားလည်ဟောပြောသူ တာဝန်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="403"/>
        <source>Talks List</source>
        <translation>ဟောပြောချက် စာရင်း</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="535"/>
        <source>Territory Map Card</source>
        <translation type="unfinished">ရပ်ကွက်မြေပုံကတ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="545"/>
        <source>Map and Addresses Sheets</source>
        <translation>မြေပုံနဲ့ လိပ်စာ စာရွက်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="681"/>
        <source>Show duration</source>
        <translation>ကြာချိန် ပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="704"/>
        <source>Show time</source>
        <translation>အချိန် ပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="806"/>
        <source>Show Workbook Issue no.</source>
        <translation>လေ့ကျင့်ခန်းစာစောင် လထုတ်နံပါတ် ပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="813"/>
        <source>Show Watchtower Issue no.</source>
        <translation>ကင်းမျှော်စင် လထုတ်နံပါတ် ပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="957"/>
        <source>Territory number(s)</source>
        <translation>ရပ်ကွက် နံပါတ်(တွေ)</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="964"/>
        <source>Comma delimited; press Enter to refresh</source>
        <translation>ကော်မာ အကန့်အသတ် ရှိ။ စာမျက်တောင်ခတ်ဖို့ Enter နှိပ်ပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="970"/>
        <source>1</source>
        <translation>၁</translation>
    </message>
    <message>
        <source>Map and address sheets</source>
        <translation type="vanished">မြေပုံနဲ့ လိပ်စာ စာရွက်များ</translation>
    </message>
    <message>
        <source>Territory number</source>
        <translation type="vanished">ရပ်ကွက်နံပါတ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="522"/>
        <source>Territory Record</source>
        <translation>ရက်ကွက်မှတ်တမ်း</translation>
    </message>
    <message>
        <source>Territory Card</source>
        <translation type="vanished">ရပ်ကွက်ကဒ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="859"/>
        <location filename="../printui.cpp" line="2232"/>
        <source>Template</source>
        <translation>ပုံစံခွက်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="885"/>
        <source>Paper Size</source>
        <translation>စာရွက်အရွယ်အစား</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="947"/>
        <source>Print From Date</source>
        <translation>ဒီရက်စွဲကနေ စာရွက်ထုတ်မယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="905"/>
        <source>Print Thru Date</source>
        <translation>ဒီရက်စွဲအထိ စာရွက်ထုတ်မယ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="778"/>
        <location filename="../printui.cpp" line="1302"/>
        <source>Slip Template</source>
        <translation>မှတ်စု ပုံစံခွက်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1028"/>
        <source>Printing</source>
        <translation>စာရွက်ပရင့်ထုတ်နေတယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="1031"/>
        <location filename="../printui.ui" line="1066"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="248"/>
        <source>Assignment Slips</source>
        <translation>တာဝန်ခန့် စာရွက်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="767"/>
        <source>Share in Dropbox</source>
        <translation>Dropbox မှာ ဝေမျှမယ်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="278"/>
        <location filename="../printui.ui" line="345"/>
        <source>Worksheets</source>
        <translation>မှတ်တမ်း</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="565"/>
        <source>Meetings for field ministry</source>
        <translation>လယ်ကွင်းစုဆုံမှု</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="423"/>
        <source>Combination</source>
        <translation>ပါဝင်ပေါင်းစပ်မှု</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2529"/>
        <location filename="../printui.cpp" line="2988"/>
        <source>Prayer</source>
        <translation>ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1767"/>
        <location filename="../printui.cpp" line="1841"/>
        <location filename="../printui.cpp" line="2538"/>
        <source>Congregation Bible Study</source>
        <translation>အသင်းတော်ကျမ်းစာလေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1842"/>
        <source>Theocratic Ministry School</source>
        <translation>သီအိုကရက်တစ်ဓမ္မအမှုကျောင်း</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1843"/>
        <source>Service Meeting</source>
        <translation>လုပ်ငန်းတော်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="632"/>
        <source>Additional Options</source>
        <translation>နောက်ထပ် ရွေးချယ်စရာ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="667"/>
        <source>Show Song Titles</source>
        <translation>သီချင်းခေါင်းစဥ် ပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="660"/>
        <source>Show Counsel Text</source>
        <translation>သွန်သင်ချက် စာသား ပြပါ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="730"/>
        <location filename="../printui.cpp" line="1678"/>
        <location filename="../printui.cpp" line="1765"/>
        <location filename="../printui.cpp" line="1839"/>
        <location filename="../printui.cpp" line="2980"/>
        <source>Public Talk</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="474"/>
        <source>Outgoing Speakers Schedule</source>
        <translation>သွားလည် ဟောပြောသူ အစီအစဥ်</translation>
    </message>
    <message>
        <source>Outgoing Speaker Assignments</source>
        <translation type="vanished">သွားလည် ဟောပြောသူ တာဝန်</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="268"/>
        <source>Print assigned only</source>
        <translation>တာဝန်ပေးထားတာကိုပဲ စာရွက်ထုတ်ပါ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="385"/>
        <source>Copied to the clipboard. Paste to word processing program (Ctrl+V/Cmd+V)</source>
        <translation>ကူးပြီးပြီ၊ တစ်နေရာရာမှာ ကူးထည့်ပါ (Ctrl+V/Cmd+V)</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="431"/>
        <location filename="../printui.cpp" line="457"/>
        <location filename="../printui.cpp" line="546"/>
        <source>file created</source>
        <translation>ဖိုင် ဖန်တီးပြီးပြီ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2550"/>
        <location filename="../printui.cpp" line="2551"/>
        <location filename="../printui.cpp" line="2552"/>
        <location filename="../printui.cpp" line="2845"/>
        <source>Class</source>
        <translation>သင်တန်း</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="396"/>
        <location filename="../printui.cpp" line="442"/>
        <location filename="../printui.cpp" line="473"/>
        <location filename="../printui.cpp" line="526"/>
        <source>Save file</source>
        <translation>ဖိုင် သိမ်းမယ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2545"/>
        <source>Exercises</source>
        <translation>လေ့ကျင့်ခန်းများ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="494"/>
        <source>Territories</source>
        <translation>ရပ်ကွက်</translation>
    </message>
    <message>
        <source>No meeting</source>
        <translation type="vanished">အစည်းအဝေး မရှိပါ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1769"/>
        <location filename="../printui.cpp" line="1784"/>
        <location filename="../printui.cpp" line="3175"/>
        <location filename="../printui.cpp" line="3203"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1700"/>
        <location filename="../printui.cpp" line="3174"/>
        <source>Outgoing Speakers</source>
        <translation>သွားလည် ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3178"/>
        <source>Theme Number</source>
        <translation>အဓိကအကြောင်းအရာ နံပါတ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="728"/>
        <location filename="../printui.cpp" line="1670"/>
        <location filename="../printui.cpp" line="1759"/>
        <location filename="../printui.cpp" line="1837"/>
        <location filename="../printui.cpp" line="3031"/>
        <location filename="../printui.cpp" line="3179"/>
        <location filename="../printui.cpp" line="3197"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1674"/>
        <location filename="../printui.cpp" line="1764"/>
        <location filename="../printui.cpp" line="1781"/>
        <location filename="../printui.cpp" line="2533"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1672"/>
        <location filename="../printui.cpp" line="1762"/>
        <location filename="../printui.cpp" line="1779"/>
        <location filename="../printui.cpp" line="2530"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="731"/>
        <location filename="../printui.cpp" line="1679"/>
        <location filename="../printui.cpp" line="1766"/>
        <location filename="../printui.cpp" line="1840"/>
        <location filename="../printui.cpp" line="2982"/>
        <source>Watchtower Study</source>
        <translation>ကင်းမျှော်စင်လေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="700"/>
        <source>Class </source>
        <translation>သင်တန်း </translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="732"/>
        <location filename="../printui.cpp" line="2535"/>
        <source>Treasures from God&apos;s Word</source>
        <translation>ကျမ်းစာထဲက အဖိုးတန်ဘဏ္ဍာများ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="733"/>
        <location filename="../printui.cpp" line="2536"/>
        <source>Apply Yourself to the Field Ministry</source>
        <translation>အမှုဆောင်လုပ်ငန်းမှာ ကျင့်သုံးပါ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="734"/>
        <location filename="../printui.cpp" line="2537"/>
        <source>Living as Christians</source>
        <translation>ခရစ်ယာန်အသက်တာလမ်းစဥ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1174"/>
        <source>Click the print button to preview assignment slips</source>
        <translation>တာဝန်ပေး မှတ်စုစာရွက်ကို ကြည့်ဖို့ စာရွက်ထုတ်တဲ့ ခလုတ်ကို နှိပ်ပါ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1174"/>
        <source>Print button is found at the bottom right corner</source>
        <translation>ပုံနှိပ်ထုတ်ဖို့ ခလုတ်ကို ညာအောက်ထောင့်မှာ တွေ့ရ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1905"/>
        <source>Territory Assignment Record</source>
        <translation>ရပ်ကွက် ပေးအပ် မှတ်တမ်း</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1906"/>
        <source>Territory Coverage</source>
        <translation>ရပ်ကွက်ခြုံငုံမှု</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3194"/>
        <source>Addresses</source>
        <comment>Addresses included in the territory</comment>
        <translation>လိပ်စာများ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3204"/>
        <source>City</source>
        <translation type="unfinished">မြို့</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3208"/>
        <source>From</source>
        <comment>From number; in number range</comment>
        <translation type="unfinished">ကနေ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3220"/>
        <source>Map</source>
        <comment>Map of a territory</comment>
        <translation>မြေပုံ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3227"/>
        <source>Territory</source>
        <translation>ရပ်ကွက်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3228"/>
        <source>Terr. No.</source>
        <translation>ရပ်ကွက် နံပါတ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1907"/>
        <source>Total number of territories</source>
        <translation>ရပ်ကွက်အရေအတွက်စုစုပေါင်း</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3219"/>
        <source>Locality</source>
        <translation>ဒေသ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3229"/>
        <source>Territory type</source>
        <translation>ရပ်ကွက် အမျိုးအစား</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3223"/>
        <source>Name of publisher</source>
        <translation>ကြေညာသူ နာမည်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3200"/>
        <source>Date checked out</source>
        <translation>ရပ်ကွက်ထုတ်ယူတဲ့ ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3201"/>
        <source>Date checked back in</source>
        <translation>ရပ်ကွက်ပြန်အပ်တဲ့ ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3202"/>
        <source>Date last worked</source>
        <translation>နောက်ဆုံး လုပ်ခဲ့တဲ့ ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3196"/>
        <source>Assigned to</source>
        <translation>တာဝန်ပေးထားတဲ့သူ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3224"/>
        <source>Remark</source>
        <translation>မှတ်ချက်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1908"/>
        <source>&lt; 6 months</source>
        <comment>territory worked</comment>
        <translation>၆ လအောက်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1909"/>
        <source>6 to 12 months</source>
        <comment>territory worked</comment>
        <translation>၆ လ ကနေ ၁၂ လ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1910"/>
        <source>&gt; 12 months ago</source>
        <comment>territory worked</comment>
        <translation>လွန်ခဲ့တဲ့ ၁၂လ အထက်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1911"/>
        <source>Average per year</source>
        <comment>Number of times territory has been worked per year on average</comment>
        <translation>တစ်နှစ်အတွင်း ပျမ်းမျှပြီးကြိမ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3193"/>
        <source>Address</source>
        <translation>လိပ်စာ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3205"/>
        <source>Country</source>
        <comment>Short name of country</comment>
        <translation>နိုင်ငံ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3225"/>
        <source>State</source>
        <comment>Short name of administrative area level 1</comment>
        <translation>ပြည်နယ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3206"/>
        <source>County</source>
        <comment>Name of administrative area level 2</comment>
        <translation>တိုင်း/ပြည်နယ်</translation>
    </message>
    <message>
        <source>DistrictSublocality</source>
        <translation type="vanished">ခရိုင်</translation>
    </message>
    <message>
        <source>City</source>
        <comment>Locality</comment>
        <translation type="vanished">မြို့</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3207"/>
        <source>District</source>
        <comment>Sublocality, first-order civil entity below a locality</comment>
        <translation>ခရိုင်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3226"/>
        <source>Street</source>
        <comment>Streetname</comment>
        <translation>လမ်း</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3218"/>
        <source>No.</source>
        <comment>House or street number</comment>
        <translation>နံပါတ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3222"/>
        <source>Postalcode</source>
        <comment>Mail code, ZIP</comment>
        <translation>စာပို့သင်္ကေတ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3221"/>
        <source>Name</source>
        <comment>Name of person or building</comment>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3195"/>
        <source>Address type</source>
        <translation>လိပ်စာ အမျိုးအစား</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2157"/>
        <source>This Territory Map Card could not be converted to a JPG file</source>
        <translation>ဒီရပ်ကွက်မြေပုံကဒ်ကို JPG ဖိုင်အဖြစ် ပြောင်းလို့မရဘူး</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1068"/>
        <location filename="../printui.cpp" line="1505"/>
        <source>Assistant</source>
        <translation type="unfinished">အကူ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1081"/>
        <source>Study %1</source>
        <comment>Text for study point on slip</comment>
        <translation>သွန်သင်ချက် %1</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1770"/>
        <source>Phone</source>
        <comment>Phone number title</comment>
        <translation>ဖုန်း</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1771"/>
        <source>Host</source>
        <comment>Host for incoming public speaker</comment>
        <translation>အိမ်ရှင်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1778"/>
        <location filename="../printui.cpp" line="3176"/>
        <source>Speaker</source>
        <translation type="unfinished">ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2054"/>
        <source>Territory Map Card</source>
        <comment>Title tag for a S-12 or similar card</comment>
        <translation>ရပ်ကွက်မြေပုံကတ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2055"/>
        <source>Territory Map</source>
        <comment>Title tag for a sheet with a territory map</comment>
        <translation>မြေပုံ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2056"/>
        <source>Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s address list</comment>
        <translation>လိပ်စာ စာရင်း</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2057"/>
        <source>Territory Map with Address List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and address list</comment>
        <translation>လိပ်စာ စာရင်းနဲ့ ရပ်ကွက် မြေပုံ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2058"/>
        <source>Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s street list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2059"/>
        <source>Territory Map with Street List</source>
        <comment>Title tag for a sheet with a territory&apos;s map and street list</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2060"/>
        <source>Do-Not-Call List</source>
        <comment>Title tag for a sheet with a territory&apos;s Do-Not-Call list</comment>
        <translation>မဟောရမယ့် အိမ်စာရင်း</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2526"/>
        <source>Opening Comments</source>
        <comment>See Workbook</comment>
        <translation type="unfinished">နိဒါန်း</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2527"/>
        <source>Review Followed by Preview of Next Week</source>
        <comment>See Workbook</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2540"/>
        <location filename="../printui.cpp" line="3069"/>
        <source>No regular meeting</source>
        <translation>ပုံမှန်အစည်းအဝေး မရှိ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2542"/>
        <source>Study</source>
        <comment>Counsel point</comment>
        <translation type="unfinished">သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2543"/>
        <source>Theme</source>
        <comment>Talk Theme description from workbook</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2544"/>
        <source>Source</source>
        <comment>Source information from workbook</comment>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2546"/>
        <source>Timing</source>
        <comment>Assignment completed in time?</comment>
        <translation type="unfinished">အချိန်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2553"/>
        <source>Auxiliary Classroom Counselor</source>
        <comment>See S-140</comment>
        <translation>အရန်သွန်သင်သူ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2554"/>
        <source>Auxiliary Classroom</source>
        <comment>See S-140</comment>
        <translation>အရန်သင်တန်း</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2555"/>
        <source>Main Hall</source>
        <comment>See S-140</comment>
        <translation>ပင်မ ခန်းမ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2556"/>
        <source>MH</source>
        <comment>Abbreviation for &apos;Main Hall&apos;</comment>
        <translation>ပင်မ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2557"/>
        <source>A</source>
        <comment>Abbreviation for &apos;Auxiliary Classroom&apos;</comment>
        <translation>အရန်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2558"/>
        <source>Student</source>
        <comment>See S-140</comment>
        <translation>ကျောင်းသား</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2559"/>
        <source>Review/Preview/Announcements</source>
        <comment>See S-140</comment>
        <translation type="unfinished">ပြန်လည်/ကြိုသုံးသပ်ချက်/ကြေညာချက်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2560"/>
        <source>Opening Comments</source>
        <comment>Customizable title for OC notes</comment>
        <translation type="unfinished">နိဒါန်း</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2561"/>
        <source>Review/Preview/Announcements</source>
        <comment>Customizable title for RPA notes</comment>
        <translation>ပြန်လည်/ကြိုသုံးသပ်ချက်/ကြေညာချက်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2562"/>
        <source>Today</source>
        <translation>ဒီနေ့</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2563"/>
        <source>Next week</source>
        <translation>နောက်အပတ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3182"/>
        <source>NOTE: Dear brother, in spite of careful database-maintenance, sometimes times or addresses might be out of date. So, please verify by looking those up via JW.ORG. Thank you!</source>
        <translation>ညီကို၊ အချက်အလက်တွေကို သေချာ ထိန်းသိမ်းပေမဲ့လည်း တစ်ခါတလေ အချိန်တွေနဲ့ လိပ်စာတွေက မမှန်တော့တာမျိုး ဖြစ်နိုင်ပါတယ်။ ဆိုတော့၊ ကျေးဇူးပြုပြီး JW.ORG မှာ ပြန်တိုက်ကြည့်ပေးပါ။ ကျေးဇူးတင်ပါတယ်။</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1677"/>
        <location filename="../printui.cpp" line="1755"/>
        <source>Public Meeting</source>
        <translation>လူထုစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="368"/>
        <source>Custom...</source>
        <comment>pick custom paper size</comment>
        <translation>စိတ်ကြိုက်...</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="727"/>
        <location filename="../printui.cpp" line="1669"/>
        <location filename="../printui.cpp" line="1758"/>
        <location filename="../printui.cpp" line="1836"/>
        <location filename="../printui.cpp" line="3030"/>
        <location filename="../printui.cpp" line="3199"/>
        <source>%1 Congregation</source>
        <comment>Congregation_Title Tag. %1 is Congregation Name</comment>
        <translation>%1 အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1254"/>
        <source>Converting %1 to JPG file</source>
        <translation>%1 ကို JPG ဖိုင်အဖြစ် ပြောင်းနေတယ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1659"/>
        <source>Select at least one option</source>
        <translation>အနည်းဆုံး တစ်ခုတော့ ရွေးပါ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="207"/>
        <location filename="../printui.ui" line="461"/>
        <location filename="../printui.cpp" line="1680"/>
        <location filename="../printui.cpp" line="2524"/>
        <location filename="../printui.cpp" line="3488"/>
        <source>Midweek Meeting</source>
        <translation>ကြားရက်စည်းဝေး</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1681"/>
        <location filename="../printui.cpp" line="1682"/>
        <location filename="../printui.cpp" line="1768"/>
        <location filename="../printui.cpp" line="2534"/>
        <source>Christian Life and Ministry Meeting</source>
        <translation>ခရစ်ယာန်အသက်တာနဲ့ အမှုဆောင်လုပ်ငန်းစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1697"/>
        <location filename="../printui.cpp" line="1698"/>
        <source>Combined Schedule</source>
        <translation>ပေါင်းထားတဲ့ အစီအစဥ်ဇယား</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1782"/>
        <source>WT Reader</source>
        <translation>ကင်:မျှော်စင် စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1785"/>
        <location filename="../printui.cpp" line="3177"/>
        <source>Theme</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2525"/>
        <source>Worksheet</source>
        <translation>မှတ်တမ်း</translation>
    </message>
    <message>
        <source>Review Followed by Preview of Next Week</source>
        <translation type="vanished">ပြန်လည်သုံးသပ်ခြင်းနဲ့ နောက်တစ်ပတ်အတွက် အကြိုသုံးသပ်ခြင်း</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2539"/>
        <location filename="../printui.cpp" line="2983"/>
        <source>Circuit Overseer</source>
        <translation>တိုက်နယ်ကြီးကြပ်မှုး</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2541"/>
        <source>Assistant</source>
        <comment>Assistant to student</comment>
        <translation>အကူ</translation>
    </message>
    <message>
        <source>Study</source>
        <comment>title for study point</comment>
        <translation type="vanished">သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2547"/>
        <source>Notes</source>
        <translation>မှတ်စုတိုများ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3230"/>
        <source>To</source>
        <comment>To number; in number range</comment>
        <translation type="unfinished">အထိ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3231"/>
        <source>Type</source>
        <comment>Type of something</comment>
        <translation type="unfinished">အမျိုးအစား</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3232"/>
        <source>Sum</source>
        <comment>Total amount</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3244"/>
        <source>Service Talk</source>
        <translation>လုပ်ငန်းတော် ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3253"/>
        <source>Begins at</source>
        <comment>Used in print template, example &apos;Begins at 11:00&apos;</comment>
        <translation>မှာ စတယ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3497"/>
        <source>Closing Comments</source>
        <translation>နိဂုံး မှတ်ချက်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3575"/>
        <source>Can&apos;t read file</source>
        <comment>cannot read printing template</comment>
        <translation>ဖိုင် ဖတ်လို့မရပါ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3836"/>
        <source>New Custom Paper Size</source>
        <comment>title of dialog box</comment>
        <translation>စိတ်ကြိုက် စာရွက်အရွယ်အစား အသစ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3836"/>
        <source>Format: width x height. Width and Height can be in or mm. Example 210mm x 297mm</source>
        <translation>Format: width x height. Width and Height can be in or mm. Example 210mm x 297mm</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2549"/>
        <source>Setting</source>
        <comment>for sisters assignment</comment>
        <translation>ဇာတ်အိမ်</translation>
    </message>
    <message>
        <source>Print button is found at the bottom left corner</source>
        <translation type="vanished">ဘယ်ဘက်အောက်ထောင့်မှာ စာရွက်ထုတ်ဖို့ ခလုတ်ရှိတယ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1242"/>
        <source>Unable to find information in this template: </source>
        <translation>ဒီပုံစံခွက်မှာ အချက်အလက် မတွေ့ပါ၊ </translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1108"/>
        <location filename="../printui.cpp" line="1525"/>
        <source>2nd talk</source>
        <comment>When printing slips: if the first talk is not &apos;Return Visit&apos;</comment>
        <translation>ပြန်လည်ပတ်မှု</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1620"/>
        <source>Slips created at %1*.pdf</source>
        <translation>%1*.pdf မှာ မှတ်တမ်းတွေ လုပ်ထားပြီ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1673"/>
        <location filename="../printui.cpp" line="2531"/>
        <source>Counselor</source>
        <translation>အကြံပေးသူ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1676"/>
        <location filename="../printui.cpp" line="1763"/>
        <source>Speaker</source>
        <comment>Public talk speaker</comment>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../printui.ui" line="298"/>
        <location filename="../printui.ui" line="448"/>
        <location filename="../printui.cpp" line="574"/>
        <location filename="../printui.cpp" line="1699"/>
        <location filename="../printui.cpp" line="1754"/>
        <location filename="../printui.cpp" line="3489"/>
        <source>Weekend Meeting</source>
        <translation>တနင်္ဂနွေစည်းဝေး</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1711"/>
        <source>Week Starting %1</source>
        <comment>%1 is Monday of the week</comment>
        <translation>%1 စမယ့်အပတ်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3496"/>
        <source>Opening Comments</source>
        <translation>နိဒါန်း</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2528"/>
        <location filename="../printui.cpp" line="2991"/>
        <source>Song</source>
        <translation>သီချင်း</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3066"/>
        <source>Watchtower Conductor</source>
        <translation>ကင်းမျှော်စင်လေ့လာမှု သင်တန်းကိုင်</translation>
    </message>
    <message>
        <source>No Meeting</source>
        <translation type="vanished">အစည်းအဝေး မရှိပါ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3173"/>
        <source>Talk</source>
        <translation>ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="1675"/>
        <location filename="../printui.cpp" line="1761"/>
        <location filename="../printui.cpp" line="2532"/>
        <source>Conductor</source>
        <translation>သင်တန်းကိုင်သူ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3570"/>
        <source>Template not found</source>
        <comment>printing template not found</comment>
        <translation>ပုံစံခွက် မတွေ့ပါ</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2842"/>
        <source>Other schools</source>
        <translation>တခြား ကျောင်းများ</translation>
    </message>
    <message>
        <source>Timing</source>
        <translation type="vanished">အချိန်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="2548"/>
        <source>Next study</source>
        <translation>နောက်သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3180"/>
        <source>Start Time</source>
        <translation>စတင်ချိန်</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3843"/>
        <source>Invalid entry, sorry.</source>
        <translation type="unfinished">အချက်အလက် ထည့်လို့မရပါ၊ တောင်းပန်ပါတယ်။</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3896"/>
        <source>Width unit does not match height unit</source>
        <comment>while asking for custom paper size</comment>
        <translation>အကျယ်ယူနစ် က အမြင့်ယူနစ် နဲ့ မကိုက်ဘူး</translation>
    </message>
    <message>
        <location filename="../printui.cpp" line="3924"/>
        <source>Invalid entry, sorry.</source>
        <comment>while asking for custom paper size</comment>
        <translation>အချက်အလက် ထည့်လို့မရပါ၊ တောင်းပန်ပါတယ်။</translation>
    </message>
    <message>
        <source>buttonGroup_2</source>
        <translation type="vanished">buttonGroup_2</translation>
    </message>
    <message>
        <source>buttonGroup</source>
        <translation type="vanished">buttonGroup</translation>
    </message>
</context>
<context>
    <name>publicmeeting_controller</name>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="202"/>
        <source>From %1</source>
        <translation>%1 ကနေ</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="230"/>
        <source>The destination date already has a talk scheduled.</source>
        <translation>ဒီရက်စွဲမှာ ဟောပြောချက် အစီအစဥ် ရှိပြီးသား ဖြစ်နေတယ်။</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="235"/>
        <source>Swap Talks</source>
        <comment>Button text</comment>
        <translation>ဟောပြောချက်တွေကို ဖလှယ်လိုက်</translation>
    </message>
    <message>
        <location filename="../publicmeeting_controller.cpp" line="236"/>
        <source>Cancel</source>
        <comment>Button text</comment>
        <translation>ဖျက်သိမ်းပါ</translation>
    </message>
</context>
<context>
    <name>publictalkedit</name>
    <message>
        <location filename="../publictalkedit.ui" line="67"/>
        <source>Public Talk</source>
        <translation>လူထုဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="257"/>
        <location filename="../publictalkedit.ui" line="592"/>
        <location filename="../publictalkedit.ui" line="681"/>
        <location filename="../publictalkedit.ui" line="881"/>
        <location filename="../publictalkedit.cpp" line="47"/>
        <source>Theme</source>
        <translation>အဓိကအကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="169"/>
        <location filename="../publictalkedit.ui" line="866"/>
        <location filename="../publictalkedit.cpp" line="48"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="144"/>
        <location filename="../publictalkedit.ui" line="328"/>
        <location filename="../publictalkedit.cpp" line="50"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="232"/>
        <location filename="../publictalkedit.ui" line="617"/>
        <location filename="../publictalkedit.ui" line="642"/>
        <source>Song</source>
        <translation>သီချင်း</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="411"/>
        <source>Watchtower Study</source>
        <translation>ကင်းမျှော်စင်လေ့လာမှု</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="472"/>
        <location filename="../publictalkedit.cpp" line="52"/>
        <source>Reader</source>
        <translation>စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="542"/>
        <source>Source</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="285"/>
        <location filename="../publictalkedit.ui" line="932"/>
        <source>Move to different week</source>
        <translation>တခြားအပတ်ကို ရွေ့ပါ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="118"/>
        <location filename="../publictalkedit.ui" line="906"/>
        <source>Send to To Do List</source>
        <translation>လုပ်စရာ စာရင်း ထဲကို ပို့မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="311"/>
        <source>Clear Public Talk selections</source>
        <comment>Clear=Delete</comment>
        <translation>လူထုဟောပြောချက် ရွေးချယ်မှု ဖျက်မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="567"/>
        <location filename="../publictalkedit.ui" line="658"/>
        <location filename="../publictalkedit.cpp" line="51"/>
        <source>Conductor</source>
        <translation>သင်တန်းကိုင်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="497"/>
        <location filename="../publictalkedit.ui" line="671"/>
        <source>Service Talk</source>
        <translation>လုပ်ငန်းတော်ဟောပြောချက်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="759"/>
        <source>Outgoing Speakers This Weekend</source>
        <translation>သွားလည် ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="833"/>
        <source>Add to Outgoing List</source>
        <translation>သွားလည် ဟောပြောသူ စာရင်းထဲ ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="876"/>
        <source>Start time</source>
        <translation>စတင်ချိန်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="886"/>
        <source>Theme No.</source>
        <translation>အဓိကအကြောင်းအရာ နံပါတ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="979"/>
        <source>To Do List</source>
        <translation>လုပ်စရာ စာရင်း</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1019"/>
        <source>Add item to schedule</source>
        <translation>အစီအစဥ်ဇယားထဲကို တစ်ခုထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1100"/>
        <source>Add Outgoing To Do item</source>
        <extracomment>Add OUT item</extracomment>
        <translation type="unfinished">ထွက်သွားမယ့် လုပ်စရာ ထည့်မယ်</translation>
    </message>
    <message>
        <source>Add Outgoing To Do item</source>
        <comment>Add OUT item</comment>
        <translation type="vanished">ထွက်သွားမယ့် လုပ်စရာ ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="793"/>
        <location filename="../publictalkedit.ui" line="1045"/>
        <source>Remove item</source>
        <translation>ဒီတစ်ခု ဖယ်မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="1071"/>
        <source>Add Incoming To Do item</source>
        <translation>ဝင်လာမယ့် လုပ်စရာ ထည့်မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.ui" line="207"/>
        <location filename="../publictalkedit.ui" line="871"/>
        <location filename="../publictalkedit.cpp" line="49"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="89"/>
        <source>Watchtower Study Edition</source>
        <translation>ကင်းမျှော်စင် လေ့လာရန်ဆောင်းပါး</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="255"/>
        <location filename="../publictalkedit.cpp" line="290"/>
        <source>From %1; speaker removed</source>
        <comment>From [scheduled date]; speaker removed</comment>
        <translation>%1 ကစပြီး ဟောပြောသူကို ဖယ်ခဲ့</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="293"/>
        <source>From %1; speaker moved to %2</source>
        <comment>From [scheduled date]; speaker moved to [new congregation]</comment>
        <translation>%1 ကစပြီး ဟောပြောသူက %2 ကို ပြောင်းသွားတယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="339"/>
        <location filename="../publictalkedit.cpp" line="374"/>
        <source>From %1; talk discontinued</source>
        <comment>From [scheduled date]; talk discontinued</comment>
        <translation>%1 နေ့က ဟောပြောချက် ဆက်မထားတော့</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="694"/>
        <source>In</source>
        <comment>Incoming</comment>
        <translation>အဝင်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="694"/>
        <source>Out</source>
        <comment>Outgoing</comment>
        <translation>အထွက်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="816"/>
        <source>The selected speaker has already public talk on this calendar month. Do you want to add?</source>
        <translation>ရွေးချယ်ထားတဲ့ ဟောပြောသူက ဒီလမှာ ဟောပြောချက်တာဝန် ရှိပြီးပြီ။ ထည့်ချင်လား?</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="944"/>
        <location filename="../publictalkedit.cpp" line="995"/>
        <location filename="../publictalkedit.cpp" line="1035"/>
        <source>The destination date already has a talk scheduled. What to do?</source>
        <translation>ဒီရက်စွဲမှာ ဟောပြောချက် အစီအစဥ် ရှိပြီးသား ဖြစ်နေတယ်။ ဘာလုပ်မလဲ?</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="945"/>
        <location filename="../publictalkedit.cpp" line="996"/>
        <source>&amp;Swap Talks</source>
        <translation>&amp;ဟောပြောချက် လဲမယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="946"/>
        <location filename="../publictalkedit.cpp" line="997"/>
        <location filename="../publictalkedit.cpp" line="1036"/>
        <source>&amp;Move other talk to To Do List</source>
        <translation>&amp;တခြား ဟောပြောချက်ကို လုပ်စရာ စာရင်းထဲကို ရွှေ့မယ်</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="947"/>
        <location filename="../publictalkedit.cpp" line="998"/>
        <location filename="../publictalkedit.cpp" line="1037"/>
        <source>&amp;Cancel</source>
        <translation>&amp;ပယ်ဖျက်ပါ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="964"/>
        <location filename="../publictalkedit.cpp" line="1010"/>
        <location filename="../publictalkedit.cpp" line="1046"/>
        <location filename="../publictalkedit.cpp" line="1055"/>
        <location filename="../publictalkedit.cpp" line="1077"/>
        <location filename="../publictalkedit.cpp" line="1110"/>
        <source>From %1</source>
        <translation>%1 ကနေ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1033"/>
        <source>Date Already Scheduled</source>
        <translation>ရက်စွဲ အစီအစဥ် ဆွဲပြီးသား</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1064"/>
        <source>Error</source>
        <translation>ပြဿနာ</translation>
    </message>
    <message>
        <location filename="../publictalkedit.cpp" line="1064"/>
        <source>Cannot schedule this item until these fields are fixed: %1</source>
        <translation>ဒီအချက်ကို အစီစဥ်ဆွဲလို့ မရဘူး၊ ဒီအကွက်တွေကို မပြင်ဘူးဆိုရင် %1</translation>
    </message>
</context>
<context>
    <name>reminders</name>
    <message>
        <location filename="../reminders.ui" line="14"/>
        <source>Reminders</source>
        <translation>သတိပေးချက်</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="58"/>
        <source>Date range</source>
        <translation>ရက်စွဲ အကြာအရှည်</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="159"/>
        <source>Send selected reminders</source>
        <translation>ရွေးထားတဲ့ အသိပေးချက်တွေကို ပို့မယ်</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="177"/>
        <source>From</source>
        <translation>ကနေ</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="195"/>
        <source>To</source>
        <translation>အထိ</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="247"/>
        <source>Details</source>
        <translation>အချက်အလက်</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="298"/>
        <source>Date</source>
        <translation>ရက်စွဲ</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="303"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="313"/>
        <source>Subject</source>
        <translation>အကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="318"/>
        <source>Message</source>
        <translation>သတင်း</translation>
    </message>
    <message>
        <location filename="../reminders.ui" line="308"/>
        <source>E-Mail</source>
        <translation>အီးမေးလ်</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="95"/>
        <source>Email sending...</source>
        <translation>အီးမေးလ် ပို့နေတယ်...</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="96"/>
        <source>Cancel</source>
        <translation>ဖျက်သိမ်းပါ</translation>
    </message>
    <message>
        <location filename="../reminders.cpp" line="113"/>
        <source>Error sending e-mail</source>
        <translation>အီးမေးလ် ပို့လို့ မရပါ</translation>
    </message>
    <message>
        <source>Send</source>
        <comment>Email reminder</comment>
        <translation type="vanished">ပို့မယ်</translation>
    </message>
    <message>
        <source>Resend</source>
        <comment>Email reminder</comment>
        <translation type="vanished">ပြန်ပို့မယ်</translation>
    </message>
</context>
<context>
    <name>schoolreminder</name>
    <message>
        <location filename="../schoolreminder.cpp" line="57"/>
        <source>Chairman</source>
        <translation>သဘာပတိ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="60"/>
        <source>Counselor-Class II</source>
        <translation>သွန်သင်သူ အတန်း II</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="63"/>
        <source>Counselor-Class III</source>
        <translation>သွန်သင်သူ အတန်း III</translation>
    </message>
    <message>
        <source>Opening Prayer</source>
        <translation type="vanished">အဖွင့် ဆုတောင်းချက်</translation>
    </message>
    <message>
        <source>Concluding Prayer</source>
        <translation type="vanished">အပိတ် ဆုတောင်းချက်</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="66"/>
        <source>Prayer I</source>
        <translation>ဆုတောင်းချက် ၁</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="69"/>
        <source>Prayer II</source>
        <translation>ဆုတောင်းချက် ၂</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="161"/>
        <source>Cancellation - Our Christian Life and Ministry Meeting Assignment</source>
        <translation>ပယ်ဖျက်ခြင်း - ခရစ်ယာန်အသက်တာနဲ့ အမှုဆောင်လုပ်ငန်း စည်းဝေး တာဝန်</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="161"/>
        <source>Our Christian Life and Ministry Meeting Assignment</source>
        <translation>ခရစ်ယာန်အသက်တာနဲ့ အမှုဆောင်လုပ်ငန်းစည်းဝေးတာဝန်</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="181"/>
        <source>Name</source>
        <translation>နာမည်</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="183"/>
        <source>Assignment</source>
        <translation>တာဝန်</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="186"/>
        <source>Theme</source>
        <translation>အဓိကအကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="188"/>
        <source>Source Material</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="202"/>
        <source>Assistant</source>
        <translation>အကူ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="212"/>
        <location filename="../schoolreminder.cpp" line="214"/>
        <source>Study</source>
        <translation>သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="219"/>
        <source>Reader for Congregation Bible Study</source>
        <translation>အသင်းတော် ကျမ်းစာလေ့လာမှု အတွက် စာဖတ်သူ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="221"/>
        <source>Assistant to %1</source>
        <comment>%1 is student&apos;s name</comment>
        <translation>%1 အတွက် အကူ</translation>
    </message>
    <message>
        <location filename="../schoolreminder.cpp" line="226"/>
        <source>Cancellation</source>
        <translation>ပြန်ရုပ်သိမ်းခြင်း</translation>
    </message>
</context>
<context>
    <name>schoolresult</name>
    <message>
        <location filename="../schoolresult.ui" line="23"/>
        <source>Volunteer</source>
        <translation>စေတနာ့ဝန်ထမ်း</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="33"/>
        <source>Study</source>
        <translation>သွန်သင်ချက်</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="45"/>
        <source>Exercise Completed</source>
        <translation>ပြီးသွားတဲ့ လေ့ကျင့်ခန်း</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="52"/>
        <source>Next study:</source>
        <translation>နောက်သွန်သင်ချက်:</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="69"/>
        <source>Setting:</source>
        <comment>for sisters assignment</comment>
        <translation>ဇာတ်အိမ်:</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="96"/>
        <source>OK</source>
        <translation>အိုကေ</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="130"/>
        <source>Cancel</source>
        <translation>ဖျက်သိမ်းပါ</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="137"/>
        <source>Completed</source>
        <translation>ပြီးသွားပြီ</translation>
    </message>
    <message>
        <location filename="../schoolresult.ui" line="151"/>
        <source>Timing:</source>
        <translation>အချိန်:</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="59"/>
        <source>Bible highlights</source>
        <translation>ကျမ်းစာ ပေါ်လွင်ချက်</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="61"/>
        <source>reading</source>
        <translation>ဖတ်ရှုခြင်း</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="65"/>
        <source>Assignment result</source>
        <translation>တာဝန် ရလဒ်</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="130"/>
        <source>Do not assign the next study</source>
        <translation>နောက်သွန်သင်ချက်အတွက် တာဝန် မပေးပါနဲ့</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="159"/>
        <location filename="../schoolresult.cpp" line="369"/>
        <source>Leave on current study</source>
        <translation>လက်ရှိ သွန်သင်ချက်အတိုင်း ဆက်ထားပါ</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="239"/>
        <source>Select setting</source>
        <translation>ဇာတ်အိမ် ရွေးချယ်ပါ</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="246"/>
        <source>Timing</source>
        <translation>အချိန်</translation>
    </message>
    <message>
        <location filename="../schoolresult.cpp" line="247"/>
        <source>The timing is empty. Save?</source>
        <translation>အချိန် သတ်မှတ်ထားတာ မရှိဘူး။ သိမ်းထားမလား?</translation>
    </message>
</context>
<context>
    <name>schoolui</name>
    <message>
        <location filename="../schoolui.cpp" line="35"/>
        <source>Theme</source>
        <translation>အဓိကအကြောင်းအရာ</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="35"/>
        <location filename="../schoolui.cpp" line="36"/>
        <source>Source</source>
        <translation>ရင်းမြစ်</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="41"/>
        <source>Class</source>
        <translation>သင်တန်း</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="111"/>
        <source>Bible highlights:</source>
        <translation>ကျမ်းစာ ပေါ်လွင်ချက်:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="115"/>
        <source>No. 1:</source>
        <translation>နံပါတ် ၁:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="119"/>
        <source>No. 2:</source>
        <translation>နံပါတ် ၂:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="123"/>
        <source>No. 3:</source>
        <translation>နံပါတ် ၃:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="127"/>
        <source>Reader:</source>
        <translation>စာဖတ်သူ:</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="176"/>
        <source>No school</source>
        <translation>ကျောင်းမရှိ</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="329"/>
        <source>Nothing to display</source>
        <translation>ဘာမှ ပြစရာ မရှိပါ</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="495"/>
        <source>No assignment has been made!</source>
        <translation>တာဝန်မပေးရသေးပါ!</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="500"/>
        <source>Please import new school schedule from Watchtower Library (Settings-&gt;Theocratic Ministry School...)</source>
        <translation>ကင်းမျှော်စင် စာကြည့်တိုက်ကနေ ကျောင်းအချိန်ဇယား အသစ် တင်သွင်းပါ (ချိန်ညှိစရာ-&gt;သီအိုကရက်တစ် ဓမ္မအမှုကျောင်း...)</translation>
    </message>
    <message>
        <location filename="../schoolui.cpp" line="542"/>
        <source>Show Details...</source>
        <translation>အသေးစိတ် ပြပါ...</translation>
    </message>
</context>
<context>
    <name>servicemeetingui</name>
    <message>
        <source>Song</source>
        <translation type="vanished">သီချင်း</translation>
    </message>
    <message>
        <source>min</source>
        <comment>Abbreviation of minutes</comment>
        <translation type="vanished">မိ.</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="vanished">ဆုတောင်းချက်</translation>
    </message>
</context>
<context>
    <name>speakersui</name>
    <message>
        <location filename="../speakersui.ui" line="14"/>
        <source>Congregations and Speakers</source>
        <translation>အသင်းတော် နဲ့ ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="400"/>
        <source>Congregation...</source>
        <translation>အသင်းတော်...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="371"/>
        <source>Speaker...</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1332"/>
        <source>Toggle Talks Editable</source>
        <translation>ဟောပြောချက် ပြင်နိုင်/မပြင်နိုင်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1300"/>
        <source>Add Multiple Talks</source>
        <translation>ဟောပြောချက်တွေ ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="95"/>
        <location filename="../speakersui.ui" line="136"/>
        <location filename="../speakersui.ui" line="180"/>
        <location filename="../speakersui.ui" line="246"/>
        <location filename="../speakersui.ui" line="1303"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="494"/>
        <source>Select a Congregation or Speaker</source>
        <translation>အသင်းတော် သို့မဟုတ် ဟောပြောသူ ရွေးမယ်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="725"/>
        <source>Info</source>
        <translation>အချက်အလက်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="792"/>
        <source>Address</source>
        <translation>လိပ်စာ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="865"/>
        <location filename="../speakersui.cpp" line="171"/>
        <source>Circuit</source>
        <translation>တိုက်နယ်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="667"/>
        <location filename="../speakersui.ui" line="1685"/>
        <location filename="../speakersui.cpp" line="179"/>
        <source>Congregation</source>
        <translation>အသင်းတော်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="92"/>
        <source>Speakers</source>
        <translation>ဟောပြောသူများ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="133"/>
        <source>Group by congregation</source>
        <translation>အသင်းတော် အလိုက် အုပ်စုဖွဲ့ပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="177"/>
        <source>Group by circuit</source>
        <translation>တိုက်နယ်အလိုက် အုပ်စုဖွဲ့ပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="221"/>
        <source>Filter</source>
        <translation>စစ်ထုတ်ပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="243"/>
        <source>Configure Filter</source>
        <translation>စစ်ထုတ်မှုကို စီစဥ်ပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="882"/>
        <source>Congregation Details</source>
        <translation>အသင်းတော် အသေးစိတ်များ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="894"/>
        <source>Meeting Times</source>
        <translation>စည်းဝေးအချိန်များ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="999"/>
        <location filename="../speakersui.ui" line="1169"/>
        <source>Mo</source>
        <translation>တနင်္လာ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1004"/>
        <location filename="../speakersui.ui" line="1174"/>
        <source>Tu</source>
        <translation>အင်္ဂါ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1009"/>
        <location filename="../speakersui.ui" line="1179"/>
        <source>We</source>
        <translation>ဗုဒ္ဓဟူး</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1014"/>
        <location filename="../speakersui.ui" line="1184"/>
        <source>Th</source>
        <translation>ကြာသပတေး</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1019"/>
        <location filename="../speakersui.ui" line="1189"/>
        <source>Fr</source>
        <translation>သောကြာ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1024"/>
        <location filename="../speakersui.ui" line="1194"/>
        <source>Sa</source>
        <translation>စနေ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1029"/>
        <location filename="../speakersui.ui" line="1199"/>
        <source>Su</source>
        <translation>တနင်္ဂနွေ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1363"/>
        <source>Personal Info</source>
        <translation>ကိုယ်ရေးအချက်အလက်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1786"/>
        <location filename="../speakersui.cpp" line="833"/>
        <source>First Name</source>
        <translation>ရှေ့နာမည်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1838"/>
        <source>Mobile</source>
        <translation>မိုဘိုင်းဖုန်း</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1576"/>
        <location filename="../speakersui.cpp" line="834"/>
        <source>Last Name</source>
        <translation>နောက်နာမည်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1540"/>
        <source>Phone</source>
        <translation>ဖုန်း</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1377"/>
        <source>Public Talks</source>
        <translation>လူထုဟောပြောချက်များ</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1646"/>
        <source>E-mail</source>
        <translation>အီးမေးလ်</translation>
    </message>
    <message>
        <location filename="../speakersui.ui" line="1467"/>
        <source>Notes</source>
        <translation>မှတ်စုများ</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="187"/>
        <source>Speaker</source>
        <translation>ဟောပြောသူ</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="361"/>
        <location filename="../speakersui.cpp" line="541"/>
        <source>Undefined</source>
        <translation>မသတ်မှတ်ရသေး</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="444"/>
        <location filename="../speakersui.cpp" line="449"/>
        <source>%1 Meeting Day/Time</source>
        <translation>စည်းဝေး နေ့စွဲ/အချိန် %1</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="574"/>
        <source>A speaker with the same name already exists: &apos;%1&apos;. Do you want to change the name?</source>
        <translation>&apos;%1&apos; ဟောပြောသူ နာမည်တူ ရှိပြီးသား ဖြစ်နေတယ်၊ နာမည်ပြောင်းချင်လား?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="706"/>
        <source>The congregation has speakers!</source>
        <translation>အသင်းတော်မှာ ဟောပြောသူတွေရှိတယ်။</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="711"/>
        <source>Remove the congregation?</source>
        <translation>ဒီအသင်းတော် ဖယ်လိုက်မလား?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="757"/>
        <source>Remove the speaker?</source>
        <translation>ဒီဟောပြောသူ ဖယ်လိုက်မလား?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="762"/>
        <source>The speaker is scheduled for talks! These talks will
be moved to the To Do List if you remove the speaker.</source>
        <translation>ဒီဟောပြောသူကို ဟောပြောချက်တွေအတွက် တာဝန်ပေးထားတယ်။ ဒီဟောပြောသူကို ဖယ်မယ်ဆိုရင် ဒီဟောပြောချက်တွေကို လုပ်ရမယ့်စာရင်းထဲ ရွှေ့လိုက်မယ်။</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="826"/>
        <source>Missing Information</source>
        <translation>အချက်အလက် မရှိ</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="826"/>
        <source>Select congregation first</source>
        <translation>အသင်းတော် အရင်ရွေးပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="859"/>
        <source>New Congregation</source>
        <translation>အသင်းတော်အသစ်</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="927"/>
        <source>Add Talks</source>
        <translation>ဟောပြောချက် ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="927"/>
        <source>Enter talk numbers separated by commas or periods</source>
        <translation>ဟောပြောချက် နံပါတ်ကို ပုဒ်ကလေး/မ ခံပြီး ရိုက်ထည့်ပါ</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="963"/>
        <source>Change congregation to &apos;%1&apos;?</source>
        <translation>&apos;%1&apos; အသင်းတော်ကို ပြောင်းမလား?</translation>
    </message>
    <message>
        <location filename="../speakersui.cpp" line="965"/>
        <source>The speaker is scheduled for outgoing talks. These talks will
be moved to the To Do List if you change the congregation.</source>
        <translation>ဒီဟောပြောသူကို တခြား အသင်းတော်မှာ သွား ဟောပြောဖို့ တာဝန်ပေးထားတယ်။ အသင်းတော်ကို ပြောင်းလိုက်မယ်ဆိုရင် ဒီဟောပြောချက်တွေကို လုပ်ရမယ့်စာရင်းထဲ ရွှေ့လိုက်မယ်။</translation>
    </message>
</context>
<context>
    <name>startup</name>
    <message>
        <location filename="../startup.ui" line="35"/>
        <source>Start Page</source>
        <translation>အစစာမျက်နှာ</translation>
    </message>
</context>
<context>
    <name>study</name>
    <message>
        <source>Song</source>
        <translation type="vanished">သီချင်း</translation>
    </message>
    <message>
        <source>Prayer</source>
        <translation type="vanished">ဆုတောင်းချက်</translation>
    </message>
    <message>
        <source>Source</source>
        <translation type="vanished">ရင်းမြစ်</translation>
    </message>
    <message>
        <source>Conductor</source>
        <translation type="vanished">သင်တန်းကိုင်</translation>
    </message>
    <message>
        <source>Reader</source>
        <translation type="vanished">စာဖတ်သူ</translation>
    </message>
</context>
<context>
    <name>sync_cloud</name>
    <message>
        <location filename="../sync_cloud.cpp" line="240"/>
        <source>Version conflict: The cloud changes have been made with a newer version!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sync_cloud.cpp" line="246"/>
        <source>Version conflict: The cloud data needs to be updated with the same version by an authorized user.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>territorymanagement</name>
    <message>
        <location filename="../territorymanagement.ui" line="14"/>
        <source>Territories</source>
        <translation>ရပ်ကွက်</translation>
    </message>
</context>
</TS>
