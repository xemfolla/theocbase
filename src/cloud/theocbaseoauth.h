#ifndef THEOCBASEOAUTH_H
#define THEOCBASEOAUTH_H

#include <QAbstractOAuth>
#include <QAbstractOAuth2>
#include <QUrlQuery>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QEventLoop>
#include <QSettings>

class TheocBaseOAuth : public QAbstractOAuth2
{
    Q_OBJECT
public:
    TheocBaseOAuth(QObject *parent = nullptr);
    Q_INVOKABLE void setCredentials(const QString username, const QString password);
    bool isTokenValid();
    int userId() const;
public slots:
    void grant() override;
    void revoke();
    void refreshAccessToken();

    QNetworkReply *get(const QUrl &url, const QVariantMap &parameters = QVariantMap()) override;
    QNetworkReply *post(const QUrl &url, const QVariantMap &parameters = QVariantMap()) override;

protected:
#if QT_VERSION < QT_VERSION_CHECK(5,10,0)
    QString responseType() const override;
#else
    QString responseType() const;
#endif
    QUrl accessTokenUrl() const;
    void setAccessTokenUrl(const QUrl &accessTokenUrl);

private:
    QUrlQuery createQuery(const QVariantMap &parameters);
    void accessTokenRequestFinished(QNetworkReply *reply);
    QString _refreshToken;
    QUrl _accessTokenUrl;
    QDateTime _expiresAt;
    QString _username;
    QString _password;
};

#endif // THEOCBASEOAUTH_H
