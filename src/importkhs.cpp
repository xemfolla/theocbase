#include "importkhs.h"

importKHS::importKHS(QString directory)
{
    m_directory = directory;
    sql = &Singleton<sql_class>::Instance();
}

void importKHS::Go()
{
    sql->startTransaction();

    QString lastImport_start = QVariant(QDateTime::currentDateTime().toTime_t()).toString();
    bool imported = false;

    QString filename = m_directory.absolutePath() + m_directory.separator() + "options.DBF";
    DBFFile optionsFile(filename);
    if (optionsFile.header.numRecords > 0) {
        QMap<QString, QVariant> optionsRrecord = optionsFile.records[0];
        QString language = optionsRrecord.value("LANGUAGE").toString();
        sql_items l;
        l = sql->selectSql("languages", "language", language);
        int languageId = 2;
        if (!l.empty()) {
            languageId = l[0].value("id").toInt();
        }

        imported = importCongregations();
        imported |= importNames();
        imported |= importSpeakers();
        imported |= importOutlines(languageId);
        imported |= importTalklist(languageId);
        imported |= importSchedule();
        imported |= importOutgoing();
    }

    if (imported) {
        sql->saveSetting("last_khsimport_start", lastImport_start);
        sql->saveSetting("last_khsimport_end", QVariant(QDateTime::currentDateTime().toTime_t()).toString());
    }

    QMessageBox::information(nullptr, QObject::tr("Import") + " KHS", QObject::tr("Import Complete"));
    //sql->commitTransaction();
}

importKHS::DBFFile::DBFFile(QString fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Could not open " << fileName;
        return;
    }

    file.seek(0);
    // read header
    file.read(reinterpret_cast<char *>(&header), sizeof(header));
    // read field descriptor
    DBFFieldDescriptor fieldDescriptor;
    while (file.read(reinterpret_cast<char *>(&fieldDescriptor), sizeof(fieldDescriptor))) {
        fields.insert(QVariant(fieldDescriptor.fieldName).toString(), fieldDescriptor);
        char nextChar;
        file.getChar(&nextChar);
        if (nextChar == 0x0D) {
            // jump over 263-byte range, that contains the backlink information,
            // which is the relative path of an associated database (.dbc) file.
            file.seek(file.pos() + 263);
            break;
        } else
            file.seek(file.pos() - 1);
    }
    // read records
    QVarLengthArray<char, 1024> recordData(header.recordSize);
    while (file.read(recordData.data(), header.recordSize)) {
        QMap<QString, QVariant> record = getFieldData(recordData.constData());
        records.append(record);

        char nextChar;
        if (!file.getChar(&nextChar))
            break;
        if (nextChar == 0x1A)
            break;
        else
            file.seek(file.pos() - 1);
    }
    file.close();
}

QMap<QString, QVariant> importKHS::DBFFile::getFieldData(const char *recordData) const
{
    QMap<QString, QVariant> recordMap;
    QMapIterator<QString, DBFFieldDescriptor> i(fields);
    while (i.hasNext()) {
        i.next();
        DBFFieldDescriptor fieldDescriptor = i.value();

        switch (fieldDescriptor.fieldType) {
        case 'C': {
            QVarLengthArray<char, 255> buffer(fieldDescriptor.fieldLength + 1);
            memcpy(buffer.data(), &recordData[fieldDescriptor.fieldOffset], size_t(fieldDescriptor.fieldLength));
            buffer.data()[fieldDescriptor.fieldLength] = '\0';
            QTextCodec *codec = QTextCodec::codecForName("Windows-1250");
            recordMap.insert(i.key(), QString(codec->toUnicode(buffer.constData())).trimmed());
            break;
        }
        case 'D': {
            QVarLengthArray<char, 9> buffer(fieldDescriptor.fieldLength + 1);
            memcpy(buffer.data(), &recordData[fieldDescriptor.fieldOffset], size_t(fieldDescriptor.fieldLength));
            buffer.data()[fieldDescriptor.fieldLength] = '\0';
            recordMap.insert(i.key(), QDate::fromString(QVariant(buffer.constData()).toString(), "yyyyMMdd"));
            break;
        }
        case 'I': {
            unsigned char buffer[4];
            memcpy(buffer, &recordData[fieldDescriptor.fieldOffset], 4);
            int intValue = buffer[0] | (buffer[1] << 8) | (buffer[2] << 16) | (buffer[3] << 24);
            recordMap.insert(i.key(), intValue);
            break;
        }
        case 'L': {
            char logicalValue = recordData[fieldDescriptor.fieldOffset];
            recordMap.insert(i.key(), QString("1TtYy").indexOf(logicalValue) != -1);
            break;
        }
        case 'M':
            break;
        default:
            break;
        }
    }
    return recordMap;
}

bool importKHS::importCongregations()
{
    bool imported = false;
    // read congregations.DBF-file
    QString filename = m_directory.absolutePath() + m_directory.separator() + "congregations.DBF";
    DBFFile dbfFile(filename);
    ccongregation cc;
    for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
        QMap<QString, QVariant> record = dbfFile.records[i];
        // import only congregations with valid names; field name must not exceed 10 characters
        QString congregationName = record.value("CONGREGATI").toString();
        if (!congregationName.isEmpty()) {
            ccongregation::congregation c = cc.getOrAddCongregation(congregationName);
            int oldId = record.value("ID").toInt();
            congOldIdToNewId.insert(oldId, c.id);
            c.address = record.value("ADDRESS").toString();
            c.circuit = record.value("CIRCUIT").toString();
            c.info = record.value("NOTES").toString();
            //            qDebug() << "coordinator's name and phone number:" << record.value("PO").toString();
            //            qDebug() << "public talk coordinator's name:" << record.value("COORDINATO").toString();
            //            qDebug() << "public talk coordinator's phone (home):" << record.value("EPHONE").toString();
            //            qDebug() << "public talk coordinator's phone (mobile):" << record.value("ECELLPHONE").toString();
            //            qDebug() << "public talk coordinator's e-mail:" << record.value("EMAIL").toString();
            //            qDebug() << "public talk coordinator's address:" << record.value("COORDADD").toString();
            c.save();
            imported = true;
        }
    }
    return imported;
}

bool importKHS::importNames()
{
    bool imported = false;
    // read names.DBF-file
    QString filename = m_directory.absolutePath() + m_directory.separator() + "names.DBF";
    DBFFile dbfFile(filename);
    for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
        QMap<QString, QVariant> record = dbfFile.records[i];

        cpersons persons;
        person *p = nullptr;

        QString firstName = record.value("FIRSTNAME").toString();
        QString lastName = record.value("LASTNAME").toString();
        QString fullname = firstName + " " + lastName;
        QString formatted = "firstname || ' ' || lastname";
        QString sqlcommand = QString("SELECT id FROM persons WHERE %1 = '%2' ORDER BY active DESC, id DESC").arg(formatted, fullname);
        sql_items ps = sql->selectSql(sqlcommand);
        if (!ps.empty()) {
            p = persons.getPerson(ps[0].value("id").toInt());
        }

        int gender = record.value("GENDER").toInt();
        if (!p) {
            // create new
            p = new person();
            p->setLastname(lastName);
            p->setFirstname(firstName);
            p->setGender(gender == 1 ? person::Male : person::Female);
            persons.addPerson(p);
        }
        int oldId = record.value("ID").toInt();
        namesOldIdToNewId.insert(oldId, p->id());

        p->setCongregationid(sql->getSetting("congregation_id", "1").toInt());
        p->setPhone(record.value("EPHONE").toString());
        p->setMobile(record.value("ECELLPHONE").toString());
        p->setEmail(record.value("EMAIL").toString());
        p->setInfo(record.value("ADDRESS").toString());

        bool isElder = record.value("ELDER").toBool();
        bool isServant = record.value("SERVANT").toBool();
        p->setServant(isElder || isServant);

        bool isSpeaker = record.value("SPEAKER").toBool();
        bool isLMMOverseer = record.value("LM_OVERSEE").toBool();
        // Treasures From God's Word
        bool isLMMChairman = record.value("LM_CHAIRMA").toBool();
        bool isDoingLMMPrayer = record.value("LM_OVERSEE").toBool();
        bool isDoingLMMTreasuresTalk = record.value("LM_TALK").toBool();
        bool isDoingLMMDigging = record.value("LM_GEMS").toBool();
        bool isLMMAuxAssistant = record.value("LM_AUX_ASS").toBool();
        bool isDoingLMMBibleReading = record.value("LM_BIBLERE").toBool();
        // Apply Yourself to the Field Ministry
        bool isDoingLMMPreparePresentations = record.value("LM_PREPARE").toBool();
        bool isDoingLMMInitialCall = record.value("LM_IC").toBool();
        bool isDoingLMMReturnVisit = record.value("LM_RV").toBool();
        bool isDoingLMMBibleStudy = record.value("LM_BS").toBool();
        bool isDoingLMMApplyTalk = record.value("LM_TALKBYB").toBool();
        bool isDoingLMMAssistant = record.value("LM_ASSISTA").toBool();
        bool isDoingLMMMainHallOnly = record.value("LM_MAIN_ON").toBool();
        bool isDoingLMMAuxHallOnly = record.value("LM_AUX_ONL").toBool();
        // Living As Christians
        bool isDoingLMMLivingTalks = record.value("LM_PARTS").toBool();
        bool isLMMCBSConductor = record.value("CONDUCTOR").toBool();
        bool isLMMCBSReader = record.value("BSREADER").toBool();
        // Weekend Meeting
        bool isWEChairman = record.value("CHAIRMAN").toBool();
        bool isWTConductor = record.value("WTCONDUCTOR").toBool();
        bool isWTReader = record.value("READER").toBool();
        // Sound
        //        qDebug() << "main hall attendant:" << record.value("ATTENDANT").toString();
        //        qDebug() << "door assistant:" << record.value("DOOR").toString();
        //        qDebug() << "sound:" << record.value("SOUND").toString();
        //        qDebug() << "video assistant:" << record.value("VIDEOASST").toString();
        //        qDebug() << "microphones:" << record.value("MICS").toString();
        //        qDebug() << "platform:" << record.value("STAGE").toString();
        // Public Witnessing
        //        qDebug() << "approved for public witnessing:" << record.value("CART").toString();

        int usefor = p->usefor();
        usefor = isSpeaker ? (usefor | person::PublicTalk) : (usefor & ~person::PublicTalk);
        usefor = isLMMOverseer ? (usefor | person::LMM_Chairman) : (usefor & ~person::LMM_Chairman);
        usefor = isLMMChairman ? (usefor | person::LMM_Chairman) : (usefor & ~person::LMM_Chairman);
        usefor = isLMMAuxAssistant ? (usefor | person::LMM_Chairman) : (usefor & ~person::LMM_Chairman);
        usefor = isDoingLMMPrayer ? (usefor | person::Prayer) : (usefor & ~person::Prayer);
        usefor = isDoingLMMTreasuresTalk ? (usefor | person::LMM_Treasures) : (usefor & ~person::LMM_Treasures);
        usefor = isDoingLMMDigging ? (usefor | person::LMM_Digging) : (usefor & ~person::LMM_Digging);
        usefor = isDoingLMMBibleReading ? (usefor | person::LMM_BibleReading) : (usefor & ~person::LMM_BibleReading);
        usefor = isDoingLMMPreparePresentations ? (usefor | person::LMM_PreparePresentations) : (usefor & ~person::LMM_PreparePresentations);
        usefor = isDoingLMMInitialCall ? (usefor | person::LMM_InitialCall) : (usefor & ~person::LMM_InitialCall);
        usefor = isDoingLMMReturnVisit ? (usefor | person::LMM_ReturnVisit) : (usefor & ~person::LMM_ReturnVisit);
        usefor = isDoingLMMBibleStudy ? (usefor | person::LMM_BibleStudy) : (usefor & ~person::LMM_BibleStudy);
        usefor = isDoingLMMApplyTalk ? (usefor | person::LMM_ApplyTalks) : (usefor & ~person::LMM_ApplyTalks);
        usefor = isDoingLMMAssistant ? (usefor | person::Assistant) : (usefor & ~person::Assistant);
        usefor = isDoingLMMMainHallOnly ? (usefor | person::SchoolMain) : (usefor & ~person::SchoolMain);
        usefor = isDoingLMMAuxHallOnly ? (usefor | person::SchoolAux) : (usefor & ~person::SchoolAux);
        usefor = isDoingLMMLivingTalks ? (usefor | person::LMM_LivingTalks) : (usefor & ~person::LMM_LivingTalks);
        usefor = isLMMCBSConductor ? (usefor | person::CBSConductor) : (usefor & ~person::CBSConductor);
        usefor = isLMMCBSReader ? (usefor | person::CBSReader) : (usefor & ~person::CBSReader);
        usefor = isWEChairman ? (usefor | person::Chairman) : (usefor & ~person::Chairman);
        usefor = isWTConductor ? (usefor | person::WtCondoctor) : (usefor & ~person::WtCondoctor);
        usefor = isWTReader ? (usefor | person::WtReader) : (usefor & ~person::WtReader);
        usefor = isWEChairman ? (usefor | person::Prayer) : (usefor & ~person::Prayer);
        p->setUsefor(usefor);

        //        qDebug() << "diceased:" << record.value("DECEASED").toBool();
        //        qDebug() << "moveDate:" << record.value("MOVE_DATE").toDate();

        p->update();
        imported = true;
    }
    return imported;
}

bool importKHS::importSpeakers()
{
    bool imported = false;
    // read speakers.DBF-file
    QString filename = m_directory.absolutePath() + m_directory.separator() + "speakers.DBF";
    DBFFile dbfFile(filename);
    for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
        QMap<QString, QVariant> record = dbfFile.records[i];
        int namesId = record.value("NAMES_ID").toInt();
        cpersons persons;
        person *p = nullptr;
        p = persons.getPerson(namesOldIdToNewId.value(namesId));
        if (!p) {
            QString firstName = record.value("FIRSTNAME").toString();
            QString lastName = record.value("LASTNAME").toString();
            QString fullname = firstName + " " + lastName;
            QString formatted = "firstname || ' ' || lastname";
            QString sqlcommand = QString("SELECT id FROM persons WHERE %1 = '%2' ORDER BY active DESC, id DESC").arg(formatted, fullname);
            sql_items ps = sql->selectSql(sqlcommand);
            if (!ps.empty()) {
                p = persons.getPerson(ps[0].value("id").toInt());
            }

            if (!p) {
                // create new
                p = new person();
                p->setLastname(lastName);
                p->setFirstname(firstName);
                p->setGender(person::Male);
                persons.addPerson(p);

                int congregationId = record.value("CONGREGATI").toInt();
                p->setCongregationid(congOldIdToNewId.value(congregationId));
            }
        }
        int oldId = record.value("ID").toInt();
        speakersOldIdToNewId.insert(oldId, p->id());

        p->setPhone(record.value("EPHONE").toString());
        p->setMobile(record.value("ECELLPHONE").toString());
        p->setEmail(record.value("EMAIL").toString());
        p->setInfo(record.value("ADDRESS").toString());

        bool isElder = record.value("ELDER").toBool();
        p->setServant(isElder);
        qDebug() << "approved for outgoing talks:" << record.value("APPROVED").toBool();
        p->setUsefor(person::PublicTalk);

        QStringList info = (QStringList()
                            << p->info()
                            << record.value("NOTES").toString());
        info.removeAll("");
        p->setInfo(info.join('\n'));

        p->update();

        imported = true;
    }
    return imported;
}

bool importKHS::importOutlines(int languageId)
{
    bool imported = false;
    // read outlines.DBF-file
    QString filename = m_directory.absolutePath() + m_directory.separator() + "outlines.DBF";
    DBFFile dbfFile(filename);
    for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
        QMap<QString, QVariant> record = dbfFile.records[i];
        int themeNumber = record.value("OUTLINE").toInt();
        QString themeName;
        switch (languageId) {
        case 5:
            themeName = record.value("TITLE_PORT").toString();
            break;
        case 7:
            themeName = record.value("TITLE_GERM").toString();
            break;
        case 8:
            themeName = record.value("TITLE_FREN").toString();
            break;
        case 9:
            themeName = record.value("TITLE_DUTC").toString();
            break;
        case 11:
            themeName = record.value("TITLE_ESP").toString();
            break;
        default:
            themeName = record.value("TITLE").toString();
            break;
        }
        QString revision = record.value("REVISION").toString();

        sql_item pt;
        pt.insert("theme_number", themeNumber);
        pt.insert("theme_name", themeName);
        pt.insert("revision", revision);
        pt.insert("lang_id", languageId);

        QDate date = QDate::currentDate();
        sql_items e = sql->selectSql(
                "SELECT * FROM publictalks WHERE theme_number = '" + QVariant(themeNumber).toString()
                + "' AND lang_id = " + QVariant(languageId).toString()
                + " AND active AND (discontinue_date IS NULL OR discontinue_date='' OR discontinue_date > '"
                + date.toString(Qt::ISODate)
                + "') AND (release_date IS NULL OR release_date='' OR release_date <= '"
                + date.toString(Qt::ISODate) + "') ");
        int themeId = 0;
        if (!e.empty()) {
            // update existing talk
            themeId = e[0].value("id").toInt();
            sql->updateSql("publictalks", "id", QVariant(themeId).toString(), &pt);
        } else {
            // check for discontinued talks
            e = sql->selectSql("SELECT * FROM publictalks WHERE theme_number = '" + QVariant(themeNumber).toString() + "' AND lang_id = '" + QVariant(languageId).toString() + "' AND active");
            if (!e.empty()) {
                // discontined talk exists; add current date as release date,
                // to make sure no concurrent talks exist at the same time
                pt.insert("release_date", date.toString(Qt::ISODate));
            }
            // insert new talk
            themeId = sql->insertSql("publictalks", &pt, "id");
        }
        outlinesOldIdToNewId.insert(themeNumber, themeId);
        imported = true;
    }
    return imported;
}

bool importKHS::importTalklist(int languageId)
{
    bool imported = false;
    // read talklist.DBF-file
    QString filename = m_directory.absolutePath() + m_directory.separator() + "talklist.DBF";
    DBFFile dbfFile(filename);
    for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
        QMap<QString, QVariant> record = dbfFile.records[i];
        int speakerOldId = record.value("SPEAKER_ID").toInt();
        int speakerNewId = speakersOldIdToNewId.value(speakerOldId);
        int outlineOldId = record.value("OUTLINE").toInt();
        int outlineNewId = outlinesOldIdToNewId.value(outlineOldId);

        if (outlineNewId > 0) {
            sql_item speakertalk;
            speakertalk.insert(":speaker_id", speakerNewId);
            speakertalk.insert(":lang_id", languageId);
            speakertalk.insert(":theme_id", outlineNewId);
            QVariant sp_talkID = sql->selectScalar("SELECT id FROM speaker_publictalks WHERE speaker_id = :speaker_id AND lang_id = :lang_id AND theme_id = :theme_id", &speakertalk);
            if (sp_talkID.isNull()) {
                sql_item s;
                s.insert("speaker_id", speakerNewId);
                s.insert("theme_id", outlineNewId);
                s.insert("lang_id", languageId);
                sql->insertSql("speaker_publictalks", &s, "id");
                imported = true;
            }
        } else {
            qDebug() << "Could not find talk";
        }
    }
    return imported;
}

bool importKHS::importSchedule()
{
    bool imported = false;
    // read schedule.DBF-file
    QString filename = m_directory.absolutePath() + m_directory.separator() + "schedule.DBF";
    DBFFile dbfFile(filename);
    for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
        QMap<QString, QVariant> record = dbfFile.records[i];
        QDate date = record.value("DATE").toDate();

        cpublictalks cp;
        cptmeeting *temp = cp.getMeeting(date);

        // in - speaker
        int speakerOldId = record.value("SPEAKER_ID").toInt();
        int speakerNewId = speakersOldIdToNewId.value(speakerOldId);
        if (speakerNewId > 0) {
            int outlineOldId = record.value("OUTLINE").toInt();
            int outlineNewId = outlinesOldIdToNewId.value(outlineOldId);
            QVariant themeNumber = sql->selectScalar("select theme_number from publictalks where id = :talkNewID", outlineNewId, 0);
            if (!themeNumber.isNull()) {
                temp->theme = cp.getThemeByNumber(themeNumber.toInt(), date);
                temp->setSpeaker(cpersons::getPerson(speakerNewId));
                imported = true;
            }
        }

        // chairman
        int chairmanOldId = record.value("CHAIRMAN").toInt();
        int chairmanNewId = namesOldIdToNewId.value(chairmanOldId);
        if (chairmanNewId > 0) {
            temp->setChairman(cpersons::getPerson(chairmanNewId));
            imported = true;
        }

        // wt-reader
        int wtReaderOldId = record.value("READER").toInt();
        int wtReaderNewId = namesOldIdToNewId.value(wtReaderOldId);
        if (wtReaderNewId > 0) {
            temp->setWtReader(cpersons::getPerson(wtReaderNewId));
            imported = true;
        }

        // host
        int hostOldId = record.value("HOST").toInt();
        int hostNewId = namesOldIdToNewId.value(hostOldId);
        if (hostNewId > 0) {
            temp->setHospitalityhost(cpersons::getPerson(hostNewId));
            imported = true;
        }

        if (imported) {
            temp->save();
        }
    }

    // remove old data from last import
    if (imported) {
        QString lastImport_start = sql->getSetting("last_khsimport_start", "-1");
        if (lastImport_start != "-1") {
            QString lastImport_end = sql->getSetting("last_khsimport_end", "-1");
            sql->removeSql("publicmeeting", "time_stamp >= " + lastImport_start + " AND time_stamp <= " + lastImport_end);
        }
    }
    return imported;
}

bool importKHS::importOutgoing()
{
    bool imported = false;
    // read schedule.DBF-file
    QString filename = m_directory.absolutePath() + m_directory.separator() + "schedule.DBF";
    DBFFile dbfFile(filename);
    for (int i = 0; i < int(dbfFile.header.numRecords); i++) {
        QMap<QString, QVariant> record = dbfFile.records[i];
        QDate date = record.value("DATE").toDate();

        cpublictalks cp;
        cptmeeting *temp = cp.getMeeting(date);

        // out - speaker
        int speakerOldId = record.value("SPEAKER").toInt();
        int speakerNewId = speakersOldIdToNewId.value(speakerOldId, 0);
        if (speakerNewId > 0) {
            int outlineOldId = record.value("OUTLINE").toInt();
            int outlineNewId = outlinesOldIdToNewId.value(outlineOldId);

            QVariant themeNumber = sql->selectScalar("select theme_number from publictalks where id = :talkNewID", outlineNewId, 0);
            if (!themeNumber.isNull()) {
                int congOldId = record.value("CONGREGATI").toInt();
                int congNewID = congOldIdToNewId.value(congOldId);

                // update of existing rows
                //  - search for date & congregation
                //    update speaker & theme
                //  - rows that remain with old ta1ks-import time-stamp
                //    will be removed later
                sql_item outgoing;
                outgoing.insert(":date", date.toString(Qt::ISODate));
                outgoing.insert(":congregation_id", congNewID);
                QVariant outgoingID = sql->selectScalar("SELECT id FROM outgoing WHERE date = :date AND congregation_id = :congregation_id", &outgoing);

                if (!outgoingID.isNull()) {
                    sql_item s;
                    s.insert("id", outgoingID);
                    s.insert("date", date);
                    s.insert("speaker_id", speakerNewId);
                    s.insert("congregation_id", congNewID);
                    s.insert("theme_id", outlineNewId);

                    sql->updateSql("outgoing", "id", outgoingID.toString(), &s);

                    imported = true;
                } else {
                    cpublictalks cp;
                    cptmeeting *currentMeeting = cp.getMeeting(date);
                    cpoutgoing *out = cp.addOutgoingSpeaker(date, speakerNewId, outlineNewId, congNewID);

                    if (out) {
                        currentMeeting->save();
                        imported = true;
                    }
                }
            }
        }

        if (imported) {
            temp->save();
        }
    }

    // remove old data from last import
    if (imported) {
        QString lastImport_start = sql->getSetting("last_khsimport_start", "-1");
        if (lastImport_start != "-1") {
            QString lastImport_end = sql->getSetting("last_khsimport_end", "-1");
            sql->removeSql("outgoing", "time_stamp >= " + lastImport_start + " AND time_stamp <= " + lastImport_end);
        }
    }

    return imported;
}
