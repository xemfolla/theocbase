import QtQuick 2.4
import QtQuick.Controls.Private 1.0

// This file contains private Qt Quick modules that might change in future versions of Qt

MouseArea {
    id: _root
    property string text: ""

    signal toolTipRequest()

    anchors.fill: parent
    hoverEnabled: _root.enabled
    propagateComposedEvents: true

    onClicked: mouse.accepted = false;
    onPressed: mouse.accepted = false;
    onReleased: mouse.accepted = false;
    onDoubleClicked: mouse.accepted = false;
    onPositionChanged: mouse.accepted = false;
    onPressAndHold: mouse.accepted = false;

    onExited: Tooltip.hideText()
    onCanceled: Tooltip.hideText()
    onTextChanged: {
        if (text.length == 0)
            Tooltip.hideText()
    }

    Timer {
        interval: 1000
        running: _root.enabled && _root.containsMouse && _root.text.length
        onTriggered: {
            _root.toolTipRequest()
            Tooltip.showText(_root, Qt.point(_root.mouseX, _root.mouseY), _root.text)
        }
    }
}
