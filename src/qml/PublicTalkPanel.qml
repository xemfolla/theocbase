/**
 * This file is part of TheocBase.
 *
 * Copyright (C) 2011-2018, TheocBase Development Team, see AUTHORS.
 *
 * TheocBase is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TheocBase is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TheocBase.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import net.theocbase 1.0

Item {
    id: weekendMeetingEdit
    property string title: "Public Talk"
    width: 500
    height: 700

    property CPTMeeting meeting
    onMeetingChanged: {
        if (meeting)
            controller.date = meeting.date
    }

    PublicMeetingController { id: controller }

    anchors.fill: parent

    ColumnLayout {
        id: layout
        anchors.fill: parent
        anchors.margins: 10

        // Theme
        Label {
            text: qsTr("Theme")
            font.capitalization: Font.AllUppercase
        }

        ComboBoxTable {
            id: comboTheme
            Layout.fillWidth: true
            currentText: (meeting.themeNumber > 0 ? meeting.themeNumber + " " : "") + meeting.theme
            column4.visible: false
            column5.visible: false

            onBeforeMenuShown: {
                model = controller.themeList(meeting.speaker ? meeting.speaker.id : 0)
                column2.resizeToContents()
            }
            onRowSelected: {
                console.log(id)
                meeting.setTheme(id)
                meeting.save()
            }
        }

        // Congregations
        Label {
            text: qsTr("Congregation")
            font.capitalization: Font.AllUppercase
        }
        ComboBoxTable {
            id: comboCongregation
            Layout.fillWidth: true
            column3.visible: false
            column4.visible: false
            column5.visible: false
            currentText: meeting.speaker ? meeting.speaker.congregationName : ""

            onBeforeMenuShown: {
                model = controller.congregationList()
                column2.resizeToContents()
            }
            onRowSelected: {
                console.log(id)
                //myMeeting.setTheme(id)
            }
        }

        Label {
            text: qsTr("Speaker")
            font.capitalization: Font.AllUppercase
        }
        ComboBoxTable {
            id: comboSpeaker
            Layout.fillWidth: true
            column4.visible: false
            currentText: meeting.speaker ? meeting.speaker.fullname : ""

            onBeforeMenuShown: {
                var congregationId = 0
                if (comboCongregation.currentText != "") {
                    if (typeof(comboCongregation.model) === "undefined")
                        comboCongregation.model = controller.congregationList()
                    congregationId = comboCongregation.model.get(comboCongregation.model.find(comboCongregation.currentText,2)).id
                }
                var talkId = 0
                if (comboTheme.currentText != "") {
                    if (typeof (comboTheme.model) === "undefined")
                        comboTheme.model = controller.themeList(0)
                    talkId = comboTheme.model.get(comboTheme.model.find(meeting.themeNumber + " " + meeting.theme,2)).id
                }                
                model = controller.speakerList(talkId, congregationId)                
                column2.resizeToContents()
            }
            onRowSelected: {
                console.log(id)
                var speaker = CPersons.getPerson(id)
                meeting.speaker = speaker
                meeting.save()
            }
        }

        Label {
            text: qsTr("Mobile")
            font.capitalization: Font.AllUppercase
        }
        TextArea {
            Layout.fillWidth: true
            selectByMouse: true
            wrapMode: Text.WordWrap
            text: meeting.speaker ? meeting.speaker.mobile : ""
            onEditingFinished: { meeting.speaker.mobile = text; meeting.speaker.update() }
        }

        Label {
            text: qsTr("Phone")
            font.capitalization: Font.AllUppercase
        }
        TextArea {
            Layout.fillWidth: true
            selectByMouse: true
            wrapMode: Text.WordWrap
            text: meeting.speaker ? meeting.speaker.phone : ""
            onEditingFinished: { meeting.speaker.phone = text; meeting.speaker.update() }
        }

        Label {
            text: qsTr("Email")
            font.capitalization: Font.AllUppercase
        }
        TextArea {
            Layout.fillWidth: true
            selectByMouse: true
            wrapMode: Text.WordWrap
            text: meeting.speaker ? meeting.speaker.email : ""
            onEditingFinished: { meeting.speaker.email = text; meeting.speaker.update() }
        }

        Label {
            text: qsTr("Info")
            font.capitalization: Font.AllUppercase
        }
        TextArea {
            Layout.fillWidth: true
            selectByMouse: true
            wrapMode: Text.WordWrap
            text: meeting.speaker ? meeting.speaker.info : ""
            onEditingFinished: { meeting.speaker.info = text; meeting.speaker.update() }
        }

        // Hospitality
        Label {
            text: qsTr("Host")
            font.capitalization: Font.AllUppercase
        }

        ComboBoxTable {
            id: comboHost
            Layout.fillWidth: true
            column4.visible: false
            currentText: meeting.hospitalityHost ? meeting.hospitalityHost.fullname : ""
            onBeforeMenuShown: {
                model = controller.hospitalityList();
                var congregationId = 0
                column2.resizeToContents()
            }
            onRowSelected: {
                meeting.hospitalityHost = CPersons.getPerson(id)
                meeting.save()
            }
        }

        Item {
            Layout.fillHeight: true
        }
    }
}

